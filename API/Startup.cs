using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using API.MiddleWares;
using Newtonsoft.Json.Serialization;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.AspNetCore.Mvc.Razor;
using System.Globalization;
using Microsoft.AspNetCore.Localization;


namespace API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public static string ConnectionString { get; private set; }
        public static IConfiguration _config { get; private set; }
        public static string reCAPTCHASiteKey { get; private set; }
        public static string reCAPTCHASecretKey { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddControllersWithViews();
            services.AddLocalization(options => options.ResourcesPath = "Resources");
            /*
            services.AddControllersWithViews(options =>
            {
                options.Filters.Add(typeof(SampleActionFilter));
            });*/
            services.AddMvc().AddNewtonsoftJson(options => options.SerializerSettings.ContractResolver =
              new DefaultContractResolver());

            services.AddMvc().AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix).AddDataAnnotationsLocalization();

            /*
            services.Configure<RequestLocalizationOptions>(options =>
            {
                var supportedCultures = new List<CultureInfo>
                {
                    new CultureInfo("vi-VN"),
                    new CultureInfo("vi"),
                            
                };

                options.DefaultRequestCulture = new RequestCulture("vi-VN");
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
            });*/

            services.AddRazorPages();
            services.AddResponseCaching();
            services.AddControllers().AddNewtonsoftJson();
            services.AddResponseCompression(options =>
            {
                options.Providers.Add<BrotliCompressionProvider>();
                options.Providers.Add<GzipCompressionProvider>();
                
                options.MimeTypes =
                    ResponseCompressionDefaults.MimeTypes.Concat(
                        new[] { "image/svg+xml" });
            });
            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                options.IdleTimeout = TimeSpan.FromSeconds(3600);
                options.Cookie.HttpOnly = true;
                // Make the session cookie essential
                options.Cookie.IsEssential = true;
            });

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy", builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            });
            /*
            services.AddControllers().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ContractResolver = new DefaultContractResolver();
            });*/

            services.AddControllers(config =>
            {
                config.RespectBrowserAcceptHeader = true;
            }).AddXmlDataContractSerializerFormatters();


            /*
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["Jwt:Issuer"],
                        ValidAudience = Configuration["Jwt:Issuer"],                        
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
                    };
                });
            */

            services.AddAuthentication(authOptions =>
            {
                authOptions.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                authOptions.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(bearerOptions =>
            {
                var paramsValidation = bearerOptions.TokenValidationParameters;
                paramsValidation.IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]));
                paramsValidation.ValidAudience = Configuration["Jwt:Issuer"];
                paramsValidation.ValidIssuer = Configuration["Jwt:Issuer"];
                paramsValidation.ValidateIssuerSigningKey = true;
                paramsValidation.ValidateLifetime = true;
                paramsValidation.ClockSkew = TimeSpan.Zero;
            });

            services.AddAuthorization(auth =>
            {
                auth.AddPolicy("Bearer", new AuthorizationPolicyBuilder()
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                    .RequireAuthenticatedUser().Build());
            });



        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            _config = Configuration;
            ConnectionString = Configuration["ConnectionStrings:DefaultConnection"];
            reCAPTCHASiteKey = Configuration["RecaptchaSettings:SiteKey"];
            reCAPTCHASecretKey = Configuration["RecaptchaSettings:SecretKey"];


            /*
            var supportedCultures = new[] { "vi", "en" };
            var localizationOptions = new RequestLocalizationOptions().SetDefaultCulture(supportedCultures[0])
                .AddSupportedCultures(supportedCultures)               
                .AddSupportedUICultures(supportedCultures);
            localizationOptions.ApplyCurrentCultureToResponseHeaders = true;

            
            */

            var supportedCultures = new[]
            {
                new CultureInfo("vi-VN"),
                //new CultureInfo("en-US"),
                
            };

            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture("vi-VN"),
                // Formatting numbers, dates, etc.
                SupportedCultures = supportedCultures,
                // UI strings that we have localized.
                SupportedUICultures = supportedCultures
            });


            

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseRequestLocalization();
            app.UseCookiePolicy();
            app.UseSession();
            app.UseRouting();
            app.UseResponseCaching();
            app.UseCors();
            app.UseAuthorization();
            app.UseMyAuthentication();
            app.UseResponseCompression();
            app.Use(async (context, next) =>
            {
                /*
                context.Response.GetTypedHeaders().CacheControl =
                    new Microsoft.Net.Http.Headers.CacheControlHeaderValue()
                    {
                        Public = true,
                        MaxAge = TimeSpan.FromSeconds(10)
                    };*/
                context.Response.Headers[Microsoft.Net.Http.Headers.HeaderNames.Vary] =
                    new string[] { "Accept-Encoding" };

                await next();
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();

                endpoints.MapAreaControllerRoute(
                    "admin",
                    "admin",
                    "Admin/{controller=Home}/{action=Index}/{id?}");
                endpoints.MapControllerRoute(
                 name: "CartList",
                 pattern: "gio-hang.html",
                 defaults: new { controller = "Products", action = "CartList" });

                endpoints.MapControllerRoute(
                  name: "Products_Categories",
                  pattern: "loai-san-pham/{alias}-{id}.html",
                  defaults: new { controller = "Products", action = "Index" });

                endpoints.MapControllerRoute(
                   name: "Products_Detail",
                   pattern: "san-pham/{alias}-{id}.html",
                   defaults: new { controller = "Products", action = "Detail" });

                endpoints.MapControllerRoute(
                 name: "Albums",
                 pattern: "albums.html",
                 defaults: new { controller = "Albums", action = "Index" });

                endpoints.MapControllerRoute(
                 name: "DuThaoVanBan",
                 pattern: "du-thao-van-ban.html",
                 defaults: new { controller = "DuThaoVanBan", action = "Index" });

                endpoints.MapControllerRoute(
                 name: "Videos",
                 pattern: "videos.html",
                 defaults: new { controller = "Videos", action = "Index" });

                endpoints.MapControllerRoute(
                    name: "Contacts",
                    pattern: "lien-he.html",
                    defaults: new { controller = "Contacts", action = "Index" });
                endpoints.MapControllerRoute(
                   name: "SiteMap",
                   pattern: "sitemap.html",
                   defaults: new { controller = "Home", action = "SiteMap" });
                endpoints.MapControllerRoute(
                   name: "Articles",
                   pattern: "articles/{alias}.html",
                   defaults: new { controller = "Articles", action = "Index" });
                endpoints.MapControllerRoute(
                   name: "ArticlesCategories",
                   pattern: "categories/{alias}-{id}.html",
                   defaults: new { controller = "Articles", action = "GetByCat" });
                endpoints.MapControllerRoute(
                   name: "GioiThieu",
                   pattern: "gioi-thieu.html",
                   defaults: new { controller = "Articles", action = "Detail", Id = 1 });
                endpoints.MapControllerRoute(
                   name: "BaoHanh",
                   pattern: "bao-hanh-bao-duong.html",
                   defaults: new { controller = "Articles", action = "Detail", Id = 11 });
                endpoints.MapControllerRoute(
                   name: "GiaoHang",
                   pattern: "giao-hang.html",
                   defaults: new { controller = "Articles", action = "Detail", Id = 10 });
                endpoints.MapControllerRoute(
                   name: "ThanhToan",
                   pattern: "thanh-toan.html",
                   defaults: new { controller = "Articles", action = "Detail", Id = 9 });
                endpoints.MapControllerRoute(
                   name: "HuongDanMuaHang",
                   pattern: "huong-dan-mua-hang.html",
                   defaults: new { controller = "Articles", action = "Detail", Id = 8 });

                endpoints.MapControllerRoute(
                   name: "CachDangKyTaiKhoan",
                   pattern: "cach-dang-ky-tai-khoan.html",
                   defaults: new { controller = "Articles", action = "Detail", Id = 7 });


                endpoints.MapControllerRoute(
                   name: "Recruitment",
                   pattern: "tuyen-dung.html",
                   defaults: new { controller = "Recruitment", action = "Index" });

                endpoints.MapControllerRoute(
                   name: "Articles_Detail",
                   pattern: "{alias}-{id}.html",
                   defaults: new { controller = "Articles", action = "Detail" });

                endpoints.MapControllerRoute(
                    "default", "{controller=Home}/{action=Index}/{id?}");
            });

        }
    }
}
