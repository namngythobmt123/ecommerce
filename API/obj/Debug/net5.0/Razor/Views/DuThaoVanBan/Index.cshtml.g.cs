#pragma checksum "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\DuThaoVanBan\Index.cshtml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "9b7756aeefd4db0924b0a76104f7699f1ce711532f141175d26ff1740878e137"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_DuThaoVanBan_Index), @"mvc.1.0.view", @"/Views/DuThaoVanBan/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using global::System;
    using global::System.Collections.Generic;
    using global::System.Linq;
    using global::System.Threading.Tasks;
    using global::Microsoft.AspNetCore.Mvc;
    using global::Microsoft.AspNetCore.Mvc.Rendering;
    using global::Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\_ViewImports.cshtml"
using API;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\_ViewImports.cshtml"
using API.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\DuThaoVanBan\Index.cshtml"
using Newtonsoft.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\DuThaoVanBan\Index.cshtml"
using API.Areas.Admin.Models.DuThaoVanBan;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"Sha256", @"9b7756aeefd4db0924b0a76104f7699f1ce711532f141175d26ff1740878e137", @"/Views/DuThaoVanBan/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"Sha256", @"de782fee9593460bafed273ea97f173af49f59f3f83dccfacb27283faf97b742", @"/Views/_ViewImports.cshtml")]
    #nullable restore
    public class Views_DuThaoVanBan_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<API.Areas.Admin.Models.DuThaoVanBan.DuThaoVanBanModel>
    #nullable disable
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("name", "_PartialMsgInfo", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "DuThaoVanBanGopY", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.PartialTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 4 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\DuThaoVanBan\Index.cshtml"
  
    Layout = "_LayoutHome";
    ViewData["Title"] = "Văn bản dự thảo";
    string ControllerName = this.ViewContext.RouteData.Values["controller"].ToString();

#line default
#line hidden
#nullable disable
            WriteLiteral("<div class=\"PageDetail\">\r\n    <nav aria-label=\"breadcrumb\">\r\n        <ol class=\"breadcrumb\">\r\n            <li class=\"breadcrumb-item\"><a href=\"/\">Trang chủ</a></li>\r\n            <li class=\"breadcrumb-item active\" aria-current=\"page\">");
#nullable restore
#line 13 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\DuThaoVanBan\Index.cshtml"
                                                              Write(ViewData["Title"]);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</li>
        </ol>
    </nav>

    <div class=""alert alert-primary draft_justify"">
        Hiện nay, Chính phủ đang xây dựng một số Dự thảo Văn bản quy phạm pháp luật. Thực hiện ý kiến chỉ đạo của Thủ tướng Chính phủ, Cổng TTĐT Chính phủ xin giới thiệu toàn văn nội dung các Dự thảo Văn bản quy phạm pháp luật dưới đây để lấy ý kiến đóng góp rộng rãi của các cơ quan, tổ chức, doanh nghiệp và đông đảo người dân trong và ngoài nước.<br>
        Mời bạn đọc nhấn vào các Dự thảo Văn bản quy phạm pháp luật dưới đây để tham gia góp ý.
    </div>

    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("partial", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "9b7756aeefd4db0924b0a76104f7699f1ce711532f141175d26ff1740878e1375941", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.PartialTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper.Name = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"

    <table class=""table table-bordered myTable"" style=""margin-top: 15px;"">
        <thead>
            <tr>
                <th width=""50px"">STT</th>
                <th>Tiêu đề</th>
                <th width=""100px"" class=""text-center"">Góp ý</th>
                <th width=""100px"" class=""text-center"">Tải về</th>
            </tr>
        </thead>
        <tbody>
");
#nullable restore
#line 34 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\DuThaoVanBan\Index.cshtml"
             if (Model.ListItems == null || Model.ListItems.Count() == 0)
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                <tr>\r\n                    <td colspan=\"5\" class=\"text-center\">Không có văn bản nào</td>\r\n                </tr>\r\n");
#nullable restore
#line 39 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\DuThaoVanBan\Index.cshtml"
            }
            else
            {
                for (int i = 0; i < Model.ListItems.Count(); i++)
                {


#line default
#line hidden
#nullable disable
            WriteLiteral("                    <tr>\r\n                        <td>");
#nullable restore
#line 46 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\DuThaoVanBan\Index.cshtml"
                        Write(i + 1);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                        <td>");
#nullable restore
#line 47 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\DuThaoVanBan\Index.cshtml"
                       Write(Model.ListItems[i].Title);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                        <td class=\"text-center\">");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "9b7756aeefd4db0924b0a76104f7699f1ce711532f141175d26ff1740878e1378903", async() => {
                WriteLiteral(" <i class=\"icofont-comment\"></i> Góp Ý");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            BeginWriteTagHelperAttribute();
#nullable restore
#line 48 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\DuThaoVanBan\Index.cshtml"
                                                       WriteLiteral(ControllerName);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-controller", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-Id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#nullable restore
#line 48 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\DuThaoVanBan\Index.cshtml"
                                                                                                                    WriteLiteral(Model.ListItems[i].Id);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["Id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-Id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["Id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            BeginWriteTagHelperAttribute();
#nullable restore
#line 48 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\DuThaoVanBan\Index.cshtml"
                                                                                                                                                           WriteLiteral(Model.ListItems[i].Ids);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["Ids"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-Ids", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["Ids"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("</td>\r\n                        <td class=\"text-center\">\r\n                            <a");
            BeginWriteAttribute("href", " href=\"", 2328, "\"", 2359, 1);
#nullable restore
#line 50 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\DuThaoVanBan\Index.cshtml"
WriteAttributeValue("", 2335, Model.ListItems[i].Link, 2335, 24, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" target=\"_blank\"> <i class=\"icofont-download\"></i> Tải về</a>\r\n\r\n                        </td>\r\n                    </tr>\r\n");
#nullable restore
#line 54 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\DuThaoVanBan\Index.cshtml"
                }
            }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </tbody>\r\n    </table>\r\n</div>\r\n\r\n<div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n        ");
#nullable restore
#line 63 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\DuThaoVanBan\Index.cshtml"
   Write(await Html.PartialAsync("_PartialPagination", Model.Pagination));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </div>\r\n\r\n</div>\r\n");
        }
        #pragma warning restore 1998
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<API.Areas.Admin.Models.DuThaoVanBan.DuThaoVanBanModel> Html { get; private set; } = default!;
        #nullable disable
    }
}
#pragma warning restore 1591
