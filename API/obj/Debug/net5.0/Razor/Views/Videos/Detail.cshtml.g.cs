#pragma checksum "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Videos\Detail.cshtml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "ae27c3d8c0fc81f79c275d91f35cb1ed406e85912a71f5da35e712f42f56873f"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Videos_Detail), @"mvc.1.0.view", @"/Views/Videos/Detail.cshtml")]
namespace AspNetCore
{
    #line hidden
    using global::System;
    using global::System.Collections.Generic;
    using global::System.Linq;
    using global::System.Threading.Tasks;
    using global::Microsoft.AspNetCore.Mvc;
    using global::Microsoft.AspNetCore.Mvc.Rendering;
    using global::Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\_ViewImports.cshtml"
using API;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\_ViewImports.cshtml"
using API.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"Sha256", @"ae27c3d8c0fc81f79c275d91f35cb1ed406e85912a71f5da35e712f42f56873f", @"/Views/Videos/Detail.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"Sha256", @"de782fee9593460bafed273ea97f173af49f59f3f83dccfacb27283faf97b742", @"/Views/_ViewImports.cshtml")]
    #nullable restore
    public class Views_Videos_Detail : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<API.Areas.Admin.Models.Videos.VideosModel>
    #nullable disable
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Videos\Detail.cshtml"
  
    ViewData["Title"] = Model.Item.Title;
    string ControllerName = this.ViewContext.RouteData.Values["controller"].ToString();

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<div class=""row"">
    <div class=""col-md-12"">
        <nav aria-label=""breadcrumb"">
            <ol class=""breadcrumb"">
                <li class=""breadcrumb-item""><a href=""/"">Trang chủ</a></li>
                <li class=""breadcrumb-item""><a href=""/videos.html"">Videos</a></li>
                <li class=""breadcrumb-item active"" aria-current=""page"">");
#nullable restore
#line 13 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Videos\Detail.cshtml"
                                                                  Write(ViewData["Title"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("</li>\r\n            </ol>\r\n        </nav>\r\n    </div>\r\n</div>\r\n<div class=\"PageDetail\">\r\n    <div class=\"row\">\r\n        <div class=\"col-lg-12\">\r\n            <video width=\"100%\"");
            BeginWriteAttribute("controls", "  controls=\"", 739, "\"", 751, 0);
            EndWriteAttribute();
            WriteLiteral(" class=\"video-item\" autoplay>\r\n                <source");
            BeginWriteAttribute("src", " src=\"", 806, "\"", 828, 1);
#nullable restore
#line 22 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Videos\Detail.cshtml"
WriteAttributeValue("", 812, Model.Item.Link, 812, 16, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" type=\"video/mp4\">\r\n            </video>\r\n            <h3 class=\"detail-title-video\">");
#nullable restore
#line 24 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Videos\Detail.cshtml"
                                      Write(Model.Item.Title);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h3>\r\n            <p class=\"text-justify\">");
#nullable restore
#line 25 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Videos\Detail.cshtml"
                               Write(Model.Item.Description);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n        </div>\r\n\r\n    </div>\r\n</div>\r\n<style>\r\n\r\n    .detail-title-video{\r\n        font-size: 14px;\r\n        text-align: center;\r\n        font-weight: bold;\r\n        margin-top: 10px;\r\n    }\r\n</style>");
        }
        #pragma warning restore 1998
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<API.Areas.Admin.Models.Videos.VideosModel> Html { get; private set; } = default!;
        #nullable disable
    }
}
#pragma warning restore 1591
