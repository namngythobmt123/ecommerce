#pragma checksum "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialProductsFeatured.cshtml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "b5b363710680c2a7cc4e6de372faaeaa274abd77669bc66a06f2f6f474a06fa0"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared__PartialProductsFeatured), @"mvc.1.0.view", @"/Views/Shared/_PartialProductsFeatured.cshtml")]
namespace AspNetCore
{
    #line hidden
    using global::System;
    using global::System.Collections.Generic;
    using global::System.Linq;
    using global::System.Threading.Tasks;
    using global::Microsoft.AspNetCore.Mvc;
    using global::Microsoft.AspNetCore.Mvc.Rendering;
    using global::Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\_ViewImports.cshtml"
using API;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\_ViewImports.cshtml"
using API.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"Sha256", @"b5b363710680c2a7cc4e6de372faaeaa274abd77669bc66a06f2f6f474a06fa0", @"/Views/Shared/_PartialProductsFeatured.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"Sha256", @"de782fee9593460bafed273ea97f173af49f59f3f83dccfacb27283faf97b742", @"/Views/_ViewImports.cshtml")]
    #nullable restore
    public class Views_Shared__PartialProductsFeatured : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<API.Areas.Admin.Models.Products.ProductsModel>
    #nullable disable
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Products", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Detail", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialProductsFeatured.cshtml"
   
    string Domain = "";
    if (Context.Request.IsHttps)
    {
        Domain = "https://" + Context.Request.Host;
    }
    else
    {
        Domain = "http://" + Context.Request.Host;
    }
    string Link = "";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<div class=""product-single-row-slider-area mb-4"">
    <div class=""container"">

        <div class=""row align-items-center"">
            <div class=""col-lg-7"">
                <!--=======  section title  =======-->

                <div class=""section-title mb-3"">
                    <h2>Sản phẩm bán chạy</h2>
                </div>

                <!--=======  End of section title  =======-->
            </div>

           
        </div>

        <div class=""row"">
            <div class=""col-lg-12"">
                <!--=======  product single row slider wrapper  =======-->

                <div class=""product-single-row-slider-wrapper"">
                    <div class=""ht-slick-slider"">
");
#nullable restore
#line 38 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialProductsFeatured.cshtml"
                         for (int i = 0; i < Model.ListItems.Count(); i++)
                        {
                            

#line default
#line hidden
#nullable disable
            WriteLiteral("                        <!--=======  single slider product  =======-->\r\n");
            WriteLiteral("                        <div class=\"single-slider-product-wrapper\">\r\n                            <div class=\"single-slider-product\">\r\n                                <div class=\"single-slider-product__image\">\r\n                                    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b5b363710680c2a7cc4e6de372faaeaa274abd77669bc66a06f2f6f474a06fa05864", async() => {
                WriteLiteral("\r\n                                        <img");
                BeginWriteAttribute("src", " src=\"", 1639, "\"", 1670, 1);
#nullable restore
#line 47 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialProductsFeatured.cshtml"
WriteAttributeValue("", 1645, Model.ListItems[i].Image, 1645, 25, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" class=\"img-fluid\"");
                BeginWriteAttribute("alt", " alt=\"", 1689, "\"", 1720, 1);
#nullable restore
#line 47 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialProductsFeatured.cshtml"
WriteAttributeValue("", 1695, Model.ListItems[i].Title, 1695, 25, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(">\r\n                                        <img");
                BeginWriteAttribute("src", " src=\"", 1768, "\"", 1799, 1);
#nullable restore
#line 48 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialProductsFeatured.cshtml"
WriteAttributeValue("", 1774, Model.ListItems[i].Image, 1774, 25, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" class=\"img-fluid\"");
                BeginWriteAttribute("alt", " alt=\"", 1818, "\"", 1849, 1);
#nullable restore
#line 48 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialProductsFeatured.cshtml"
WriteAttributeValue("", 1824, Model.ListItems[i].Title, 1824, 25, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(">\r\n                                    ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#nullable restore
#line 46 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialProductsFeatured.cshtml"
                                                                                       WriteLiteral(Model.ListItems[i].Id);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            BeginWriteTagHelperAttribute();
#nullable restore
#line 46 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialProductsFeatured.cshtml"
                                                                                                                                WriteLiteral(Model.ListItems[i].Alias);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["alias"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-alias", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["alias"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"


                                    <div class=""hover-icons"">
                                        <ul>
                                            <li><a data-toggle=""modal"" data-target=""#quick-view-modal-container"" href=""javascript:void(0)""><i class=""icon-eye""></i></a></li>
                                            <li><a href=""javascript:void(0)""><i class=""icon-heart""></i></a></li>
                                            <li><a href=""javascript:void(0)""><i class=""icon-sliders""></i></a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class=""single-slider-product__content"">
                                    <div class=""product-availability mb-15"">
                                        <div class=""product-availability__text"">
                                            <div class=""sold"">Đã bán:");
#nullable restore
#line 64 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialProductsFeatured.cshtml"
                                                                Write(Model.ListItems[i].Sold);

#line default
#line hidden
#nullable disable
            WriteLiteral("</div>\r\n                                            <div class=\"remaining\">Số lượng còn:");
#nullable restore
#line 65 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialProductsFeatured.cshtml"
                                                                           Write(Model.ListItems[i].Available);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</div>
                                        </div>
                                        <div class=""progress"">
                                            <div class=""progress-bar bg-warning"" role=""progressbar"" style=""width: 75%"" aria-valuenow=""75"" aria-valuemin=""0"" aria-valuemax=""100""></div>
                                        </div>
                                    </div>
                                    <p class=""product-title"">");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b5b363710680c2a7cc4e6de372faaeaa274abd77669bc66a06f2f6f474a06fa013183", async() => {
#nullable restore
#line 71 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialProductsFeatured.cshtml"
                                                                                                                                                                                           Write(Model.ListItems[i].PriceDisplay);

#line default
#line hidden
#nullable disable
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#nullable restore
#line 71 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialProductsFeatured.cshtml"
                                                                                                                WriteLiteral(Model.ListItems[i].Id);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            BeginWriteTagHelperAttribute();
#nullable restore
#line 71 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialProductsFeatured.cshtml"
                                                                                                                                                         WriteLiteral(Model.ListItems[i].Alias);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["alias"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-alias", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["alias"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"</p>
                                    <div class=""rating"">
                                        <i class=""ion-android-star active""></i>
                                        <i class=""ion-android-star active""></i>
                                        <i class=""ion-android-star active""></i>
                                        <i class=""ion-android-star active""></i>
                                        <i class=""ion-android-star""></i>
                                    </div>
                                    <p class=""product-price""><span class=""discounted-price"">");
#nullable restore
#line 79 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialProductsFeatured.cshtml"
                                                                                       Write(Model.ListItems[i].PriceDisplay);

#line default
#line hidden
#nullable disable
            WriteLiteral("</span> <span class=\"main-price discounted\">");
#nullable restore
#line 79 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialProductsFeatured.cshtml"
                                                                                                                                                                   Write(Model.ListItems[i].PriceDealDisplay);

#line default
#line hidden
#nullable disable
            WriteLiteral("</span></p>\r\n\r\n                                    <span class=\"cart-icon\">");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b5b363710680c2a7cc4e6de372faaeaa274abd77669bc66a06f2f6f474a06fa018423", async() => {
                WriteLiteral("<i class=\"icon-shopping-cart\"></i>");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#nullable restore
#line 81 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialProductsFeatured.cshtml"
                                                                                                               WriteLiteral(Model.ListItems[i].Id);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            BeginWriteTagHelperAttribute();
#nullable restore
#line 81 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialProductsFeatured.cshtml"
                                                                                                                                                        WriteLiteral(Model.ListItems[i].Alias);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["alias"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-alias", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["alias"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("</span>\r\n                                </div>\r\n                            </div>\r\n\r\n                        </div>\r\n");
            WriteLiteral("                        <!--=======  End of single slider product  =======-->\r\n");
#nullable restore
#line 88 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialProductsFeatured.cshtml"

                        }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </div>\r\n                </div>\r\n\r\n                <!--=======  End of product single row slider wrapper  =======-->\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n");
        }
        #pragma warning restore 1998
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<API.Areas.Admin.Models.Products.ProductsModel> Html { get; private set; } = default!;
        #nullable disable
    }
}
#pragma warning restore 1591
