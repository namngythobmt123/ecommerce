#pragma checksum "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialSurvey.cshtml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "fa50624eb699938281a2aa2cc8a1943f5fcf6375c7ff41a23e516f87f8406bc4"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared__PartialSurvey), @"mvc.1.0.view", @"/Views/Shared/_PartialSurvey.cshtml")]
namespace AspNetCore
{
    #line hidden
    using global::System;
    using global::System.Collections.Generic;
    using global::System.Linq;
    using global::System.Threading.Tasks;
    using global::Microsoft.AspNetCore.Mvc;
    using global::Microsoft.AspNetCore.Mvc.Rendering;
    using global::Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\_ViewImports.cshtml"
using API;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\_ViewImports.cshtml"
using API.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialSurvey.cshtml"
using API.Areas.Admin.Models.Survey;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialSurvey.cshtml"
using Microsoft.Extensions.Configuration;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"Sha256", @"fa50624eb699938281a2aa2cc8a1943f5fcf6375c7ff41a23e516f87f8406bc4", @"/Views/Shared/_PartialSurvey.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"Sha256", @"de782fee9593460bafed273ea97f173af49f59f3f83dccfacb27283faf97b742", @"/Views/_ViewImports.cshtml")]
    #nullable restore
    public class Views_Shared__PartialSurvey : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    #nullable disable
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Home", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "SendQuesion", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 4 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialSurvey.cshtml"
  
    Survey DetailItem = SurveyService.GetSurveyHome(Configuration["Security:SecretId"] + "HomeController");

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialSurvey.cshtml"
 if (DetailItem != null && DetailItem.Id > 0) {

#line default
#line hidden
#nullable disable
            WriteLiteral(@"<div id=""vm_survey"">
    <div class=""alert alert-primary"" role=""alert"" id=""msgSuccessSurvey"" style=""display:none;"">Khảo sát thành công</div>
    <div class=""alert alert-danger"" role=""alert"" id=""msgErrSurvey"" style=""display:none;"">Khảo sát thất bại</div>
    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "fa50624eb699938281a2aa2cc8a1943f5fcf6375c7ff41a23e516f87f8406bc45578", async() => {
                WriteLiteral("\r\n\r\n        <div class=\"survey-item\">\r\n            <div class=\"survey-title\">");
#nullable restore
#line 14 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialSurvey.cshtml"
                                 Write(DetailItem.Title);

#line default
#line hidden
#nullable disable
                WriteLiteral("</div>\r\n            <div class=\"survey-question\">\r\n");
#nullable restore
#line 16 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialSurvey.cshtml"
                 if (DetailItem.ListQuestion != null && DetailItem.ListQuestion.Count > 0)
                {
                    for (int i = 0; i < DetailItem.ListQuestion.Count(); i++)
                    {


#line default
#line hidden
#nullable disable
                WriteLiteral("                <div class=\"form-check\">\r\n                    <input class=\"form-check-input\" name=\"question_customer\" type=\"radio\"");
                BeginWriteAttribute("id", " id=\"", 1104, "\"", 1156, 2);
                WriteAttributeValue("", 1109, "flexRadioDefault_", 1109, 17, true);
#nullable restore
#line 22 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialSurvey.cshtml"
WriteAttributeValue("", 1126, DetailItem.ListQuestion[i].Id, 1126, 30, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                BeginWriteAttribute("value", " value=\"", 1157, "\"", 1195, 1);
#nullable restore
#line 22 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialSurvey.cshtml"
WriteAttributeValue("", 1165, DetailItem.ListQuestion[i].Id, 1165, 30, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(">\r\n                    <label class=\"form-check-label\"");
                BeginWriteAttribute("for", " for=\"", 1250, "\"", 1303, 2);
                WriteAttributeValue("", 1256, "flexRadioDefault_", 1256, 17, true);
#nullable restore
#line 23 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialSurvey.cshtml"
WriteAttributeValue("", 1273, DetailItem.ListQuestion[i].Id, 1273, 30, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(">\r\n                        ");
#nullable restore
#line 24 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialSurvey.cshtml"
                   Write(DetailItem.ListQuestion[i].Title);

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                    </label>\r\n                </div>\r\n");
#nullable restore
#line 27 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialSurvey.cshtml"
                    }

                }

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n            </div>\r\n            <div class=\"survey-footer text-center\">\r\n                <button type=\"button\" class=\"btn btn-primary btn-sm me-2\" style=\"display:none;\">Bình chọn ...</button>\r\n                <button type=\"button\"");
                BeginWriteAttribute("onclick", " onclick=\"", 1696, "\"", 1753, 6);
                WriteAttributeValue("", 1706, "SendQuestion(", 1706, 13, true);
#nullable restore
#line 34 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialSurvey.cshtml"
WriteAttributeValue("", 1719, DetailItem.Id, 1719, 14, false);

#line default
#line hidden
#nullable disable
                WriteAttributeValue("", 1733, ",", 1733, 1, true);
                WriteAttributeValue(" ", 1734, "\'", 1735, 2, true);
#nullable restore
#line 34 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialSurvey.cshtml"
WriteAttributeValue("", 1736, DetailItem.Ids, 1736, 15, false);

#line default
#line hidden
#nullable disable
                WriteAttributeValue("", 1751, "\')", 1751, 2, true);
                EndWriteAttribute();
                WriteLiteral(" class=\"btn btn-primary btn-sm me-2\">Bình chọn</button>\r\n\r\n            </div>\r\n        </div>\r\n    ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Controller = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n</div>\r\n");
#nullable restore
#line 40 "C:\Users\namng\Documents\source\CropTayNguyen\API_MYPHAM\API\Views\Shared\_PartialSurvey.cshtml"
}

#line default
#line hidden
#nullable disable
        }
        #pragma warning restore 1998
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public IConfiguration Configuration { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; } = default!;
        #nullable disable
    }
}
#pragma warning restore 1591
