﻿function SendQuestion(Id, Ids) {
    $('#msgErrSurvey').hide();
    $('#msgSuccessSurvey').hide();
    var token = jQuery('input[name="__RequestVerificationToken"]').val();
    var headers = {};
    headers["RequestVerificationToken"] = token;
    
    var datailItem = { SurveyId: Id, SurveyIds: Ids, Id: $("input[name='question_customer']:checked").val() , Ids: '', IdCoQuan: 0, Title: '',Token:'' };

    grecaptcha.ready(function () {
        grecaptcha.execute('6Lc9Mz4aAAAAAIcKI4xnbI2BWZc5zaBkRhXotoUq', { action: 'submit' }).then(function (token) {
                       
            datailItem.Token = token;
            
            return $.ajax({
                type: "POST",
                url: '/Home/SendQuestion',
                headers: headers,
                data: JSON.stringify(datailItem),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    
                    if (result.Success == true) {

                        $('#msgSuccessSurvey').html(result.Msg) ;
                        $('#msgSuccessSurvey').show() ;                       
                        localStorage.setItem("flagQuestion", datailItem.SurveyId);

                    } else {
                        $('#msgErrSurvey').html(result.Msg);
                        $('#msgErrSurvey').show();
                    }

                },
                error: function () {
                    

                }
            });
        });
    });
} 
function SendNewsLetter() {
    var token = jQuery('#NewsLetterForm input[name="__RequestVerificationToken"]').val();
    var headers = {};
    headers["RequestVerificationToken"] = token;

    var Email = $("#newsletter-email").val();

    if (Email == null || Email == "") {
        ResetFormNewsLetter();
        $("#newsletter-info").html("Email không được để trống");
        $("#newsletter-info").show();

    } else {
        $("#newsletter-send").hide();
        $("#newsletter-nosend").show();

        var datailItem = { Email: Email, Token: '' };
        grecaptcha.ready(function () {
            grecaptcha.execute('6Lc9Mz4aAAAAAIcKI4xnbI2BWZc5zaBkRhXotoUq', { action: 'submit' }).then(function (token) {

                datailItem.Token = token;

                $.ajax({
                    url: '/Home/NewsLetter',
                    headers: headers,
                    type: 'POST',
                    data: JSON.stringify(datailItem),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        ResetFormNewsLetter();
                        $("#newsletter-info").html(data.Msg);
                        $("#newsletter-info").show();
                    },
                    error: function () {
                        ResetFormNewsLetter();
                    }
                });

            });
        });



        
    }


}

function ResetFormNewsLetter() {
    $("#newsletter-send").show();
    $("#newsletter-nosend").hide();
    $("#newsletter-info").hide();
    $("#newsletter-email").val('');

}

function AddToCart(Ids, Quantity) {
    $.ajax({
        url: "/Products/AddCart?Ids=" + Ids + "&Quantity=" + Quantity,
        method: 'GET',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            window.location.replace("/gio-hang.html");
            $(".cart-counter").html(result.Data);
        },
        error: function () {

        }
    });
}

function AddToCart2(Id, Quantity) {
    $.ajax({
        url: "/Products/AddCartSearch?Id=" + Id + "&Quantity=" + Quantity,
        method: 'GET',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            window.location.replace("/gio-hang.html");
            $(".cart-counter").html(result.Data);
        },
        error: function () {

        }
    });
}

function dropdownToggleMenu(id) {
    console.log(id);
    $('.' + id).slideToggle(400);
    
}



