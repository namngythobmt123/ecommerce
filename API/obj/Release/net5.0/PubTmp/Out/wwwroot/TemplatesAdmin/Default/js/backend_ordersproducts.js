﻿var vmBackendOrdersProducts = new Vue({
    el: '#app_backend_ordersproducts',
    data: {
        title: "Quản lý Vé",
        SearchItems: { CurrentPage: 1, ItemsPerPage: 10, Keyword: '', msg: '', recharge_type: -1, Day: 0, Month: 0, Year: 0, IdSearch: 0, UserIdCheckIn:0 },      
        msgInfo: "",
        ListItems: [],
        detailItem: { FullName: "", FullName: '', Ids: '', Id: 0 },

        flag: false,
        flagLoading: false,
        flagLoadingChild: false,
        pageCount: 0,
        TotalRows: 0,
    },
    created() {
        
        $(".app-vue").show();
        this.getListItems();

    },
    methods: {

        getListItems: function () {
            this.ListItems = [];
            this.flagLoading = true;
            $.ajax({
                url: '/Admin/OrdersProducts/GetListItems',
                type: 'POST',
                data: JSON.stringify(this.SearchItems),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    
                    if (result.Success) {
                        vmBackendOrdersProducts.ListItems = result.Data;
                        if (vmBackendOrdersProducts.ListItems.length > 0) {
                            vmBackendOrdersProducts.TotalRows = (vmBackendOrdersProducts.ListItems[0]).TotalRows;
                            vmBackendOrdersProducts.pageCount = Math.ceil(vmBackendOrdersProducts.TotalRows / vmBackendOrdersProducts.SearchItems.ItemsPerPage);

                        } else {
                            vmBackendOrdersProducts.TotalRows = 0;
                            vmBackendOrdersProducts.pageCount = 0;
                        }
                    } else {
                        vmBackendOrdersProducts.TotalRows = 0;
                        vmBackendOrdersProducts.pageCount = 0;
                    }
                    vmBackendOrdersProducts.flagLoading = false;


                }
            });


        },
        pageChanged() {
            this.getListItems();
        },
        SearchChange() {

            this.SearchItems.CurrentPage = 1;
            this.getListItems();
        },

        ChangeKeyword(e) {
            console.log(e);
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) {
                this.SearchChange();
            }
        },
    }
});
Vue.component('paginate', VuejsPaginate);