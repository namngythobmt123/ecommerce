﻿var vmBackendOrdersProducts = new Vue({
    el: '#app_backend_baocao',
    data: {
        title: "Báo cáo tình trạng check in theo tháng",
        SearchItems: { Month: 0, Year: 0 },
        msgInfo: "",
        ListItems: [],        
        flag: false,
        flagLoading: false,
        flagLoadingChild: false,
        ListDay: [],
        ListYear: [],
        ListMonth: [],
        
    },
    created() {

        $(".app-vue").show();
        var date = new Date();
        
        this.SearchItems.Year = date.getFullYear();
        this.SearchItems.Month = date.getMonth() + 1;

        for (let i = 1; i < 13; i++) {
            this.ListMonth.push(i);
        }
        for (let i = 1; i < 32; i++) {
            this.ListDay.push(i);
        }
        for (let i = 2020; i <= date.getFullYear(); i++) {
            this.ListYear.push(i);
        }  

        this.getListItems();

    },
    methods: {

        getListItems: function () {
            this.ListItems = [];
            this.flagLoading = true;
            $.ajax({
                url: '/Admin/BaoCao/GetListItems',
                type: 'POST',
                data: JSON.stringify(this.SearchItems),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {

                    if (result.Success) {
                        vmBackendOrdersProducts.ListItems = result.Data;
                        
                    } else {
                       
                    }
                    vmBackendOrdersProducts.flagLoading = false;


                }
            });


        },
        pageChanged() {
            this.getListItems();
        },
        SearchChange() {

            this.SearchItems.CurrentPage = 1;
            this.getListItems();
        },

        ChangeKeyword(e) {
            console.log(e);
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) {
                this.SearchChange();
            }
        },
    }
});
Vue.component('paginate', VuejsPaginate);