﻿var vmBackendOrdersOffline = new Vue({
    el: '#app_backend_orders_offline',
    data: {
        title: "Quản lý vé offline",
        SearchItems: { CurrentPage: 1, ItemsPerPage: 10, Keyword: '', msg: '', recharge_type: -1, Day: 0, Month: 0, Year: 0 },
        msgInfo: "",
        ListItems: [],
        detailItem: { FullName: "", FullName: '', Ids: '', Id: 0 },
        deleteItem: { FullName: "", FullName: '', Ids: '', Id: 0 },
        flag: false,
        flagLoading: false,
        flagLoadingChild: false,
        pageCount: 0,
        TotalRows: 0,
        ListDay: [],
        ListYear: [],
        ListMonth: [],
        now: new Date(),
    },
    created() {
        $(".app-vue").show();
        this.getListItems();
        var date = new Date();
        this.SearchItems.Day = date.getDate(); //+ "/"+(date.getMonth()+1)+"/"+date.getFullYear();   
        this.SearchItems.Year = date.getFullYear();
        this.SearchItems.Month = date.getMonth() + 1;

        for (let i = 1; i < 13; i++) {
            this.ListMonth.push(i);
        }
        for (let i = 1; i < 32; i++) {
            this.ListDay.push(i);
        }
        for (let i = 2020; i <= this.now.getFullYear(); i++) {
            this.ListYear.push(i);
        }



    },
    methods: {
        UpdateStatus: function () {
            var token = jQuery('input[name="__RequestVerificationToken"]').val();
            var headers = {};
            headers["RequestVerificationToken"] = token;
            
            this.flagLoadingChild = true;
            return $.ajax({
                type: "POST",
                url: '/Admin/OrdersOffline/UpdateStatus/' + this.detailItem.Ids,
                headers: headers,
                data: JSON.stringify({ "Ids": this.detailItem.Ids }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    vmBackendOrdersOffline.detailItem = result.Data;
                    vmBackendOrdersOffline.getListItems();
                    vmBackendOrdersOffline.flagLoadingChild = false;
                    $("#UpdateStatusModal").modal("hide");
                    
                },
                error: function () {
                    vmBackendOrdersOffline.flagLoadingChild = false;
                    vmBackendOrdersOffline.getListItems();
                }
            });
        },
        showUpdateStatus(i) {
            this.detailItem = this.ListItems[i];
            $("#UpdateStatusModal").modal("show");
        },

        
        showDeleted: function (item) {
            this.deleteItem = item;
            $("#DeletedModal").modal("show");

        },
        GeneralTicket: function (item) {
            this.msgInfo = "";
            var token = jQuery('input[name="__RequestVerificationToken"]').val();
            this.detailItem = item;
            var headers = {};
            $("#GeneralTicketModal").modal("show");
            headers["RequestVerificationToken"] = token;
            this.flagLoadingChild = true;
            return $.ajax({
                type: "POST",
                url: '/Admin/OrdersOffline/GeneralTicket/' + item.Ids,
                headers: headers,
                data: JSON.stringify({ "Ids": item.Ids }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {

                    vmBackendOrdersOffline.msgInfo = 'http://datve.vuonquocgiabavi.com.vn/'+ result.Data;

                    vmBackendOrdersOffline.flagLoadingChild = false;
                },
                error: function () {

                    vmBackendOrdersOffline.flagLoadingChild = false;
                }
            });

        },
        DeletedItem: function () {
            var token = jQuery('input[name="__RequestVerificationToken"]').val();
            var headers = {};
            headers["RequestVerificationToken"] = token;
            this.flagLoadingChild = true;
            return $.ajax({
                type: "POST",
                url: '/Admin/OrdersOffline/DeleteItem/' + this.deleteItem.Ids,
                headers: headers,
                data: JSON.stringify({ "Ids": this.deleteItem.Ids }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (result.Success) {
                        vmBackendOrdersOffline.getListItems();                        
                        $("#DeletedModal").modal("hide");
                    } else {
                        vmBackendOrdersOffline.msgInfo = result.Msg;
                    }
                    vmBackendOrdersOffline.flagLoadingChild = false;
                },
                error: function () {
                    vmBackendOrdersOffline.flagLoadingChild = false;
                    vmBackendOrdersOffline.getListItems();
                }
            });

        },
        getListItems: function () {
            this.ListItems = [];
            this.flagLoading = true;
            $.ajax({
                url: '/Admin/OrdersOffline/GetListItems',
                type: 'POST',
                data: JSON.stringify(this.SearchItems),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    if (result.Success) {
                        vmBackendOrdersOffline.ListItems = result.Data.ListItems;
                        if (vmBackendOrdersOffline.ListItems.length > 0) {
                            vmBackendOrdersOffline.TotalRows = (vmBackendOrdersOffline.ListItems[0]).TotalRows;
                            vmBackendOrdersOffline.pageCount = Math.ceil(vmBackendOrdersOffline.TotalRows / vmBackendOrdersOffline.SearchItems.ItemsPerPage);

                        } else {
                            vmBackendOrdersOffline.TotalRows = 0;
                            vmBackendOrdersOffline.pageCount = 0;
                        }
                    } else {
                        vmBackendOrdersOffline.TotalRows = 0;
                        vmBackendOrdersOffline.pageCount = 0;
                    }
                    vmBackendOrdersOffline.flagLoading = false;


                }
            });


        },
        pageChanged() {
            this.getListItems();
        },
        SearchChange() {

            this.SearchItems.CurrentPage = 1;
            this.getListItems();
        },

        ChangeKeyword(e) {
            console.log(e);
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) {
                this.SearchChange();
            }
        },
    }
});
Vue.component('paginate', VuejsPaginate);