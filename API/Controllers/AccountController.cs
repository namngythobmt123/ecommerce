﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using API.Areas.Admin.Models.USUsers;
using Microsoft.AspNetCore.Authorization;
using API.Models;
using Microsoft.AspNetCore.Session;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using API.Areas.Admin.Models.USGroups;
using API.Areas.Admin.Models.DMChucVu;
using API.Areas.Admin.Models.ManagerFiles;
using System.IO;

namespace API.Controllers
{
    //[AddHeaderAttribute("ds","ds")]    
    public class AccountController : Controller
    {
        public IActionResult Login()
        {
            AccountLogin model = new AccountLogin();
            return View(model);
        }
        [HttpPost]        
        public IActionResult Login([FromBody] AccountLogin model)
        {
            string Msg = "";
            model.UserName = model.UserName.Trim();
            if (model.UserName == null || model.Password == null || model.UserName.Trim() == "" || model.Password.Trim() == "")
            {

                Msg = "Thông tin đăng nhập không được để trống";
            }
            else
            {
                try
                {
                    USUsers Item = USUsersService.CheckLogin(model.UserName);
                    if (Item == null)
                    {
                        Msg = "Tài khoản hoặc mật khẩu không chính xác";
                    }
                    else if (Item.IdGroup == 1 || Item.IdGroup == 2)
                    {
                        bool validPassword = BCrypt.Net.BCrypt.Verify(model.Password + USUsersService.SecretPassword, Item.Password);
                        if (validPassword)
                        {

                            UserToken User = new UserToken()
                            {
                                Id = Item.Id,
                                UserName = Item.UserName,
                                Gender = Item.Gender,
                                Email = Item.Email
                            };
                            string token =  USUsersService.GenerateJsonWebToken(User, 540);

                            USInformation info = new USInformation()
                            {
                                Token = token,UserName = Item.UserName,Avatar = Item.Avatar, Gender = Item.Gender,TimeExpire = DateTime.UtcNow.AddMinutes(540)

                            };

                            HttpContext.Session.SetString("login_be_id", Item.Id.ToString());
                            HttpContext.Session.SetString("login_be_username", Item.UserName);
                            HttpContext.Session.SetString("login_be_fullname", Item.FullName);

                            return Json(new MsgSuccess() { Data = info });
                        }
                        else
                        {
                            Msg = "Tài khoản hoặc mật khẩu không chính xác";
                        }

                    }
                }
                catch (Exception e)
                {
                    Msg = "Tài khoản hoặc mật khẩu không chính xác."+ e.Message;
                }


            }
            return Json(new MsgError() { Msg = Msg });
        }

        public async Task<IActionResult> Index()
        {
            UserToken User = new UserToken()
            {
                UserName = "phucbv.dlc",
                Email = "phucbv.dlc@vnpt.vn"
            };
            string token =  USUsersService.GenerateJsonWebToken(User, 120);
            return Json(new { token });
        }

        [HttpGet]
        public IActionResult ChangePassword()
        {
            
            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString() + "_" + HttpContext.Request.Headers["UserName"];
            ChangePassword Model = new ChangePassword() { };
            string login_be_id = HttpContext.Session.GetString("login_be_id");
            if (login_be_id != null && login_be_id != "")
            {
                Model = new ChangePassword()
                {
                    Id = int.Parse(login_be_id)
                };
                USUsers Item = USUsersService.GetItem(Model.Id, API.Models.Settings.SecretId + "Users");
                Model.Item = Item;

            }
            else {
                return Redirect("/");
            }

            return View(Model);
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ChangePassword(ChangePassword Model)
        {
            string login_be_id = HttpContext.Session.GetString("login_be_id");
            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString() + "_" + HttpContext.Request.Headers["UserName"];

            if (login_be_id != null && login_be_id != "")
            {
                int IdDC = Int32.Parse(login_be_id);
                string pass = USUsersService.GetMD5(Model.OldPassword);
                Model.NewPassword = Model.NewPassword.Trim();
                Model.RePassword = Model.RePassword.Trim();
                Boolean StrongPass = USUsersService.ValidateStrongPassword(Model.NewPassword);
                if (Model.Id == IdDC)
                {
                    if (StrongPass)
                    {
                        if (Model.NewPassword == Model.RePassword)
                        {
                            USUsers Item = USUsersService.GetItem(Model.Id);
                            bool validPassword = BCrypt.Net.BCrypt.Verify(Model.OldPassword + USUsersService.SecretPassword, Item.Password);
                            if (validPassword)
                            {
                                string new_pass = BCrypt.Net.BCrypt.HashPassword(Model.NewPassword + USUsersService.SecretPassword, BCrypt.Net.SaltRevision.Revision2A);
                                var result = USUsersService.ChangePassword(IdDC, new_pass);
                                TempData["MessageSuccess"] = "Thay đổi Mật khẩu thành công";
                                return Redirect("/");
                            }
                            TempData["MessageError"] = "Mật khẩu cũ không chính xác";

                        }
                        else
                        {
                            TempData["MessageError"] = "Mật khẩu mới và mật khẩu Nhập lại không giống nhau";
                        }
                    }
                    else
                    {
                        TempData["MessageError"] = "Mật khẩu quá đơn giản. Độ dài Mật khẩu phải lới hơn 7, có các ký tự đặc biệt";
                    }
                }
                else
                {
                    TempData["MessageError"] = "Thay đổi Mật khẩu Không thành công";
                }

            }
            else
            {
                return Redirect("/Users/Login");
            }

            return View(new ChangePassword());
        }


        public IActionResult Info()
        {
            USUsersModel data = new USUsersModel();
            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            
            string login_be_id = HttpContext.Session.GetString("login_be_id");

            if (login_be_id != null && login_be_id != "")
            {
                int IdDC = int.Parse(HttpContext.Session.GetString("login_be_id"));
                data.SearchData = new SearchUSUsers() { CurrentPage = 0, ItemsPerPage = 10, Keyword = "" };
                //data.ListItemsGroups = USGroupsService.GetListSelectItems();
                //data.ListItemsStatus = USUsersService.GetStatusSelectItems();
                data.ListDMChucVu = DMChucVuService.GetListSelectItems();
                if (IdDC == 0)
                {
                    data.Item = new USUsers();
                }
                else
                {
                    data.Item = USUsersService.GetItem(IdDC, API.Models.Settings.SecretId + ControllerName);
                }
            }
            else {
                return Redirect("/");
            }

            
            return View(data);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Info(USUsers model)
        {
            USUsersModel data = new USUsersModel() { Item = model };
            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            string login_be_id = HttpContext.Session.GetString("login_be_id");
            if (login_be_id != null && login_be_id != "")
            {
                int IdDC = int.Parse(login_be_id);                
                if (ModelState.IsValid)
                {
                    if (model.Id == IdDC)
                    {
                        if (model.Id > 0)
                        {
                            dynamic DataSave = USUsersService.SaveAccountInfoBE(model);
                            TempData["MessageSuccess"] = "Cập nhật thành công";
                        }

                    }
                }
                else
                {
                    TempData["MessageError"] = "Cập nhật Không thành công";
                }
                
                data.Item = USUsersService.GetItem(IdDC, API.Models.Settings.SecretId + ControllerName);
                
            }
            else { 
            
            }
            data.ListDMChucVu = DMChucVuService.GetListSelectItems();
            return View(data);

        }

        [HttpGet]
        [Authorize("Bearer")]
        public IActionResult GetUserByToken()
        {

            var userID = User.Claims.Where(a => a.Type == ClaimTypes.NameIdentifier).FirstOrDefault().Value;

            int userId2 = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);

            return Ok(userID);

        }

        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return Redirect("/");
        }

        [HttpPost]

        public async Task<IActionResult> UploadAvatar()
        {
            string dirPath =  "/uploads/avatar/";
            
            string Extension  = "";
            string filePath = "";
            string Size = "";
            Boolean flagSave = false;
            Boolean flagUpload = false;

            List<String> listMIMETypes = MIMETypesService.GetListTinymce();

            string login_be_id = HttpContext.Session.GetString("login_be_id");

            if (login_be_id != null && login_be_id != "")
            {
                for (int i = 0; i < Request.Form.Files.Count(); i++)
                {
                    if (Request.Form.Files[i].Length > 0)
                    {
                        filePath = dirPath + login_be_id+"_"+ Request.Form.Files[i].FileName;
                        Extension = Path.GetExtension(filePath);
                        Size = Request.Form.Files[i].Length.ToString();
                        for (int j = 0; j < listMIMETypes.Count(); j++)
                        {
                            if (Extension.ToLower() == listMIMETypes[j].ToLower())
                            {
                                flagSave = true;
                            }
                        }
                        if (flagSave)
                        {
                            if (System.IO.File.Exists(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot") + filePath))
                            {
                                System.IO.File.Delete(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot") + filePath);
                            }

                            using (var fileStream = new FileStream(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot") + filePath, FileMode.Create))
                            {
                                try
                                {
                                    flagUpload = true;
                                    await Request.Form.Files[i].CopyToAsync(fileStream);
                                    USUsers Item = new USUsers()
                                    {
                                        Id = Int32.Parse(login_be_id),
                                        Avatar = filePath
                                    };

                                    USUsersService.SaveAccountAvatar(Item);
                                }
                                catch
                                {
                                    flagUpload = false;
                                }
                            }
                        }
                    }
                }
                return Json(new MsgSuccess() { Success = flagUpload,Data = filePath });
            }
            else {
                return Json(new MsgError() { Msg = "Bạn không có quyền truy cập" });
            }
            


            


        }


    }
}
