﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using API.Areas.Admin.Models.NgaySanXuat;
using API.Areas.Admin.Models.NhaMay;
using API.Models;
using Microsoft.Extensions.Configuration;
namespace API.Controllers
{
    public class NgaySanXuatController : Controller
    {
        public IActionResult Index([FromQuery] SearchNgaySanXuat dto)
        {
            

            int TotalItems = 0;
            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            NgaySanXuatModel data = new NgaySanXuatModel() { SearchData = dto };
            data.ListItems = NgaySanXuatService.GetListPagination(data.SearchData, API.Models.Settings.SecretId + ControllerName);
            if (data.ListItems != null && data.ListItems.Count() > 0)
            {
                TotalItems = data.ListItems[0].TotalRows;
            }
            data.Pagination = new Areas.Admin.Models.Partial.PartialPagination() { CurrentPage = data.SearchData.CurrentPage, ItemsPerPage = data.SearchData.ItemsPerPage, TotalItems = TotalItems, QueryString = Request.QueryString.ToString() };
            data.ListItemsNhaMay = NhaMayService.GetListSelectItems();
            data.ListItemsTinhTrang = NgaySanXuatService.GetListItemsTinhTrang();

            return View(data);
        }

        public IActionResult Report()
        {
            DataTable data = API.Areas.Admin.Models.NgaySanXuat.NgaySanXuatService.GetReportFE();
            return Json(data);
        }
    }
}
