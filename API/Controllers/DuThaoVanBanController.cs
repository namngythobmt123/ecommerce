﻿using API.Areas.Admin.Models.DocumentsField;
using API.Areas.Admin.Models.DuThaoVanBan;
using API.Areas.Admin.Models.DuThaoVanBanGopY;
using API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    public class DuThaoVanBanController : Controller
    {
        private string controllerName = "DuThaoVanBanController";
        private string controllerSecret;
        private IConfiguration Configuration;       
        public DuThaoVanBanController(IConfiguration config)
        {
            Configuration = config;
            controllerSecret = config["Security:SecretId"] + controllerName;
        }
        public IActionResult Index([FromQuery] SearchDuThaoVanBan dto)
        {
            int TotalItems = 0;
            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            DuThaoVanBanModel data = new DuThaoVanBanModel() { SearchData = dto };
            
            data.ListItems = DuThaoVanBanService.GetListPagination(data.SearchData, controllerSecret);
            if (data.ListItems != null && data.ListItems.Count() > 0)
            {
                TotalItems = data.ListItems[0].TotalRows;
            }
            data.Pagination = new Areas.Admin.Models.Partial.PartialPagination() { CurrentPage = data.SearchData.CurrentPage, ItemsPerPage = data.SearchData.ItemsPerPage, TotalItems = TotalItems, QueryString = Request.QueryString.ToString() };
            data.ListDocumentsField = DocumentsFieldService.GetListSelectItems();

            return View(data);
        }

        public IActionResult DuThaoVanBanGopY(int Id, string Ids)
        {
            DuThaoVanBanGopYModel data = new DuThaoVanBanGopYModel()
            {
                SearchData = new SearchDuThaoVanBanGopY() { CurrentPage = 0, ItemsPerPage = 10, Keyword = "",IdDuThao= Id, IdsDuThao=Ids },
                Item = new DuThaoVanBanGopY() { IdDuThao = Id, IdsDuThao = Ids }
            };
            return View(data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DuThaoVanBanGopY(DuThaoVanBanGopY model)        
        {
            API.Models.MyHelper.Google Google = new Models.MyHelper.Google();
            Boolean CheckGoogle = Google.CheckGoogle(Configuration["RecaptchaSettings:SecretKey"], model.Token);

            DuThaoVanBanGopYModel data = new DuThaoVanBanGopYModel()
            {
                SearchData = new SearchDuThaoVanBanGopY() { CurrentPage = 0, ItemsPerPage = 10, Keyword = "" },
                Item = model
            };

            

            if (ModelState.IsValid && CheckGoogle)
            {
                model.Id = 0;
                model.CreatedBy = 0;
                model.ModifiedBy = 0;

                int IdDC = Int32.Parse(MyModels.Decode(model.IdsDuThao, controllerSecret).ToString());
                if (IdDC > 0)
                {

                    try
                    {
                        DuThaoVanBanGopYService.SaveItem(model);
                        TempData["MessageSuccess"] = "Gửi dự thảo văn bản thành công";
                    }
                    catch
                    {
                        TempData["MessageError"] = "Gửi dự thảo văn bản Thất bại. Xin vui lòng gửi lại";
                    }
                }
                else
                {
                    TempData["MessageError"] = "Gửi dự thảo văn bản Thất bại. Xin vui lòng gửi lại ...";
                }

                return RedirectToAction("Index");

            }
            
            return View(data);
        }
    }
}
