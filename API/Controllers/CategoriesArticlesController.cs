﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Areas.Admin.Models.CategoriesArticles;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    public class CategoriesArticlesController : Controller
    {
        public IActionResult Index(string alias, int id)
        {
            List<CategoriesArticles> ListItems = CategoriesArticlesService.GetListChild(id);
            
            CategoriesArticlesModel data = new CategoriesArticlesModel() { 
                ListItems = ListItems,
                Item = CategoriesArticlesService.GetItem(id)
            };
            return View(data);
        }
    }
}