﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using API.Areas.Admin.Models.CategoriesProducts;
using API.Areas.Admin.Models.Orders;
using API.Areas.Admin.Models.SYSParams;
using API.Areas.Admin.Models.Products;
using API.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.IO;
using API.Models.MyHelper;
using API.Areas.Admin.Models.OrdersProducts;
using API.Areas.Admin.Models.Employees;
using API.Areas.Admin.Models.Recruitment;
using DocumentFormat.OpenXml.EMMA;
using API.Areas.Admin.Models.DMPhuongXa;
using API.Areas.Admin.Models.DMQuanHuyen;
using API.Areas.Admin.Models.DMTinhThanh;

namespace API.Controllers
{
    public class ProductsController : Controller
    {
        public IActionResult Index(string alias, int id, [FromQuery] SearchProducts dto)
        {
            int TotalItems = 0;
            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            ProductsModel data = new ProductsModel() { SearchData = dto };
            data.SearchData.Status = 1;
            data.SearchData.CatId = id;
            data.CategoriesItem = CategoriesProductsService.GetItem(id);
            data.ListItems = ProductsService.GetListPagination(data.SearchData, API.Models.Settings.SecretId + ControllerName);
            data.ListItemsCategories = CategoriesProductsService.GetListItems();

            if (data.ListItems != null && data.ListItems.Count() > 0)
            {
                TotalItems = data.ListItems[0].TotalRows;
            }
            data.Pagination = new Areas.Admin.Models.Partial.PartialPagination() { CurrentPage = data.SearchData.CurrentPage, ItemsPerPage = data.SearchData.ItemsPerPage, TotalItems = TotalItems, QueryString = Request.QueryString.ToString() };

            return View(data);
        }

        public IActionResult CartList()
        {
            ProductsModel data = new ProductsModel() { SearchData = new SearchProducts(), Cart = new Cart() { Total = 0, Amount = 0 } };
            string str = HttpContext.Session.GetString("ListCart");
            List<Products> ListCart = new List<Products>();
            if (str != null && str != "")
            {
                ListCart = JsonConvert.DeserializeObject<List<Products>>(str);
            }
            data.ListItems = ListCart;

            return View(data);

        }

        public IActionResult UpdateCart([FromQuery] string Ids = null, int Quantity = 0, int Flag = 1)
        {
            int TotalProduct = 0;
            string str = HttpContext.Session.GetString("ListCart");
            List<Products> ListCart = new List<Products>();
            List<Products> ListCartNew = new List<Products>();
            if (str != null && str != "")
            {
                ListCart = JsonConvert.DeserializeObject<List<Products>>(str);
            }
            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            int IdDC = Int32.Parse(MyModels.Decode(Ids, API.Models.Settings.SecretId + ControllerName).ToString());
            if (IdDC > 0)
            {
                for (int i = 0; i < ListCart.Count(); i++)
                {
                    if (ListCart[i].Id == IdDC)
                    {
                        ListCart[i].Quantity = Quantity;
                    }
                    if (ListCart[i].Quantity > 0) {
                        ListCartNew.Add(ListCart[i]);
                    }
                    TotalProduct = TotalProduct + ListCart[i].Quantity;
                }
                HttpContext.Session.SetString("ListCart", JsonConvert.SerializeObject(ListCartNew));
                HttpContext.Session.SetString("TotalCart", TotalProduct.ToString());

                if (Flag == 1)
                {
                    return Json(new MsgSuccess() { Msg = "Cập nhật sản phẩm thành công", Data = TotalProduct });
                }
                else
                {
                    return Redirect("/gio-hang.html");
                }


            }
            else {
                if (Flag == 1)
                {
                    return Json(new MsgError() { Msg = "Sản phẩm đã bị khóa hoặc không tồn tại" });
                }
                else
                {
                    return Redirect("/gio-hang.html");
                }
            }

        }

        public IActionResult UpdateCart2([FromQuery] int Id = 0, int Quantity = 0, int Flag = 1)
        {
            int TotalProduct = 0;
            string str = HttpContext.Session.GetString("ListCart");
            List<Products> ListCart = new List<Products>();
            List<Products> ListCartNew = new List<Products>();
            if (str != null && str != "")
            {
                ListCart = JsonConvert.DeserializeObject<List<Products>>(str);
            }
            if (Id > 0)
            {
                for (int i = 0; i < ListCart.Count(); i++)
                {
                    if (ListCart[i].Id == Id)
                    {
                        ListCart[i].Quantity = Quantity;
                    }
                    if (ListCart[i].Quantity > 0)
                    {
                        ListCartNew.Add(ListCart[i]);
                    }
                    TotalProduct = TotalProduct + ListCart[i].Quantity;
                }
                HttpContext.Session.SetString("ListCart", JsonConvert.SerializeObject(ListCartNew));
                HttpContext.Session.SetString("TotalCart", TotalProduct.ToString());

                if (Flag == 1)
                {
                    return Json(new MsgSuccess() { Msg = "Cập nhật sản phẩm thành công", Data = TotalProduct });
                }
                else
                {
                    return Redirect("/gio-hang.html");
                }


            }
            else
            {
                if (Flag == 1)
                {
                    return Json(new MsgError() { Msg = "Sản phẩm đã bị khóa hoặc không tồn tại" });
                }
                else
                {
                    return Redirect("/gio-hang.html");
                }
            }

        }

        public IActionResult AddCart([FromQuery] string Ids = null, int Quantity = 0, int Flag = 1)
        {
            // Flag = 1; Adcart ajax; Flag=2; 

            int TotalProduct = 0;
            string str = HttpContext.Session.GetString("ListCart");
            List<Products> ListCart = new List<Products>();
            if (str != null && str != "")
            {
                ListCart = JsonConvert.DeserializeObject<List<Products>>(str);
            }
            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            int IdDC = Int32.Parse(MyModels.Decode(Ids, API.Models.Settings.SecretId + ControllerName).ToString());

            if (IdDC > 0)
            {
                Products Item = ProductsService.GetItem(IdDC, API.Models.Settings.SecretId + ControllerName);
                if (Item != null && Item.Id > 0)
                {
                    Boolean flagIsset = false;
                    for (int i = 0; i < ListCart.Count(); i++)
                    {
                        if (ListCart[i].Id == Item.Id)
                        {
                            flagIsset = true;
                            ListCart[i].Quantity = ListCart[i].Quantity + Quantity;
                        }
                        TotalProduct = TotalProduct + ListCart[i].Quantity;
                    }
                    Item.Quantity = Item.Quantity + Quantity;
                    if (!flagIsset)
                    {
                        Item.Quantity = Quantity;
                        ListCart.Add(Item);
                        TotalProduct = TotalProduct + Item.Quantity;
                    }
                }

                HttpContext.Session.SetString("ListCart", JsonConvert.SerializeObject(ListCart));
                HttpContext.Session.SetString("TotalCart", TotalProduct.ToString());
                if (Flag == 1)
                {
                    return Json(new MsgSuccess() { Msg = "Thêm sản phẩm thành công", Data = TotalProduct });
                }
                else
                {
                    return Redirect("/gio-hang.html");
                }

            }

            if (Flag == 1)
            {
                return Json(new MsgError() { Msg = "Sản phẩm đã bị khóa hoặc không tồn tại" });
            }
            else
            {
                return Redirect("/gio-hang.html");
            }


        }

        public IActionResult AddCartSearch([FromQuery] int Id = 0, int Quantity = 0, int Flag = 1)
        {
            // Flag = 1; Adcart ajax; Flag=2; 

            int TotalProduct = 0;
            string str = HttpContext.Session.GetString("ListCart");
            List<Products> ListCart = new List<Products>();
            if (str != null && str != "")
            {
                ListCart = JsonConvert.DeserializeObject<List<Products>>(str);
            }

            if (Id > 0)
            {
                Products Item = ProductsService.GetItem(Id);
                if (Item != null && Item.Id > 0)
                {
                    Boolean flagIsset = false;
                    for (int i = 0; i < ListCart.Count(); i++)
                    {
                        if (ListCart[i].Id == Item.Id)
                        {
                            flagIsset = true;
                            ListCart[i].Quantity = ListCart[i].Quantity + Quantity;
                        }
                        TotalProduct = TotalProduct + ListCart[i].Quantity;
                    }
                    Item.Quantity = Item.Quantity + Quantity;
                    if (!flagIsset)
                    {
                        Item.Quantity = Quantity;
                        ListCart.Add(Item);
                        TotalProduct = TotalProduct + Item.Quantity;
                    }
                }

                HttpContext.Session.SetString("ListCart", JsonConvert.SerializeObject(ListCart));
                HttpContext.Session.SetString("TotalCart", TotalProduct.ToString());
                if (Flag == 1)
                {
                    return Json(new MsgSuccess() { Msg = "Thêm sản phẩm thành công", Data = TotalProduct });
                }
                else
                {
                    return Redirect("/gio-hang.html");
                }

            }

            if (Flag == 1)
            {
                return Json(new MsgError() { Msg = "Sản phẩm đã bị khóa hoặc không tồn tại" });
            }
            else
            {
                return Redirect("/gio-hang.html");
            }


        }

        //public IActionResult AddCart([FromQuery] int Id = 0, int Quantity = 0, int Flag = 1)
        //{

        //    int TotalProduct = 0;
        //    string str = HttpContext.Session.GetString("ListCart");
        //    List<Products> ListCart = new List<Products>();
        //    if (str != null && str != "")
        //    {
        //        ListCart = JsonConvert.DeserializeObject<List<Products>>(str);
        //    }
        //        Products Item = ProductsService.GetItem(Id);
        //        if (Item != null && Item.Id > 0)
        //        {
        //            Boolean flagIsset = false;
        //            for (int i = 0; i < ListCart.Count(); i++)
        //            {
        //                if (ListCart[i].Id == Item.Id)
        //                {
        //                    flagIsset = true;
        //                    ListCart[i].Quantity = ListCart[i].Quantity + Quantity;
        //                }
        //                TotalProduct = TotalProduct + ListCart[i].Quantity;
        //            }
        //            Item.Quantity = Item.Quantity + Quantity;
        //            if (!flagIsset)
        //            {
        //                Item.Quantity = Quantity;
        //                ListCart.Add(Item);
        //                TotalProduct = TotalProduct + Item.Quantity;
        //            }
        //        }

        //        HttpContext.Session.SetString("ListCart", JsonConvert.SerializeObject(ListCart));
        //        HttpContext.Session.SetString("TotalCart", TotalProduct.ToString());
        //        if (Flag == 1)
        //        {
        //            return Json(new MsgSuccess() { Msg = "Thêm sản phẩm thành công", Data = TotalProduct });
        //        }
        //        else
        //        {
        //            return Redirect("/gio-hang.html");
        //        }

        //    if (Flag == 1)
        //    {
        //        return Json(new MsgError() { Msg = "Sản phẩm đã bị khóa hoặc không tồn tại" });
        //    }
        //    else
        //    {
        //        return Redirect("/gio-hang.html");
        //    }


        //}
        [HttpPost]
        public IActionResult AddCart([FromForm] Products dto)
        {
            // Flag = 1; Adcart ajax; Flag=2; 

            int TotalProduct = 0;
            string str = HttpContext.Session.GetString("ListCart");
            List<Products> ListCart = new List<Products>();
            if (str != null && str != "")
            {
                ListCart = JsonConvert.DeserializeObject<List<Products>>(str);
            }
            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            int IdDC = Int32.Parse(MyModels.Decode(dto.Ids, API.Models.Settings.SecretId + ControllerName).ToString());

            if (IdDC > 0)
            {
                Products Item = ProductsService.GetItem(IdDC, API.Models.Settings.SecretId + ControllerName);
                if (Item != null && Item.Id > 0)
                {
                    Boolean flagIsset = false;
                    for (int i = 0; i < ListCart.Count(); i++)
                    {
                        if (ListCart[i].Id == Item.Id)
                        {
                            flagIsset = true;
                            ListCart[i].Amounts = ListCart[i].Amounts + dto.Quantity;
                        }
                        TotalProduct = TotalProduct + ListCart[i].Amounts;
                    }
                    Item.Amounts = Item.Amounts + dto.Quantity;
                    if (!flagIsset)
                    {
                        ListCart.Add(Item);
                        TotalProduct = TotalProduct + Item.Amounts;
                    }
                }

                HttpContext.Session.SetString("ListCart", JsonConvert.SerializeObject(ListCart));
                HttpContext.Session.SetString("TotalCart", TotalProduct.ToString());
                return Redirect("/gio-hang.html");

            }

            return Redirect("/gio-hang.html");


        }

        public IActionResult GetListQuanHuyenByJson([FromQuery] int IdTinhThanh = 0)
        {
            List<DMQuanHuyen> ListItems = DMQuanHuyenService.GetListByTinhThanh(true, IdTinhThanh);
            return Json(ListItems);
        }

        public IActionResult GetListPhuongXaByJson([FromQuery] int IdQuanHuyen = 0)
        {
            List<DMPhuongXa> ListItems = DMPhuongXaService.GetListByQuanHuyen(true, IdQuanHuyen);
            return Json(ListItems);
        }
        public IActionResult CheckOut(string alias, int id, SearchOrders dto)
        {
            OrdersModel data = new OrdersModel() { Item = new Orders() { } };
            string str = HttpContext.Session.GetString("ListCart");
            List<Products> ListCart = new List<Products>();
            if (str != null && str != "")
            {
                ListCart = JsonConvert.DeserializeObject<List<Products>>(str);
            }
            data.ListCart = ListCart;
            data.SearchData = dto;

            data.ListItemsTinhThanh = DMTinhThanhService.GetListSelectItems();
            //data.ListItemsQuanHuyen = DMQuanHuyenService.GetListSelectItems(data.SearchData.IdTinhThanh);
            //data.ListItemsPhuongXa = DMPhuongXaService.GetListSelectItems(data.SearchData.IdQuanHuyen);

            if (data.ListCart == null || data.ListCart.Count() == 0)
            {
                return Redirect("/gio-hang.html");
            }
           
            return View(data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CheckOut(Orders dto, SYSConfig dto2)
        {
            OrdersModel data = new OrdersModel() { Item = dto };
            SYSConfigModel data2 = new SYSConfigModel() { Item  = dto2 };
            data2.Item = SYSParamsService.GetItemConfigByHome();
            string str = HttpContext.Session.GetString("ListCart");
            List<Products> ListCart = new List<Products>();
            if (str != null && str != "")
            {
                ListCart = JsonConvert.DeserializeObject<List<Products>>(str);
            }
            data.ListCart = ListCart;

            if (ModelState.IsValid)
            {
                double AmountAll = 0;
                if (data.ListCart != null || data.ListCart.Count() > 0)
                {
                    DataTable tbItem = new DataTable();
                    tbItem.Columns.Add("OrderId", typeof(int));
                    tbItem.Columns.Add("ProductId", typeof(int));
                    tbItem.Columns.Add("Price", typeof(float));
                    tbItem.Columns.Add("Quantity", typeof(int));
                    tbItem.Columns.Add("Total", typeof(float));
                    tbItem.Columns.Add("TotalDisplay", typeof(string));

                    for (int i = 0; i < data.ListCart.Count(); i++)
                    {
                        var row = tbItem.NewRow();
                        row["OrderId"] = 0;
                        row["ProductId"] = ListCart[i].Id;
                        row["Price"] = ListCart[i].Price;
                        row["Quantity"] = ListCart[i].Quantity;
                        row["Total"] = ListCart[i].Quantity * ListCart[i].Price;
                        row["TotalDisplay"] = API.Models.MyHelper.StringHelper.ConvertNumberToDisplay((ListCart[i].Quantity * ListCart[i].Price).ToString());
                        tbItem.Rows.Add(row);
                        AmountAll = AmountAll + ListCart[i].Quantity * ListCart[i].Price;
                    }
                    dto.Amount = AmountAll;
                    dto.Total = AmountAll + dto.ShipFee;

                    DMPhuongXa CheckPhuongXa = new DMPhuongXa();
                    DMQuanHuyen CheckQuanHuyen = new DMQuanHuyen();
                    DMTinhThanh CheckTinhThanh = new DMTinhThanh();
                    CheckPhuongXa = DMPhuongXaService.GetItem(data.Item.IdPhuongXa);
                    CheckQuanHuyen = DMQuanHuyenService.GetItem(data.Item.IdQuanHuyen);
                    CheckTinhThanh = DMTinhThanhService.GetItem(data.Item.IdTinhThanh);

                    if (data.Item.MaNV != null)
                    {
                        Employees CheckMaNV = new Employees();
                        CheckMaNV = EmployeesService.GetItem(data.Item.MaNV);
                        if (CheckMaNV != null)
                        {
                            dto.Total = (AmountAll - ((CheckMaNV.ChietKhau * AmountAll) / 100)) + dto.ShipFee;
                            dto.MaNV = CheckMaNV.MaNV;
                            dto.TenNV = CheckMaNV.Fullname;
                            dto.PhoneNV = CheckMaNV.Phone;
                            dto.ChietKhau = CheckMaNV.ChietKhau;

                            dto.TinhThanh = CheckTinhThanh.Ten;
                            dto.QuanHuyen = CheckQuanHuyen.Ten;
                            dto.PhuongXa = CheckPhuongXa.Ten;

                            dynamic DataSave = OrdersService.SaveItem(data.Item, tbItem);
                            HttpContext.Session.SetString("TotalCart", "0");
                            HttpContext.Session.SetString("ListCart", "");

                            //Send Email


                            IConfiguration _config = Startup._config;
                            MailSettings settings = new MailSettings()
                            {
                                Host = _config["MailSettings:Host"],
                                Port = Int32.Parse(_config["MailSettings:Port"]),
                                Password = _config["MailSettings:Password"],
                                DisplayName = _config["MailSettings:DisplayName"],
                                Mail = _config["MailSettings:Mail"],
                            };
                            string linkEmail = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TemplatesReport/email_thanhtoan.html");
                            string textBody = System.IO.File.ReadAllText(linkEmail);
                            string HtmlChild = OrdersService.ConvertToHtmlChild(ListCart, dto.ChietKhau);

                            textBody = OrdersService.ConvertBodyEmail(data.Item, textBody, HtmlChild);

                            MailRequest request = new MailRequest()
                            {
                                ToEmail = data2.Item.Email,
                                Subject = "Crop Tây Nguyên - Đơn hàng mới #" + DataSave.OrderCode,
                                Body = textBody,
                            };
                            await MyEmail.SendEmailAsync(settings, request);

                            return Redirect("/Products/Thanks/" + DataSave.OrderCode);
                        }
                        else
                        {
                            TempData["MessageError"] = "Sai thông tin mã giới thiệu";
                            data.ListItemsTinhThanh = DMTinhThanhService.GetListSelectItems();
                            if (data.Item.IdTinhThanh > 0)
                            {
                                data.ListItemsQuanHuyen = DMQuanHuyenService.GetListSelectItems(data.Item.IdTinhThanh);
                                if (data.Item.IdQuanHuyen > 0)
                                {
                                    data.ListItemsPhuongXa = DMPhuongXaService.GetListSelectItems(data.Item.IdQuanHuyen);
                                }
                            }
                            return View(data);

                        }

                    }
                    else
                    {
                        dto.TinhThanh = CheckTinhThanh.Ten;
                        dto.QuanHuyen = CheckQuanHuyen.Ten;
                        dto.PhuongXa = CheckPhuongXa.Ten;

                        dynamic DataSave = OrdersService.SaveItem(data.Item, tbItem);
                        HttpContext.Session.SetString("TotalCart", "0");
                        HttpContext.Session.SetString("ListCart", "");

                        //Send Email


                        IConfiguration _config = Startup._config;
                        MailSettings settings = new MailSettings()
                        {
                            Host = _config["MailSettings:Host"],
                            Port = Int32.Parse(_config["MailSettings:Port"]),
                            Password = _config["MailSettings:Password"],
                            DisplayName = _config["MailSettings:DisplayName"],
                            Mail = _config["MailSettings:Mail"],
                        };
                        string linkEmail = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TemplatesReport/email_thanhtoan.html");
                        string textBody = System.IO.File.ReadAllText(linkEmail);
                        string HtmlChild = OrdersService.ConvertToHtmlChild(ListCart,0);

                        textBody = OrdersService.ConvertBodyEmail(data.Item, textBody, HtmlChild);

                        MailRequest request = new MailRequest()
                        {
                            ToEmail = data2.Item.Email,
                            Subject = "Crop Tây Nguyên - Đơn hàng mới #" + DataSave.OrderCode,
                            Body = textBody,
                        };
                        await MyEmail.SendEmailAsync(settings, request);

                        return Redirect("/Products/Thanks/" + DataSave.OrderCode);

                    }
                    //dynamic DataSave = OrdersService.SaveItem(data.Item,tbItem);
                    //HttpContext.Session.SetString("TotalCart", "0");

                    //Send Email


                    //IConfiguration _config = Startup._config;
                    //MailSettings settings = new MailSettings()
                    //{
                    //    Host = _config["MailSettings:Host"],
                    //    Port = Int32.Parse(_config["MailSettings:Port"]),
                    //    Password = _config["MailSettings:Password"],
                    //    DisplayName = _config["MailSettings:DisplayName"],
                    //    Mail = _config["MailSettings:Mail"],
                    //};
                    //string linkEmail = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TemplatesReport/email_thanhtoan.html");
                    //string textBody = System.IO.File.ReadAllText(linkEmail);
                    //string HtmlChild = OrdersService.ConvertToHtmlChild(ListCart);

                    //textBody = OrdersService.ConvertBodyEmail(data.Item, textBody, HtmlChild);

                    //MailRequest request = new MailRequest()
                    //{
                    //    ToEmail = data2.Item.Email,
                    //    Subject = "Crop Tây Nguyên - Đơn hàng mới #" + DataSave.OrderCode,
                    //    Body = textBody,
                    //};
                    //await MyEmail.SendEmailAsync(settings, request); 

                    //return Redirect("/Products/Thanks/" + DataSave.OrderCode);
                }
            }
            else
            {
                data.ListItemsTinhThanh = DMTinhThanhService.GetListSelectItems();
                if (data.Item.IdTinhThanh > 0)
                {
                    data.ListItemsQuanHuyen = DMQuanHuyenService.GetListSelectItems(data.Item.IdTinhThanh);
                    if (data.Item.IdQuanHuyen > 0)
                    {
                        data.ListItemsPhuongXa = DMPhuongXaService.GetListSelectItems(data.Item.IdQuanHuyen);
                    }
                }
            }
                
            return View(data);
        }
        public IActionResult Detail(string alias, int id)
        {
            ProductsModel data = new ProductsModel();
            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            data.SearchData = new SearchProducts() { CurrentPage = 0, ItemsPerPage = 10, Keyword = "" };
            data.Item = ProductsService.GetItem(id, API.Models.Settings.SecretId + ControllerName);
            data.Item.Amounts = 1;


            data.ListItemsCategories = CategoriesProductsService.GetListItems();
            return View(data);
        }

        public IActionResult UpdateQuantity([FromQuery] string Ids, int Quantity)
        {

            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            int TotalProduct = 0;
            string str = HttpContext.Session.GetString("ListCart");
            List<Products> ListCart = new List<Products>();
            if (str != null && str != "")
            {
                ListCart = JsonConvert.DeserializeObject<List<Products>>(str);
            }

            int IdDC = Int32.Parse(MyModels.Decode(Ids, API.Models.Settings.SecretId + ControllerName).ToString());


            if (IdDC > 0)
            {
                Products Item = ProductsService.GetItem(IdDC, API.Models.Settings.SecretId + ControllerName);
                if (Item != null && Item.Id > 0)
                {

                    for (int i = 0; i < ListCart.Count(); i++)
                    {
                        if (ListCart[i].Id == Item.Id)
                        {
                            ListCart[i].Quantity = Quantity;
                        }
                        TotalProduct = TotalProduct + ListCart[i].Quantity;
                    }

                }
            }
            HttpContext.Session.SetString("ListCart", JsonConvert.SerializeObject(ListCart));
            HttpContext.Session.SetString("TotalCart", TotalProduct.ToString());

            return Json(new MsgSuccess() { Data = TotalProduct, Msg = "Cập nhật  sản phẩm thành công" });
        }

        public IActionResult Thanks(string Id)
        {

            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            Orders Item = OrdersService.GetItemByCode(Id, API.Models.Settings.SecretId + ControllerName);
            OrdersModel data = new OrdersModel() { Item = new Orders() { OrderCode = Id } };

            if (Item != null && Item.Id > 0)
            {
                data = new OrdersModel()
                {
                    Item = Item,
                    ListOrdersProducts = OrdersProductsService.GetListPagination(new SearchOrdersProducts() { OrderId = Item.Id }, API.Models.Settings.SecretId + ControllerName)
                };
            }
                            
            return View(data);
        }
    }
}