﻿using Microsoft.AspNetCore.Mvc;

using API.Areas.Admin.Models.PostMessage;
using System;
using System.IO;
using API.Areas.Admin.Models.ManagerFiles;
using API.Areas.Admin.Models.ManagerFile;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Security.Cryptography;
using Microsoft.Extensions.Configuration;

namespace API.Controllers
{
    public class PostMessageController : Controller
    {
        private IConfiguration Configuration;
        public PostMessageController(IConfiguration config)
        {
            Configuration = config;
        }
        public IActionResult Index()
        {
            PostMessageModel data = new PostMessageModel()
            {
                SearchData = new SearchPostMessage() { CurrentPage = 0, ItemsPerPage = 10, Keyword = "" },
                Item = new PostMessage()
            };
            return View(data);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(PostMessage model)
        {
            API.Models.MyHelper.Google Google = new Models.MyHelper.Google();
            Boolean CheckGoogle = Google.CheckGoogle(Configuration["RecaptchaSettings:SecretKey"], model.Token);
            string Size = "";
            string Extension = "";
            Boolean flagSave = false;
            List<String> listMIMETypes = MIMETypesService.GetListTinymce();
            List<ManagerFile.TinymceFile> ListFiles = new List<ManagerFile.TinymceFile>();

            PostMessageModel data = new PostMessageModel() { Item = model };

            if (ModelState.IsValid && CheckGoogle)
            {
                model.Id = 0;
                model.CreatedBy = 0;
                model.ModifiedBy = 0;

                try
                {
                    if (Request.Form.Files.Count > 0)
                    {

                        TempData["MessageSuccess"] = "Gửi tin bài thành công";
                        if (Request.Form.Files[0].Length > 0)
                        {

                            Extension = Path.GetExtension(Request.Form.Files[0].FileName);
                            Size = Request.Form.Files[0].Length.ToString();
                            for (int j = 0; j < listMIMETypes.Count; j++)
                            {
                                if (Extension.ToLower() == listMIMETypes[j].ToLower())
                                {
                                    flagSave = true;
                                }
                            }
                            if (flagSave)
                            {// Được quyền upload


                                string NewFile = DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + API.Models.MyHelper.StringHelper.UrlFriendly(Request.Form.Files[0].FileName.Replace(Extension, ""));

                                model.LinkFile = NewFile + Extension;

                                string filePath = Path.Combine(Directory.GetCurrentDirectory(), "Upload_files/") + model.LinkFile;

                                using (var fileStream = new FileStream(filePath, FileMode.Create))
                                {
                                    await Request.Form.Files[0].CopyToAsync(fileStream);
                                }
                                PostMessageService.SaveItem(model);
                                return RedirectToAction("Index");
                            }
                            else
                            {
                                TempData["MessageError"] = "Kiểu file không hợp lệ. Bạn chỉ gửi được file .Pdf, .doc,.docx";
                            }
                        }
                        else
                        {
                            TempData["MessageError"] = "Dung lượng file upload không được quá 30M";
                        }

                    }
                    TempData["MessageError"] = "Bạn chưa đính kèm file";
                }
                catch
                {
                    TempData["MessageError"] = "Gửi tin bài Thất bại. Xin vui lòng gửi lại";
                }

            }
            else {
                TempData["MessageError"] = "Gửi tin bài Thất bại. Xin vui lòng gửi lại";
            }

            return View(data);
        }
    }

    
}
