﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Areas.Admin.Models.Contacts;
using API.Areas.Admin.Models.DMLinhVuc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using API.Models;
using Microsoft.Extensions.Configuration;

namespace API.Controllers
{

    public class ContactsController : Controller
    {
        private string controllerName = "DuThaoVanBanController";
        private string controllerSecret;
        private IConfiguration Configuration;
        public ContactsController(IConfiguration config)
        {
            Configuration = config;
            controllerSecret = config["Security:SecretId"] + controllerName;
        }
        public IActionResult Index()
        {
            ContactsModel data = new ContactsModel() {
                SearchData = new SearchContacts() { CurrentPage = 0, ItemsPerPage = 10, Keyword = "" },
                Item = new Contacts()
            };                        
            return View(data);            
        }
       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Index(Contacts model)
        {
            API.Models.MyHelper.Google Google = new Models.MyHelper.Google();
            Boolean CheckGoogle = Google.CheckGoogle(Configuration["RecaptchaSettings:SecretKey"], model.Token);

            ContactsModel data = new ContactsModel() { Item = model };
            if (ModelState.IsValid && CheckGoogle)
            {
                model.Id = 0;
                model.CreatedBy = 0;
                model.ModifiedBy = 0;
                model.IdCoQuan = int.Parse(HttpContext.Session.GetString("DomainId"));
                try
                {
                    ContactsService.SaveItem(model);
                    TempData["MessageSuccess"] = "Gửi câu hỏi thành công";
                }
                catch
                {
                    TempData["MessageError"] = "Gửi câu hỏi Thất bại. Xin vui lòng gửi lại";

                }
                return RedirectToAction("Index");

            }
            return View(data);
        }

        public IActionResult Detail()
        {
            ContactsModel data = new ContactsModel()
            {
                SearchData = new SearchContacts() { CurrentPage = 0, ItemsPerPage = 10, Keyword = "" },
                Item = new Contacts()
            };
            data.ListLinhVuc = DMLinhVucService.GetListSelectItems();
            return View(data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Detail(Contacts model)
        {
            API.Models.MyHelper.Google Google = new Models.MyHelper.Google();
            Boolean CheckGoogle = Google.CheckGoogle(Configuration["RecaptchaSettings:SecretKey"], model.Token);

            ContactsModel data = new ContactsModel() { Item = model };
            if (ModelState.IsValid && CheckGoogle)
            {
                model.Id = 0;
                model.CreatedBy = 0;
                model.ModifiedBy = 0;
                model.IdCoQuan = int.Parse(HttpContext.Session.GetString("DomainId"));
                try
                {
                    ContactsService.SaveItem(model);
                    TempData["MessageSuccess"] = "Gửi câu hỏi thành công";
                }
                catch
                {
                    TempData["MessageError"] = "Gửi câu hỏi Thất bại. Xin vui lòng gửi lại";

                }
                return RedirectToAction("Detail");

            }
            data.ListLinhVuc = DMLinhVucService.GetListSelectItems();
            return View(data);
        }

        public IActionResult List([FromQuery] SearchContacts dto)
        {
            int TotalItems = 0;
            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            ContactsModel data = new ContactsModel() { SearchData = dto };
            data.ListItems = ContactsService.GetListPagination(data.SearchData, API.Models.Settings.SecretId + ControllerName,true);
            if (data.ListItems != null && data.ListItems.Count() > 0)
            {
                TotalItems = data.ListItems[0].TotalRows;
            }
            data.Pagination = new API.Areas.Admin.Models.Partial.PartialPagination() { CurrentPage = data.SearchData.CurrentPage, ItemsPerPage = data.SearchData.ItemsPerPage, TotalItems = TotalItems, QueryString = Request.QueryString.ToString() };
            data.ListLinhVuc = DMLinhVucService.GetListSelectItems();
            return View(data);
        }

    }
}