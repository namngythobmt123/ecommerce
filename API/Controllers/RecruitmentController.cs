﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Areas.Admin.Models.Recruitment;
using API.Areas.Admin.Models.DMLinhVuc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using API.Models;
using Microsoft.Extensions.Configuration;

namespace API.Controllers
{

    public class RecruitmentController : Controller
    {
        private string controllerName = "DuThaoVanBanController";
        private string controllerSecret;
        private IConfiguration Configuration;
        public RecruitmentController(IConfiguration config)
        {
            Configuration = config;
            controllerSecret = config["Security:SecretId"] + controllerName;
        }
        public IActionResult Index()
        {
            RecruitmentModel data = new RecruitmentModel()
            {
                SearchData = new SearchRecruitment() { CurrentPage = 0, ItemsPerPage = 10, Keyword = "" },
                Item = new Recruitment()
            };
            return View(data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Index(Recruitment model)
        {
            API.Models.MyHelper.Google Google = new Models.MyHelper.Google();
            Boolean CheckGoogle = Google.CheckGoogle(Configuration["RecaptchaSettings:SecretKey"], model.Token);

            RecruitmentModel data = new RecruitmentModel() { Item = model };
            if (ModelState.IsValid && CheckGoogle)
            {
                model.Id = 0;
                model.CreatedBy = 0;
                model.ModifiedBy = 0;
                model.IdCoQuan = int.Parse(HttpContext.Session.GetString("DomainId"));
                try
                {
                    RecruitmentService.SaveItem(model);
                    TempData["MessageSuccess"] = "Gửi Đăng ký tuyển dụng thành công";
                }
                catch
                {
                    TempData["MessageError"] = "Gửi Đăng ký tuyển dụng Thất bại. Xin vui lòng gửi lại";

                }
                return RedirectToAction("Index");

            }
            return View(data);
        }

    }
}