﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using API.Models;
using API.Areas.Admin.Models.DMChucVu;
using System.Data;
using API.Models.Home;
using API.Areas.Admin.Models.Products;
using API.Areas.Admin.Models.CategoriesProducts;
using API.Areas.Admin.Models.Articles;
using API.Areas.Admin.Models.Banners;
using Microsoft.Extensions.Configuration;
using BCrypt.Net;
using API.Areas.Admin.Models.Search;
using API.Areas.Admin.Models.NewsLetter;

namespace API.Controllers
{
    public class HomeController : Controller
    {
        private IConfiguration Configuration;
        private string controllerSecret;
        public HomeController(IConfiguration config)
        {
            Configuration = config;
            controllerSecret = config["Security:SecretId"] + "HomeController";
        }

        public IActionResult Index([FromQuery] string Keyword)
        {
            HomeModel dto = new HomeModel(){};
            dto.ListCatProduct = CategoriesProductsService.GetListFeaturedHome();
            dto.ListFeaturedProducts = ProductsService.GetListFeatured(0, 5, API.Models.Settings.SecretId + "Products");
            dto.ListNewProducts = ProductsService.GetListNew(0,5, API.Models.Settings.SecretId + "Products");
            dto.ListProductsByCat = ProductsService.GetListAllProductByCat(API.Models.Settings.SecretId + "Products");
            dto.ListFeaturedArticles = ArticlesService.GetListHot();
            dto.ListNewArticles = ArticlesService.GetListNew();
            dto.ListPartner = BannersService.GetListByCat(3);
            return View(dto);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult NewsLetter([FromBody] NewsLetter model)
        {
            API.Models.MyHelper.Google Google = new Models.MyHelper.Google();
            Boolean CheckGoogle = Google.CheckGoogle(Configuration["RecaptchaSettings:SecretKey"], model.Token);

            if (model.Email == null || model.Email == "")
            {
                return Json(new API.Models.MsgError() { Data = model.Email, Msg = "Email không được để trống" });
            }
            else
            {
                model.Email = model.Email.Trim();
                NewsLetter data = NewsLetterService.GetItemByEmail(model.Email);
                if (data == null)
                {
                    model.Title = "Khách tự đăng ký";
                    model.CreatedBy = 0;
                    model.ModifiedBy = 0;
                    var a = NewsLetterService.SaveItem(model);
                    return Json(new API.Models.MsgSuccess() { Data = model.Email, Msg ="Đăng ký thành công" });
                }
                else
                {
                    return Json(new API.Models.MsgSuccess() { Data = model.Email, Msg = "Đăng ký Không thành công" });
                }

            }

        }

        public IActionResult Search([FromQuery] string Keyword = "", int CurrentPage = 1)
        {
            int TotalItems = 0;
            SearchAll SearchAll = new SearchAll() { Keyword = Keyword, CurrentPage = CurrentPage };
            SearchAllModel data = new SearchAllModel()
            {
                SearchData = SearchAll,
                Flag = 1,
                ListItems = SearchService.GetListPagination(SearchAll)
            };
            if (data.ListItems != null && data.ListItems.Count() > 0)
            {
                TotalItems = data.ListItems[0].TotalRows;
            }
            data.Pagination = new Areas.Admin.Models.Partial.PartialPagination() { CurrentPage = data.SearchData.CurrentPage, ItemsPerPage = data.SearchData.ItemsPerPage, TotalItems = TotalItems, QueryString = Request.QueryString.ToString() };
            return View(data);
        }

        public IActionResult SearchProducts([FromQuery] SearchProducts dto)
        {
            int TotalItems = 0;
            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            
            ProductsModel data = new ProductsModel() { SearchData = dto };
            data.SearchData.Status = 1;
                        
            data.ListItems = ProductsService.GetListPagination(data.SearchData, API.Models.Settings.SecretId + ControllerName);
            data.ListItemsCategories = CategoriesProductsService.GetListItems();

            if (data.ListItems != null && data.ListItems.Count() > 0)
            {
                TotalItems = data.ListItems[0].TotalRows;
            }
            data.Pagination = new Areas.Admin.Models.Partial.PartialPagination() { CurrentPage = data.SearchData.CurrentPage, ItemsPerPage = data.SearchData.ItemsPerPage, TotalItems = TotalItems, QueryString = Request.QueryString.ToString() };

            return View(data);
        }

        public IActionResult Login()
        {
            return View();
        }

        public IActionResult ThongBaoBaoTri()
        {

            return View();
        }
        public IActionResult LoiCauHinh()
        {            
            return View();
        }

        public IActionResult SiteMap()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            string SecretPassword = Configuration["Security:SecretPassword"];
            string Password = BCrypt.Net.BCrypt.HashPassword("Abc@123" + Configuration["Security:SecretPassword"], SaltRevision.Revision2A);

            
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
