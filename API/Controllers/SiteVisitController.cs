﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using API.Areas.Admin.Models.SiteVisit;
namespace API.Controllers
{
    public class SiteVisitController : Controller
    {
        public IActionResult Index()
        {
            SiteVisitDetail DateWeek = SiteVisitService.GetByDateWeek();
            SiteVisitDetail GetAll = SiteVisitService.GetAll();
            SiteVisitDetail DateNow = SiteVisitService.GetByDate(DateTime.Now);
            SiteVisitDetail LastDate = SiteVisitService.GetByDate(DateTime.Now.AddDays(-1));
            SiteVisitResult data = new SiteVisitResult()
            {
                Total  = String.Format("{0:#,###,###.##}", GetAll.Amount)
            };
            if (DateWeek != null) {
                data.DateOfWeek = String.Format("{0:#,###,###.##}", DateWeek.Amount);
            }
            if (DateNow != null) {
                data.DateNow = String.Format("{0:#,###,###.##}", DateNow.Amount); 
            }
            if (LastDate != null) {
                data.Yesterday = String.Format("{0:#,###,###.##}", LastDate.Amount); 
            }
           
            return Json(data);
        }
    }
}