﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Areas.Admin.Models.Ablums;
using API.Areas.Admin.Models.CategoriesAblums;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    public class AlbumsController : Controller
    {
        public IActionResult Index([FromQuery] SearchCategoriesAblums dto)
        {            
            CategoriesAblumsModel data = new CategoriesAblumsModel() { SearchData = dto };           
            data.ListItems = CategoriesAblumsService.GetList();            
            return View(data);
        }

        public IActionResult Detail(string alias, int id)
        {
            AblumsModel data = new AblumsModel();
            data.ListItems = AblumsService.GetList(id);
            data.CatItem = CategoriesAblumsService.GetItem(id);
            return View(data);
        }
    }
}