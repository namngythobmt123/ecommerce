﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models.MyHelper
{
    public class Google
    {
        

        public Boolean CheckGoogle(string SecretKey, string Token)
        {
            Boolean flag = false;

            string link = "https://www.google.com/recaptcha/api/siteverify?secret=" + SecretKey + "&response=" + Token;
            var client = new RestClient(link);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);

            API.Models.Google google = JsonConvert.DeserializeObject<API.Models.Google>(response.Content);

            if (google.success == true)
            {
                flag = true;
            }
            return flag;
        }
    }
}
