﻿var vmBackendOrders = new Vue({
    el: '#app_backend_orders',
    data: {
        title: "Quản lý Vé",
        SearchItems: { CurrentPage: 1, ItemsPerPage: 10, Keyword: '', msg: '', recharge_type: -1, Day: 0, Month: 0, Year: 0, IdSearch:0},                
        msgInfo: "",
        ListItems: [],        
        detailItem: { FullName: "", FullName: '', Ids: '', Id: 0},
        deleteItem: { FullName: "", FullName: '', Ids: '', Id: 0},
        flag: false,
        flagLoading: false,        
        flagLoadingChild: false,
        pageCount: 0,
        TotalRows: 0,
        ListDay: [],
        ListYear: [],
        ListMonth: [],
        now: new Date(),
        
    },
    created() {
        $(".app-vue").show();
        this.getListItems();
        var date = new Date();
        this.SearchItems.Day = date.getDate(); //+ "/"+(date.getMonth()+1)+"/"+date.getFullYear();   
        this.SearchItems.Year = date.getFullYear();
        this.SearchItems.Month = date.getMonth() + 1;
       
        for (let i = 1; i < 13; i++) {
            this.ListMonth.push(i);
        }
        for (let i = 1; i < 32; i++) {
            this.ListDay.push(i);
        }
        for (let i = 2020; i <= this.now.getFullYear(); i++) {
            this.ListYear.push(i);
        }



    },
    methods: {
        UpdateStatus: function () {
            var token = jQuery('input[name="__RequestVerificationToken"]').val();
            var headers = {};
            headers["RequestVerificationToken"] = token;
            this.detailItem.StatusId = 1;
            this.flagLoadingChild = true;
            return $.ajax({
                type: "POST",
                url: '/Admin/Orders/UpdateStatus/' + this.detailItem.Ids,
                headers: headers,
                data: JSON.stringify({ "Ids": this.detailItem.Ids }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    vmBackendOrders.getListItems();
                    vmBackendOrders.flagLoadingChild = false;
                    $("#UpdateStatusModal").modal("hide");
                },
                error: function () {
                    vmBackendOrders.flagLoadingChild = false;
                    vmBackendOrders.getListItems();
                }
            });
        },
        showUpdateStatus(i) {
            this.detailItem = this.ListItems[i];
            $("#UpdateStatusModal").modal("show");           
        },
       
        DownloadTicket: function (Ids) {
            return $.ajax({
                type: "POST",
                url: '/Admin/Orders/DownloadTicket/' + Ids,
                headers: headers,
                data: JSON.stringify({ "Ids": Ids }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    vmBackendOrders.flagLoadingChild = false;
                    axios({
                        url: Ids,
                        method: 'GET',
                        responseType: 'blob',
                    }).then((response) => {
                        var fileURL = window.URL.createObjectURL(new Blob([data]));
                        var fileLink = document.createElement('a');
                        fileLink.href = fileURL;
                        fileLink.setAttribute('download', link);
                        document.body.appendChild(fileLink);
                        fileLink.click();
                    });        
                },
                error: function () {

                    vmBackendOrders.flagLoadingChild = false;
                }
            });

        },
        showDeleted: function (item) {
            msgInfo = "";
            this.deleteItem = item;
            $("#DeletedModal").modal("show");

        },
        GeneralTicket: function (item) {
            msgInfo = "";
            var token = jQuery('input[name="__RequestVerificationToken"]').val();
            this.detailItem = item;
            var headers = {};
            headers["RequestVerificationToken"] = token;
            this.flagLoadingChild = true;
            return $.ajax({
                type: "POST",
                url: '/Admin/Orders/GeneralTicket/' + item.Ids,
                headers: headers,
                data: JSON.stringify({ "Ids": item.Ids }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    
                    axios({
                        url: '/Admin/Orders/DownloadTicket/' + item.Ids,
                        method: 'GET',
                        responseType: 'blob',
                    }).then((response) => {
                        var fileURL = window.URL.createObjectURL(new Blob([response.data]));
                        var fileLink = document.createElement('a');
                        fileLink.href = fileURL;
                        fileLink.setAttribute('download', item.OrderCode.trim()+'.zip');
                        document.body.appendChild(fileLink);
                        fileLink.click();
                    });     

                    vmBackendOrders.flagLoadingChild = false;
                },
                error: function () {
                    
                    vmBackendOrders.flagLoadingChild = false;
                }
            });

        },
        DeletedItem: function () {
            msgInfo = "";
            var token = jQuery('input[name="__RequestVerificationToken"]').val();            
            var headers = {};
            headers["RequestVerificationToken"] = token;
            this.flagLoadingChild = true;
            return $.ajax({
                type: "POST",
                url: '/Admin/Orders/DeleteItem/' + this.deleteItem.Ids,
                headers: headers,
                data: JSON.stringify({ "Ids": this.deleteItem.Ids }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    if (result.Success) {
                        vmBackendOrders.getListItems();
                        vmBackendOrders.flagLoadingChild = false;
                        $("#DeletedModal").modal("hide");
                    } else {
                        msgInfo = result.Msg;
                    }
                },
                error: function () {
                    vmBackendOrders.flagLoadingChild = false;
                    vmBackendOrders.getListItems();
                }
            });

        },
        getListItems: function () {
            this.ListItems = [];
            this.flagLoading = true;
            $.ajax({
                url: '/Admin/Orders/GetListItems',
                type: 'POST',
                data: JSON.stringify(this.SearchItems),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    if (result.Success) {
                        vmBackendOrders.ListItems = result.Data.ListItems;
                        if (vmBackendOrders.ListItems.length > 0) {
                            vmBackendOrders.TotalRows = (vmBackendOrders.ListItems[0]).TotalRows;
                            vmBackendOrders.pageCount = Math.ceil(vmBackendOrders.TotalRows / vmBackendOrders.SearchItems.ItemsPerPage);

                        } else {
                            vmBackendOrders.TotalRows = 0;
                            vmBackendOrders.pageCount = 0;
                        }
                    } else {
                        vmBackendOrders.TotalRows = 0;
                        vmBackendOrders.pageCount = 0;
                    }
                    vmBackendOrders.flagLoading = false;


                }
            });

            
        },
        pageChanged() {
            this.getListItems();
        },
        SearchChange() {

            this.SearchItems.CurrentPage = 1;
            this.getListItems();
        },
       
        ChangeKeyword(e) {
            console.log(e);
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) {
                this.SearchChange();
            }
        },
    }
});
Vue.component('paginate', VuejsPaginate);