﻿var vmBackendOrdersSV = new Vue({
    el: '#app_backend_orders_save',
    data: {
        title: "Quản lý Vé",
        SearchItems: { CurrentPage: 1, ItemsPerPage: 10, Keyword: '', msg: '',  OrderId: 0,Ids:"" },
        msgInfo: "",
        ListItems: [],
        detailItem: { FullName: "", FullName: '', Ids: '', Id: 0 },
      
        flag: false,
        flagLoading: false,
        flagLoadingChild: false,
        pageCount: 0,
        TotalRows: 0,      
    },
    created() {
        console.log($("#Item_Ids").val());
        this.SearchItems.Ids = $("#Item_Ids").val();
        $(".app-vue").show();
        this.getListItems();
        
    },
    methods: {
       
        getListItems: function () {
            this.ListItems = [];
            this.flagLoading = true;
            $.ajax({
                url: '/Admin/Orders/GetListOrdersProducts',
                type: 'POST',
                data: JSON.stringify(this.SearchItems),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    if (result.Success) {
                        vmBackendOrdersSV.ListItems = result.Data;
                        if (vmBackendOrdersSV.ListItems.length > 0) {
                            vmBackendOrdersSV.TotalRows = (vmBackendOrdersSV.ListItems[0]).TotalRows;
                            vmBackendOrdersSV.pageCount = Math.ceil(vmBackendOrdersSV.TotalRows / vmBackendOrdersSV.SearchItems.ItemsPerPage);

                        } else {
                            vmBackendOrdersSV.TotalRows = 0;
                            vmBackendOrdersSV.pageCount = 0;
                        }
                    } else {
                        vmBackendOrdersSV.TotalRows = 0;
                        vmBackendOrdersSV.pageCount = 0;
                    }
                    vmBackendOrdersSV.flagLoading = false;


                }
            });


        },
        pageChanged() {
            this.getListItems();
        },
        SearchChange() {

            this.SearchItems.CurrentPage = 1;
            this.getListItems();
        },

        ChangeKeyword(e) {
            console.log(e);
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) {
                this.SearchChange();
            }
        },
    }
});
Vue.component('paginate', VuejsPaginate);