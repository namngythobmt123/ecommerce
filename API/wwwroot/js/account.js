﻿const AccountModal = {
    data() {
        return {
            message: 'Hello Vue.js!',
            detailItem: { UserName: "phucbv.dlc", Password: 'Abc@123' },
            flagLoading: false
        }
    },
    created() {
        $("#account_modal").show();
        console.log("Token", localStorage.getItem("token"));
    },
    methods: {
        UploadAvatar() {
            var url = "/Account/UploadAvatar";
           
            this.flagLoading = true;
            var fd = new FormData();
            var files = $('#inputFile')[0].files[0];
            fd.append('file', files);

            $.ajax({
                url: url,
                type: "POST",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,
                enctype: 'multipart/form-data',
                success: function (result) {
                    vmAccount.flagLoading = false;
                    if (result.Success == true) {
                        
                    } else {
                        location.reload();
                    }                    
                },
                error: function () {
                    location.reload();
                }
            });
        },
        readURL() {
            var input = $('#inputFile')[0].files[0];
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#image_upload_preview').attr('src', e.target.result);
                
            }
            reader.readAsDataURL(input);
            
        }

    }
}

var vmAccount = Vue.createApp(AccountModal).mount('#account_modal');



