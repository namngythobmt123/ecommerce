﻿function SendNewsLetter()
{
    var token = jQuery('#NewsLetterForm input[name="__RequestVerificationToken"]').val();
    var headers = {};
    headers["RequestVerificationToken"] = token;

    var Email = $("#newsletter-email").val();
    
    if (Email == null || Email == "") {
        ResetFormNewsLetter();
        $("#newsletter-info").html("Email không được để trống");
        $("#newsletter-info").show();

    } else {
        $("#newsletter-send").hide();
        $("#newsletter-nosend").show();
        $.ajax({
            url: '/Home/NewsLetter',
            headers: headers,
            type: 'POST',
            data: JSON.stringify(Email),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                ResetFormNewsLetter();
                if (data.Success == true) {
                    $('#thanks-popup').modal('show');
                } else {
                    $("#newsletter-info").html(data.Msg);
                    $("#newsletter-info").show();
                }
                
            },
            error: function () {
                ResetFormNewsLetter();
            }
        });
    }
   
    
}

function ResetFormNewsLetter() {
    $("#newsletter-send").show();
    $("#newsletter-nosend").hide();
    $("#newsletter-info").hide();
}

