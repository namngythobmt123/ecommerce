﻿const vmLoginApp = {
    data() {        
        return {
            message: 'Hello Vue.js!',
            detailItem: { UserName: "", Password: '' },
            flagLoading:false
        }
    },
    created() {
        
        console.log("Token",localStorage.getItem("token"));
    },
    methods: {
        login() {
            var token = jQuery('input[name="__RequestVerificationToken"]').val();
            var headers = {};
            headers["RequestVerificationToken"] = token;
            
            this.flagLoading = true;
            return $.ajax({
                type: "POST",
                url: '/Account/Login',
                headers: headers,
                data: JSON.stringify(this.detailItem),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {     
                    vmLogin.flagLoading = false;
                    if (result.Success == true) {
                        
                        localStorage.setItem("token", result.Data.Token);
                        window.location.href = "/Account/Info";

                    } else {

                    }
                    
                    //$("#UpdateStatusModal").modal("hide");
                },
                error: function () {
                    vmLogin.flagLoading = false;
                    
                }
            });
        }
    }
}

var vmLogin = Vue.createApp(vmLoginApp).mount('#vm_login');
