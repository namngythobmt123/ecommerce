﻿$(document).ready(function () {
    return $.ajax({
        type: "GET",
        url: "/NgaySanXuat/Report",
        data: JSON.stringify({}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $("#NgaySXShow").html(data[0].NgaySXShow);
                $("#CongXuat").html(data[0].CongXuat);
                $("#SanLuong").html(new Intl.NumberFormat('de-DE').format(data[0].SanLuong));
                $("#SXNgay").html(data[0].SXNgay);
                $("#SXThang").html(new Intl.NumberFormat('de-DE').format(data[0].SXThang));
                $("#SXNam").html(new Intl.NumberFormat('de-DE').format(data[0].SXNam));
                $("#SXMucNuocHo").html(data[0].SXMucNuocHo);
                $("#MNDBT").html(data[0].MNDBT);
                $("#MucNuocHienTai").html(data[0].MucNuocHienTai);
                $("#MNC").html(data[0].MNC);
                $("#TinhTrangThietBi").html(data[0].TinhTrangThietBi);
                $("#GhiChu").html(data[0].GhiChu);
            }



        },
        error: function () {

        }
    });
});