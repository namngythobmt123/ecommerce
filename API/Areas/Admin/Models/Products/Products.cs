﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using API.Areas.Admin.Models.Partial;
using API.Areas.Admin.Models.CategoriesProducts;
using Microsoft.AspNetCore.Mvc.Rendering;
/*https://docs.microsoft.com/en-us/aspnet/core/tutorials/razor-pages/validation?view=aspnetcore-2.2*/
namespace API.Areas.Admin.Models.Products
{
    public class Products
    {
        public int Id { get; set; }

        [Display(Name = "Tên sản phẩm")]
        [StringLength(60, MinimumLength = 3, ErrorMessage = "Độ dài chuỗi phải lớn hơn {2} Không quá {1} ký tự")]
        [Required(ErrorMessage = "Tên sản phẩm không được để trống")]
        public string Title { get; set; }
        public string Alias { get; set; }
        public string Image { get; set; }
        public string Sku { get; set; }
        public double Price { get; set; }
        public double PriceDeal { get; set; }
        public double Discount { get; set; }
        public int Quantity { get; set; }
        public string Description { get; set; }
        public string Introtext { get; set; }
        public string PriceDisplay { get; set; }
        public string PriceDealDisplay { get; set; }
        public Boolean Status { get; set; }
        public Boolean Featured { get; set; }
        public DateTime PublishUp { get; set; } = DateTime.Now;
        public string PublishUpShow { get; set; } = DateTime.Now.ToString("dd/MM/yyyyy");
        public string Ids { get; set; }
        public int TotalRows { get; set; } = 0;
        public int CreatedBy { get; set; } = 0;
        public int ModifiedBy { get; set; } = 0;
        public string Metadesc { get; set; }
        public string Metakey { get; set; }
        public string Metadata { get; set; }
        public List<int> CatId { get; set; }
        public int CategoryId { get; set; }
        public int Amounts { get; set; } = 0;
        public int Available { get; set; } = 0;
        public int Sold { get; set; } = 0;
        public int Star { get; set; } = 5;
        public API.Models.MetaData MetadataCV { get; set; }
    }

    public class ProductsModel {
        public List<Products> ListItems { get; set; }
        public List<CategoriesProducts.CategoriesProducts> ListCat { get; set; }
        public List<SelectListItem> ListItemsCategories { get; set; }
        public SearchProducts SearchData { get; set; }
        public Products Item { get; set; }
        public CategoriesProducts.CategoriesProducts CategoriesItem { get; set; }
        public PartialPagination Pagination { get; set; }
        public Cart Cart { get; set; }
    }

    public class Cart
    {
        public Double Total { get; set; } = 0;
        public int Amount { get; set; } = 0;
    }

    public class AddCart
    {
        public Double Total { get; set; } = 0;
        public int Amount { get; set; } = 0;
    }

    public class SearchProducts {
        public int CurrentPage { get; set; }
        public int ItemsPerPage { get; set; }
        public int CatId { get; set; }
        public int IdCoQuan { get; set; }
        public int AuthorId { get; set; }
        public int CreatedBy { get; set; }
        public string Keyword { get; set; }        
        public string KeywordMobile { get; set; }        
        public int Status { get; set; } = -1;
    }

  
}
