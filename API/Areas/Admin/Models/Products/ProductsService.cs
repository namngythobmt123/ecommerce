﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Areas.Admin.Models.Products;
using API.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;

namespace API.Areas.Admin.Models.Products
{
    public class ProductsService
    {
        
        public static List<Products> GetListPagination(SearchProducts dto, string SecretId)
        {
            if (dto.CurrentPage <= 0)
            {
                dto.CurrentPage = 1;
            }
            if (dto.ItemsPerPage <= 0)
            {
                dto.ItemsPerPage = 10;
            }
            if (dto.Keyword == null)
            {
                dto.Keyword = "";
            }

            string str_sql = "GetListPagination_Status";
            //string str_sql = "GetListPagination";
            Boolean Status = true;
            if (dto.Status == -1)
            {
                str_sql = "GetListPagination";
            }
            else if (dto.Status == 0)
            {
                Status = false;
            }
            if (dto.CatId == 0)
            {
                var tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Products",
                    new string[] { "@flag", "@CurrentPage", "@ItemsPerPage", "@Keyword" },
                    new object[] { "GetListPagination", dto.CurrentPage, dto.ItemsPerPage, dto.Keyword });

                if (tabl == null)
                {
                    return new List<Products>();
                }
                else
                {
                    return (from r in tabl.AsEnumerable()
                            select new Products
                            {
                                Id = (int)r["Id"],
                                Title = (string)r["Title"],
                                Status = (bool)r["Status"],
                                Featured = (bool)r["Featured"],
                                Introtext = (string)((r["Introtext"] == System.DBNull.Value) ? "" : r["Introtext"]),
                                Alias = (string)((r["Alias"] == System.DBNull.Value) ? "" : r["Alias"]),
                                Image = (string)((r["Image"] == System.DBNull.Value) ? "" : r["Image"]),
                                Sku = (string)((r["Sku"] == System.DBNull.Value) ? "" : r["Sku"]),
                                Price = (double)((r["Price"] == System.DBNull.Value) ? Double.Parse("0") : r["Price"]),
                                PriceDisplay = (string)((r["PriceDisplay"] == System.DBNull.Value) ? "0" : r["PriceDisplay"]),
                                PriceDealDisplay = (string)((r["PriceDealDisplay"] == System.DBNull.Value) ? "0" : r["PriceDealDisplay"]),
                                PriceDeal = (double)((r["PriceDeal"] == System.DBNull.Value) ? Double.Parse("0") : r["PriceDeal"]),
                                Discount = (double)((r["Discount"] == System.DBNull.Value) ? Double.Parse("0") : r["Discount"]),
                                Quantity = (int)((r["Quantity"] == System.DBNull.Value) ? 0 : r["Quantity"]),
                                Sold = (int)((r["Sold"] == System.DBNull.Value) ? 0 : r["Sold"]),
                                Available = (int)((r["Available"] == System.DBNull.Value) ? 0 : r["Available"]),
                                Ids = MyModels.Encode((int)r["Id"], SecretId),
                                TotalRows = (int)r["TotalRows"],
                            }).ToList();
                }
            }
            else
            {
                var tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Products",
                new string[] { "@flag", "@CurrentPage", "@ItemsPerPage", "@Keyword", "@CatId", "@Status" },
                new object[] { str_sql, dto.CurrentPage, dto.ItemsPerPage, dto.Keyword, dto.CatId, Status });

                if (tabl == null)
                {
                    return new List<Products>();
                }
                else
                {
                    return (from r in tabl.AsEnumerable()
                            select new Products
                            {
                                Id = (int)r["Id"],
                                Title = (string)r["Title"],
                                Status = (bool)r["Status"],
                                Featured = (bool)r["Featured"],
                                Introtext = (string)((r["Introtext"] == System.DBNull.Value) ? "" : r["Introtext"]),
                                Alias = (string)((r["Alias"] == System.DBNull.Value) ? "" : r["Alias"]),
                                Image = (string)((r["Image"] == System.DBNull.Value) ? "" : r["Image"]),
                                Sku = (string)((r["Sku"] == System.DBNull.Value) ? "" : r["Sku"]),
                                Price = (double)((r["Price"] == System.DBNull.Value) ? Double.Parse("0") : r["Price"]),
                                PriceDisplay = (string)((r["PriceDisplay"] == System.DBNull.Value) ? "0" : r["PriceDisplay"]),
                                PriceDealDisplay = (string)((r["PriceDealDisplay"] == System.DBNull.Value) ? "0" : r["PriceDealDisplay"]),
                                PriceDeal = (double)((r["PriceDeal"] == System.DBNull.Value) ? Double.Parse("0") : r["PriceDeal"]),
                                Discount = (double)((r["Discount"] == System.DBNull.Value) ? Double.Parse("0") : r["Discount"]),
                                Quantity = (int)((r["Quantity"] == System.DBNull.Value) ? 0 : r["Quantity"]),
                                Sold = (int)((r["Sold"] == System.DBNull.Value) ? 0 : r["Sold"]),
                                Available = (int)((r["Available"] == System.DBNull.Value) ? 0 : r["Available"]),
                                Ids = MyModels.Encode((int)r["Id"], SecretId),
                                TotalRows = (int)r["TotalRows"],
                            }).ToList();
                }
            }           
        }

        public static List<Products> GetListAllProductByCat(string SecretId = "", Boolean Featured = false)
        {
            string sql = "GetListAllProductByCat";
            if (Featured)
            {
                sql = "GetListAllProductByCatFeature";
            }
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Products",
            new string[] { "@flag" }, new object[] { sql });
            if (tabl == null)
            {
                return new List<Products>();
            }
            else
            {
                return (from r in tabl.AsEnumerable()
                        select new Products
                        {
                            Id = (int)r["Id"],
                            Title = (string)r["Title"],
                            Status = (bool)r["Status"],
                            Introtext = (string)((r["Introtext"] == System.DBNull.Value) ? "" : r["Introtext"]),
                            Alias = (string)((r["Alias"] == System.DBNull.Value) ? "" : r["Alias"]),
                            Image = (string)((r["Image"] == System.DBNull.Value) ? "" : r["Image"]),
                            Sku = (string)((r["Sku"] == System.DBNull.Value) ? "" : r["Sku"]),
                            Price = (double)((r["Price"] == System.DBNull.Value) ? Double.Parse("0") : r["Price"]),
                            PriceDisplay = (string)((r["PriceDisplay"] == System.DBNull.Value) ? "0" : r["PriceDisplay"]),
                            PriceDealDisplay = (string)((r["PriceDealDisplay"] == System.DBNull.Value) ? "0" : r["PriceDealDisplay"]),
                            PriceDeal = (double)((r["PriceDeal"] == System.DBNull.Value) ? Double.Parse("0") : r["PriceDeal"]),
                            Discount = (double)((r["Discount"] == System.DBNull.Value) ? Double.Parse("0") : r["Discount"]),
                            Quantity = (int)((r["Quantity"] == System.DBNull.Value) ? 0 : r["Quantity"]),
                            CategoryId = (int)((r["CatId"] == System.DBNull.Value) ? 0 : r["CatId"]),
                            Sold = (int)((r["Sold"] == System.DBNull.Value) ? 0 : r["Sold"]),
                            Available = (int)((r["Available"] == System.DBNull.Value) ? 0 : r["Available"]),

                            Ids = MyModels.Encode((int)r["Id"], SecretId),
                        }).ToList();
            }

        }

        public static List<Products> GetListFeatured(int CatId = 0, int Limit = 5,string SecretId="")
        {
            
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Products",
                
            new string[] { "@flag", "@CatId" }, new object[] { "GetListFeatured", CatId });
            if (tabl == null)
            {
                return new List<Products>();
            }
            else
            {
                return (from r in tabl.AsEnumerable()
                        select new Products
                        {
                            Id = (int)r["Id"],
                            Title = (string)r["Title"],
                            Status = (bool)r["Status"],
                            Introtext = (string)((r["Introtext"] == System.DBNull.Value) ? "" : r["Introtext"]),
                            Alias = (string)((r["Alias"] == System.DBNull.Value) ? "" : r["Alias"]),
                            Image = (string)((r["Image"] == System.DBNull.Value) ? "" : r["Image"]),
                            Sku = (string)((r["Sku"] == System.DBNull.Value) ? "" : r["Sku"]),
                            Price = (double)((r["Price"] == System.DBNull.Value) ? Double.Parse("0") : r["Price"]),
                            PriceDisplay = (string)((r["PriceDisplay"] == System.DBNull.Value) ? "0" : r["PriceDisplay"]),
                            PriceDealDisplay = (string)((r["PriceDealDisplay"] == System.DBNull.Value) ? "0" : r["PriceDealDisplay"]),
                            PriceDeal = (double)((r["PriceDeal"] == System.DBNull.Value) ? Double.Parse("0") : r["PriceDeal"]),
                            Discount = (double)((r["Discount"] == System.DBNull.Value) ? Double.Parse("0") : r["Discount"]),
                            Quantity = (int)((r["Quantity"] == System.DBNull.Value) ? 0 : r["Quantity"]),
                            Sold = (int)((r["Sold"] == System.DBNull.Value) ? 0 : r["Sold"]),
                            Available = (int)((r["Available"] == System.DBNull.Value) ? 0 : r["Available"]),
                            Ids = MyModels.Encode((int)r["Id"], SecretId),
                        }).ToList();
            }

        }

        public static List<Products> GetListNew(int CatId = 0, int Limit = 5,string SecretId="")
        {
            
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Products",                
            new string[] { "@flag", "@CatId" }, new object[] { "GetListNew", CatId });
            if (tabl == null)
            {
                return new List<Products>();
            }
            else
            {
                return (from r in tabl.AsEnumerable()
                        select new Products
                        {
                            Id = (int)r["Id"],
                            Title = (string)r["Title"],
                            Status = (bool)r["Status"],
                            Introtext = (string)((r["Introtext"] == System.DBNull.Value) ? "" : r["Introtext"]),
                            Alias = (string)((r["Alias"] == System.DBNull.Value) ? "" : r["Alias"]),
                            Image = (string)((r["Image"] == System.DBNull.Value) ? "" : r["Image"]),
                            Sku = (string)((r["Sku"] == System.DBNull.Value) ? "" : r["Sku"]),
                            Price = (double)((r["Price"] == System.DBNull.Value) ? Double.Parse("0") : r["Price"]),
                            PriceDisplay = (string)((r["PriceDisplay"] == System.DBNull.Value) ? "0" : r["PriceDisplay"]),
                            PriceDealDisplay = (string)((r["PriceDealDisplay"] == System.DBNull.Value) ? "0" : r["PriceDealDisplay"]),
                            PriceDeal = (double)((r["PriceDeal"] == System.DBNull.Value) ? Double.Parse("0") : r["PriceDeal"]),
                            Discount = (double)((r["Discount"] == System.DBNull.Value) ? Double.Parse("0") : r["Discount"]),
                            Quantity = (int)((r["Quantity"] == System.DBNull.Value) ? 0 : r["Quantity"]),
                            Sold = (int)((r["Sold"] == System.DBNull.Value) ? 0 : r["Sold"]),
                            Available = (int)((r["Available"] == System.DBNull.Value) ? 0 : r["Available"]),
                            Ids = MyModels.Encode((int)r["Id"], SecretId),
                        }).ToList();
            }

        }

        public static DataTable GetList(Boolean Selected = true)
        {            
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Products",
                new string[] { "@flag", "@Selected" }, new object[] { "GetList", Selected });
            return tabl;
        }

        public static List<int> GetListCatIdByProductId(int ProductsId)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Products",
                new string[] { "@flag", "@Id" }, new object[] { "GetListCatIdByProductId", ProductsId });
            if (tabl == null)
            {
                return new List<int>();
            }
            else {
                return tabl.AsEnumerable().Select(r => (int)r["CatId"]).ToList();
            }
            
        }

        public static List<SelectListItem> GetListSelectItems(Boolean Selected = true)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Products",
                new string[] { "@flag", "@Selected" }, new object[] { "GetList", Convert.ToDecimal(Selected) });
            List<SelectListItem> ListItems = (from r in tabl.AsEnumerable()
                                              select new SelectListItem
                                              {
                                                  Value = (string)((r["Id"] == System.DBNull.Value) ? null : r["Id"].ToString()),
                                                  Text = (string)((r["Title"] == System.DBNull.Value) ? "" : r["Title"]),
                                              }).ToList();
            ListItems.Insert(0, (new SelectListItem { Text = "--- Chọn Chức Vụ ---", Value = "0" }));
            return ListItems;
        }

        public static Products GetItem(int Id, string SecretId = null)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Products",
            new string[] { "@flag", "@Id" }, new object[] { "GetItem", Id });
            Products Item = (from r in tabl.AsEnumerable()
                    select new Products
                    {
                        Id = (int)r["Id"],
                        Title = (string)r["Title"],
                        Alias = (string)((r["Alias"] == System.DBNull.Value) ? "" : r["Alias"]),
                        Status = (Boolean)((r["Status"] == System.DBNull.Value) ? false : r["Status"]),
                        Featured = (Boolean)((r["Featured"] == System.DBNull.Value) ? false : r["Featured"]),
                        Image = (string)((r["Image"] == System.DBNull.Value) ? "" : r["Image"]),
                        PublishUp = (DateTime)((r["PublishUp"] == System.DBNull.Value) ? DateTime.Now : r["PublishUp"]),
                        PublishUpShow = (string)((r["PublishUp"] == System.DBNull.Value) ? DateTime.Now.ToString("dd/MM/yyyy") : (string)((DateTime)r["PublishUp"]).ToString("dd/MM/yyyy")),                        
                        Sku = (string)((r["Sku"] == System.DBNull.Value) ? "" : r["Sku"]),
                        Price = (double)((r["Price"] == System.DBNull.Value) ? 0 : r["Price"]),
                        PriceDisplay = (string)((r["PriceDisplay"] == System.DBNull.Value) ? "0" : r["PriceDisplay"]),
                        PriceDealDisplay = (string)((r["PriceDealDisplay"] == System.DBNull.Value) ? "0" : r["PriceDealDisplay"]),
                        PriceDeal = (double)((r["PriceDeal"] == System.DBNull.Value) ? 0 : r["PriceDeal"]),
                        Discount = (double)((r["Discount"] == System.DBNull.Value) ? 0 : r["Discount"]),
                        Quantity = (int)((r["Quantity"] == System.DBNull.Value) ? 0 : r["Quantity"]),
                        Sold = (int)((r["Sold"] == System.DBNull.Value) ? 0 : r["Sold"]),
                        Available = (int)((r["Available"] == System.DBNull.Value) ? 0 : r["Available"]),
                        Introtext = (string)((r["Introtext"] == System.DBNull.Value) ? "" : r["Introtext"]),
                        Description = (string)((r["Description"] == System.DBNull.Value) ? "" : r["Description"]),
                        Metadesc = (string)((r["Metadesc"] == System.DBNull.Value) ? "" : r["Metadesc"]),
                        Metakey = (string)((r["Metakey"] == System.DBNull.Value) ? "" : r["Metakey"]),
                        Metadata = (string)((r["Metadata"] == System.DBNull.Value) ? "" : r["Metadata"]),                        
                        Ids = MyModels.Encode((int)r["Id"], SecretId),
                    }).FirstOrDefault();

            if (Item.Metadata != null)
            {
                try
                {
                    Item.MetadataCV = JsonConvert.DeserializeObject<API.Models.MetaData>(Item.Metadata);
                }
                catch
                {
                    Item.MetadataCV = new API.Models.MetaData() { MetaTitle = Item.Title };
                }
            }
            else
            {
                Item.MetadataCV = new API.Models.MetaData() { MetaTitle = Item.Title, MetaH1 = Item.Title, MetaH3 = Item.Title };
            }

            return Item;
        }

        public static dynamic SaveItem(Products dto, DataTable tbItem)
        {
            if (dto.MetadataCV.MetaTitle == null || dto.MetadataCV.MetaTitle.Trim() == "")
            {
                dto.MetadataCV.MetaTitle = dto.Title;
                dto.MetadataCV.MetaH1 = dto.Title;
                dto.MetadataCV.MetaH3 = dto.Title;
            }

            dto.Metadata = JsonConvert.SerializeObject(dto.MetadataCV);

            dto.PriceDisplay = API.Models.MyHelper.StringHelper.ConvertNumberToDisplay(dto.Price.ToString());
            dto.PriceDealDisplay = API.Models.MyHelper.StringHelper.ConvertNumberToDisplay(dto.PriceDeal.ToString());

            if (dto.PriceDeal > 0)
            {
                dto.Discount = Math.Round((dto.PriceDeal / dto.Price * 100) - 100);
            }
            else {
                dto.Discount = 0;
            }
            

            DateTime NgayDang = DateTime.ParseExact(dto.PublishUpShow, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Products",
            new string[] { "@flag","@Id", "@Title", "@Alias", "@Status", "@Featured", "@Metadesc", "@Metakey", "@Metadata", "@Image", "@Description",  "@CreatedBy", "@ModifiedBy", "@Sku" , "@Price", "@PriceDeal" , "@Discount", "@Quantity", "@PublishUp", "@TBL_ProductsCat", "@PriceDisplay","@PriceDealDisplay", "@Available", "@Sold" },
            new object[] { "SaveItem", dto.Id, dto.Title,dto.Alias, dto.Status, dto.Featured, dto.Metadesc, dto.Metakey, dto.Metadata, dto.Image, dto.Description, dto.CreatedBy,dto.ModifiedBy,dto.Sku,dto.Price,dto.PriceDeal,dto.Discount,dto.Quantity,NgayDang,tbItem,dto.PriceDisplay,dto.PriceDealDisplay,dto.Available,dto.Sold });
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();
        }

        public static dynamic UpdateStatus(Products dto)
        {
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Products",
            new string[] { "@flag", "@Id","@Status", "@ModifiedBy" },
            new object[] { "UpdateStatus", dto.Id,dto.Status, dto.ModifiedBy });
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }

        public static dynamic UpdateFeatured(Products dto)
        {
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Products",
            new string[] { "@flag", "@Id", "@Featured", "@ModifiedBy" },
            new object[] { "UpdateFeatured", dto.Id, dto.Featured, dto.ModifiedBy });
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }

        public static dynamic DeleteItem(Products dto)
        {
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Products",
            new string[] { "@flag", "@Id", "@ModifiedBy" },
            new object[] { "DeleteItem", dto.Id, dto.ModifiedBy});
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }


    }
}
