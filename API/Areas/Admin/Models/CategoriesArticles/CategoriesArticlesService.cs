﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using API.Areas.Admin.Models.CategoriesArticles;
using API.Areas.Admin.Models.CategoriesLanguage;
using API.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;

namespace API.Areas.Admin.Models.CategoriesArticles
{
    public class CategoriesArticlesService
    {
        public static List<CategoriesArticles> GetListPagination(SearchCategoriesArticles dto, string SecretId,int IdCoQuan=1)
        {
			if (dto.CurrentPage <= 0)
            {
                dto.CurrentPage = 1;
            }
            if (dto.ItemsPerPage <= 0)
            {
                dto.ItemsPerPage = 10;
            }
            if (dto.Keyword == null)
            {
                dto.Keyword = "";
            }
            var tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_CategoriesArticles",
                new string[] { "@flag", "@CurrentPage", "@ItemsPerPage", "@Keyword", "@IdCoQuan" },
                new object[] { "GetListPagination", dto.CurrentPage, dto.ItemsPerPage, dto.Keyword, IdCoQuan });
            if (tabl == null)
            {
                return new List<CategoriesArticles>();
            }
            else
            {
                return (from r in tabl.AsEnumerable()
					select new CategoriesArticles
					{
						Id = (int)r["Id"],
 						Title = (string)((r["Title"] == System.DBNull.Value) ? null : r["Title"]),
 						Alias = (string)((r["Alias"] == System.DBNull.Value) ? null : r["Alias"]), 						
 						ParentId = (int?)((r["ParentId"] == System.DBNull.Value) ? null : r["ParentId"]),
 						Status = (Boolean)r["Status"],
                        FeaturedHome = (Boolean)((r["FeaturedHome"] == System.DBNull.Value) ? false : r["FeaturedHome"]),
                        Hits = (int?)((r["Hits"] == System.DBNull.Value) ? null : r["Hits"]),
                        Images = (string)((r["Images"] == System.DBNull.Value) ? null : r["Images"]),
                        Params = (string)((r["Params"] == System.DBNull.Value) ? null : r["Params"]),
                        Ordering = (int?)((r["Ordering"] == System.DBNull.Value) ? null : r["Ordering"]),
                        Ids = MyModels.Encode((int)r["Id"], SecretId),						
					}).ToList();
            }


        }

        public static List<CategoriesArticles> GetList(Boolean Selected = true,int IdCoQuan=1)
        {
            
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_CategoriesArticles",
                new string[] { "@flag", "@Selected" , "@IdCoQuan" }, new object[] { "GetList", Convert.ToDecimal(Selected), IdCoQuan });
            if (tabl == null)
            {
                return new List<CategoriesArticles>();
            }
            else
            {
                return (from r in tabl.AsEnumerable()
					select new CategoriesArticles
					{
						Id = (int)r["Id"],
 						Title = (string)((r["Title"] == System.DBNull.Value) ? null : r["Title"]),
 						Alias = (string)((r["Alias"] == System.DBNull.Value) ? null : r["Alias"]),
 						//Description = (string)((r["Description"] == System.DBNull.Value) ? null : r["Description"]),
 						ParentId = (int?)((r["ParentId"] == System.DBNull.Value) ? null : r["ParentId"]),
                        Images = (string)((r["Images"] == System.DBNull.Value) ? null : r["Images"]),
                        //Status = (Boolean)r["Status"],
                        FeaturedHome = (Boolean)((r["FeaturedHome"] == System.DBNull.Value) ? false : r["FeaturedHome"]),
                        /*
                        Metadesc = (string)((r["Metadesc"] == System.DBNull.Value) ? null : r["Metadesc"]),
 						Metakey = (string)((r["Metakey"] == System.DBNull.Value) ? null : r["Metakey"]),
 						Metadata = (string)((r["Metadata"] == System.DBNull.Value) ? null : r["Metadata"]),
                        
                        Params = (string)((r["Params"] == System.DBNull.Value) ? null : r["Params"]),
                        Ordering = (int?)((r["Ordering"] == System.DBNull.Value) ? null : r["Ordering"]),
                        Hits = (int?)((r["Hits"] == System.DBNull.Value) ? null : r["Hits"]),						
						TotalRows = (int)r["TotalRows"],*/
                    }).ToList();
            }

        }

        public static List<CategoriesArticles> GetListSiteMap(Boolean Selected = true, int IdCoQuan = 1)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_CategoriesArticles",
                new string[] { "@flag", "@Selected", "@IdCoQuan" }, new object[] { "GetListSiteMap", Convert.ToDecimal(Selected), IdCoQuan });
            if (tabl == null)
            {
                return new List<CategoriesArticles>();
            }
            else
            {
                return (from r in tabl.AsEnumerable()
                        select new CategoriesArticles
                        {
                            Id = (int)r["Id"],
                            Title = (string)((r["Title"] == System.DBNull.Value) ? null : r["Title"]),
                            Alias = (string)((r["Alias"] == System.DBNull.Value) ? null : r["Alias"]),                            
                            ParentId = (int?)((r["ParentId"] == System.DBNull.Value) ? 0 : r["ParentId"]),
                            ModifiedDate = (DateTime)((r["ModifiedDate"] == System.DBNull.Value) ? DateTime.Now : r["ModifiedDate"]),
                        }).ToList();
            }

        }

        public static List<CategoriesArticles> GetListFeaturedHome(int IdCoQuan=1)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_CategoriesArticles",
                new string[] { "@flag", "@IdCoQuan" }, new object[] { "GetListFeaturedHome", IdCoQuan });
            if (tabl == null)
            {
                return new List<CategoriesArticles>();
            }
            else
            {
                return (from r in tabl.AsEnumerable()
                        select new CategoriesArticles
                        {
                            Id = (int)r["Id"],
                            Title = (string)((r["Title"] == System.DBNull.Value) ? null : r["Title"]),
                            Images = (string)((r["Images"] == System.DBNull.Value) ? null : r["Images"]),
                            Alias = (string)((r["Alias"] == System.DBNull.Value) ? null : r["Alias"]),                            
                            Description = (string)((r["Description"] == System.DBNull.Value) ? null : r["Description"]),                            
                            ParentId = (int?)((r["ParentId"] == System.DBNull.Value) ? null : r["ParentId"]),                            
                            Hits = (int)((r["Hits"] == System.DBNull.Value) ? 0 : r["Hits"]),
                            Member = (int)((r["Member"] == System.DBNull.Value) ? 0 : r["Member"]),
                            CreatedByName = (string)((r["CreatedByName"] == System.DBNull.Value) ? "" : r["CreatedByName"]),                            
                            CreatedDate = (DateTime)((r["CreatedDate"] == System.DBNull.Value) ? DateTime.Now : r["CreatedDate"]),                            
                        }).ToList();
            }

        }
        public static List<SelectListItem> GetListItems(Boolean Selected = true)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_CategoriesArticles",
                new string[] { "@flag", "@Selected" }, new object[] { "GetList", Convert.ToDecimal(Selected) });
            List<SelectListItem> ListItems = (from r in tabl.AsEnumerable()
                                              select new SelectListItem
                                              {
                                                  Value = (string)((r["Id"] == System.DBNull.Value) ? null : r["Id"].ToString()),
                                                  Text = (string)((r["Title"] == System.DBNull.Value) ? null : r["Title"]),
                                              }).ToList();

            ListItems.Insert(0, (new SelectListItem { Text = "Chọn Danh mục", Value = "0" }));
            return ListItems;

        }

        public static CategoriesArticles GetItem(decimal Id, string SecretId = null,int IdCoQuan=1,string Culture="vi")
        {
            string sql = "GetItem";

           
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_CategoriesArticles",
            new string[] { "@flag", "@Id", "@IdCoQuan" }, new object[] { sql, Id, IdCoQuan });
            CategoriesArticles Item = (from r in tabl.AsEnumerable()
                    select new CategoriesArticles
                    {
                        Id = (int)r["Id"],
 						Title = (string)((r["Title"] == System.DBNull.Value) ? null : r["Title"]),
 						Alias = (string)((r["Alias"] == System.DBNull.Value) ? null : r["Alias"]),
 						Description = (string)((r["Description"] == System.DBNull.Value) ? null : r["Description"]),
 						ParentId = (int?)((r["ParentId"] == System.DBNull.Value) ? null : r["ParentId"]),
 						Status = (Boolean)r["Status"], 						
                        FeaturedHome = (Boolean)((r["FeaturedHome"] == System.DBNull.Value) ? false : r["FeaturedHome"]),
                        Metadesc = (string)((r["Metadesc"] == System.DBNull.Value) ? null : r["Metadesc"]),
 						Metakey = (string)((r["Metakey"] == System.DBNull.Value) ? null : r["Metakey"]),
 						Metadata = (string)((r["Metadata"] == System.DBNull.Value) ? null : r["Metadata"]),                        
                        Images = (string)((r["Images"] == System.DBNull.Value) ? null : r["Images"]),
                        Params = (string)((r["Params"] == System.DBNull.Value) ? null : r["Params"]),
                        Ordering = (int?)((r["Ordering"] == System.DBNull.Value) ? null : r["Ordering"]),
                        Hits = (int)((r["Hits"] == System.DBNull.Value) ? 0 : r["Hits"]),
                        Member = (int)((r["Member"] == System.DBNull.Value) ? 0 : r["Member"]),
                        Ids = MyModels.Encode((int)r["Id"], SecretId),
                    }).FirstOrDefault();

            if (Item != null && Item.Id > 0)
            {
                if (Item.Title == null)
                {
                    Item.Title = "";
                }
                if (Item.Metadata != null)
                {
                    Item.MetadataCV = JsonConvert.DeserializeObject<API.Models.MetaData>(Item.Metadata);
                }

            }
            return Item;
        }
        public static CategoriesArticles GetItemByAlias(string Alias, string SecretId = null)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_CategoriesArticles",
            new string[] { "@flag", "@Alias" }, new object[] { "GetItemByAlias", Alias });
            CategoriesArticles Item = (from r in tabl.AsEnumerable()
                    select new CategoriesArticles
                    {
                        Id = (int)r["Id"],
                        Title = (string)((r["Title"] == System.DBNull.Value) ? null : r["Title"]),
                        Alias = (string)((r["Alias"] == System.DBNull.Value) ? null : r["Alias"]),
                        Description = (string)((r["Description"] == System.DBNull.Value) ? null : r["Description"]),
                        ParentId = (int?)((r["ParentId"] == System.DBNull.Value) ? null : r["ParentId"]),
                        Status = (Boolean)r["Status"],                        
                        Metadesc = (string)((r["Metadesc"] == System.DBNull.Value) ? null : r["Metadesc"]),
                        Metakey = (string)((r["Metakey"] == System.DBNull.Value) ? null : r["Metakey"]),
                        Metadata = (string)((r["Metadata"] == System.DBNull.Value) ? null : r["Metadata"]),                        
                        Images = (string)((r["Images"] == System.DBNull.Value) ? null : r["Images"]),
                        Params = (string)((r["Params"] == System.DBNull.Value) ? null : r["Params"]),
                        Ordering = (int?)((r["Ordering"] == System.DBNull.Value) ? null : r["Ordering"]),
                        Hits = (int)((r["Hits"] == System.DBNull.Value) ? 0 : r["Hits"]),
                        Member = (int)((r["Member"] == System.DBNull.Value) ? 0 : r["Member"]),
                        Ids = MyModels.Encode((int)r["Id"], SecretId),
                    }).FirstOrDefault();
            if (Item.Metadata != null)
            {
                Item.MetadataCV = JsonConvert.DeserializeObject<API.Models.MetaData>(Item.Metadata);
            }
            return Item;
        }

        public static List<CategoriesArticles> GetListChild(int Id)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_CategoriesArticles",
            new string[] { "@flag", "@Id" }, new object[] { "GetListChild", Id });
            return (from r in tabl.AsEnumerable()
                    select new CategoriesArticles
                    {
                        Id = (int)r["Id"],
                        Title = (string)((r["Title"] == System.DBNull.Value) ? null : r["Title"]),
                        Alias = (string)((r["Alias"] == System.DBNull.Value) ? null : r["Alias"]),
                        Description = (string)((r["Description"] == System.DBNull.Value) ? null : r["Description"]),
                        ParentId = (int?)((r["ParentId"] == System.DBNull.Value) ? null : r["ParentId"]),
                        Status = (Boolean)r["Status"],                       
                        Metadesc = (string)((r["Metadesc"] == System.DBNull.Value) ? null : r["Metadesc"]),
                        Metakey = (string)((r["Metakey"] == System.DBNull.Value) ? null : r["Metakey"]),
                        Metadata = (string)((r["Metadata"] == System.DBNull.Value) ? null : r["Metadata"]),                        
                        Images = (string)((r["Images"] == System.DBNull.Value) ? null : r["Images"]),
                        Params = (string)((r["Params"] == System.DBNull.Value) ? null : r["Params"]),
                        Ordering = (int?)((r["Ordering"] == System.DBNull.Value) ? null : r["Ordering"]),
                        Hits = (int)((r["Hits"] == System.DBNull.Value) ? 0 : r["Hits"]),
                        Member = (int)((r["Hits"] == System.DBNull.Value) ? 0 : r["Member"]),                        
                    }).ToList();
        }

        public static dynamic SaveItem(CategoriesArticles dto)
        {
            if (dto.Alias == null || dto.Alias == "")
            {
                dto.Alias = API.Models.MyHelper.StringHelper.UrlFriendly(dto.Title);
            }
            if (dto.MetadataCV.MetaTitle == null || dto.MetadataCV.MetaTitle == "")
            {
                dto.MetadataCV.MetaTitle = dto.Title;
            }
            dto.Metadata = JsonConvert.SerializeObject(dto.MetadataCV);

            if (dto.Culture.ToLower() == "vi")
            {
                DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_CategoriesArticles",
                new string[] { "@flag", "@Id", "@Title", "@Alias", "@Description", "@ParentId", "@Status", "@Deleted", "@CreatedBy", "@ModifiedBy", "@Metadesc", "@Metakey", "@Metadata", "@Images", "@Params", "@Ordering", "@Hits", "@FeaturedHome", "@IdCoQuan", "@Member" },
                new object[] { "SaveItem", dto.Id, dto.Title, dto.Alias, dto.Description, dto.ParentId, dto.Status, dto.Deleted, dto.CreatedBy, dto.ModifiedBy, dto.Metadesc, dto.Metakey, dto.Metadata, dto.Images, dto.Params, dto.Ordering, dto.Hits, dto.FeaturedHome, dto.IdCoQuan, dto.Member });
                return (from r in tabl.AsEnumerable()
                        select new
                        {
                            N = (int)(r["N"]),
                        }).FirstOrDefault();
            }
            else {
                var a = CategoriesLanguageService.SaveItem(new CategoriesLanguage.CategoriesLanguage() { 
                    TypeId = 1,
                    IdRoot = dto.Id,
                    LanguageCode = dto.Culture,
                    Title = dto.Title,
                    Alias = dto.Alias,
                    Description = dto.Description,
                    Metakey = dto.Metakey,
                    Metadesc = dto.Metadesc,
                    Metadata = dto.Metadata  ,                  
                    MetadataCV = dto.MetadataCV,                  
                });

                return new{ N=dto.Id };
            }
           

        }
        public static dynamic DeleteItem(CategoriesArticles dto)
        {
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_CategoriesArticles",
            new string[] { "@flag", "@Id", "@ModifiedBy" },
            new object[] { "DeleteItem", dto.Id, dto.ModifiedBy});
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }
		
		public static dynamic UpdateStatus(CategoriesArticles dto)
        {
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_CategoriesArticles",
            new string[] { "@flag", "@Id","@Status", "@ModifiedBy" },
            new object[] { "UpdateStatus", dto.Id,dto.Status, dto.ModifiedBy });
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }

        public static dynamic UpdateAlias(int Id, string Alias)
        {
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_CategoriesArticles",
            new string[] { "@flag", "@Id", "@Alias" },
            new object[] { "UpdateAlias", Id, Alias });
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }



    }
}
