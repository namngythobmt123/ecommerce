﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using API.Areas.Admin.Models.Partial;
/*https://docs.microsoft.com/en-us/aspnet/core/tutorials/razor-pages/validation?view=aspnetcore-2.2*/
namespace API.Areas.Admin.Models.OrdersProducts
{
    public class OrdersProducts
    {
        public int Id { get; set; }        
        public string Title { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
        public string ProductsTitle { get; set; }
        public string TotalDisplay { get; set; }
        public double Total { get; set; }        
        public int ProductId { get; set; }        
        public string Ids { get; set; }
        public int TotalRows { get; set; } = 0;
        public int CreatedBy { get; set; } = 0;
        public int ModifiedBy { get; set; } = 0;
    }

    public class OrdersProductsModel
    {
        public List<OrdersProducts> ListItems { get; set; }
        public SearchOrdersProducts SearchData { get; set; }
        public OrdersProducts Item { get; set; }
        public PartialPagination Pagination { get; set; }
    }
    public class SearchOrdersProducts
    {
        public int CurrentPage { get; set; }
        public int ItemsPerPage { get; set; }
        public int OrderId { get; set; }
        public string Ids { get; set; }
        public string Keyword { get; set; }
        public int UserIdCheckIn { get; set; }
    }
}
