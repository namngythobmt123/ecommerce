﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Areas.Admin.Models.OrdersProducts;
using API.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace API.Areas.Admin.Models.OrdersProducts
{
    public class OrdersProductsService
    {

        public static List<OrdersProducts> GetListPagination(SearchOrdersProducts dto, string SecretId)
        {
            if (dto.CurrentPage <= 0)
            {
                dto.CurrentPage = 1;
            }
            if (dto.ItemsPerPage <= 0)
            {
                dto.ItemsPerPage = 10;
            }
            if (dto.Keyword == null)
            {
                dto.Keyword = "";
            }
            var tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_OrdersProducts",
                new string[] { "@flag", "@CurrentPage", "@ItemsPerPage", "@Keyword", "@OrderId", "@UserIdCheckIn" },
                new object[] { "GetListPagination", dto.CurrentPage, dto.ItemsPerPage, dto.Keyword, dto.OrderId, dto.UserIdCheckIn });
            if (tabl == null)
            {
                return new List<OrdersProducts>();
            }
            else
            {
                return (from r in tabl.AsEnumerable()
                        select new OrdersProducts
                        {
                            Id = (int)r["Id"],
                            ProductId = (int)((r["ProductId"] == System.DBNull.Value) ? 0 : r["ProductId"]),
                            Total = (double)((r["Total"] == System.DBNull.Value) ? Double.Parse("0") : r["Total"]),
                            Price = (double)((r["Price"] == System.DBNull.Value) ? Double.Parse("0") : r["Price"]),
                            Quantity = (int)((r["Quantity"] == System.DBNull.Value) ? 0 : r["Quantity"]),
                            ProductsTitle = (string)((r["ProductsTitle"] == System.DBNull.Value) ? "" : r["ProductsTitle"]),
                            TotalDisplay = (string)((r["TotalDisplay"] == System.DBNull.Value) ? "" : r["TotalDisplay"]),
                            Ids = MyModels.Encode((int)r["Id"], SecretId),
                            TotalRows = (int)r["TotalRows"]
                        }).ToList();
            }
        }
        public static List<OrdersProducts> GetList(Boolean Selected = true)
        {
            
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_OrdersProducts",
                new string[] { "@flag", "@Selected" }, new object[] { "GetList", Selected });
            return (from r in tabl.AsEnumerable()
                    select new OrdersProducts
                    {
                        Id = (int)r["Id"],
                        Title = (string)r["Title"],
                        ProductId = (int)((r["ProductId"] == System.DBNull.Value) ? 0 : r["ProductId"]),
                        Total = (double)((r["Total"] == System.DBNull.Value) ? 0 : r["Total"]),
                        Price = (double)((r["Price"] == System.DBNull.Value) ? Double.Parse("0") : r["Price"]),
                        Quantity = (int)((r["Quantity"] == System.DBNull.Value) ? 0 : r["Quantity"]),
                        ProductsTitle = (string)((r["ProductsTitle"] == System.DBNull.Value) ? "" : r["ProductsTitle"]),
                    }).ToList();

        }

      
        public static OrdersProducts GetItem(int Id, string SecretId = null)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_OrdersProducts",
            new string[] { "@flag", "@Id" }, new object[] { "GetItem", Id });
            return (from r in tabl.AsEnumerable()
                    select new OrdersProducts
                    {
                        Id = (int)r["Id"],
                        Title = (string)r["Title"],
                        ProductId = (int)((r["ProductId"] == System.DBNull.Value) ? 0 : r["ProductId"]),
                        Total = (double)((r["Total"] == System.DBNull.Value) ? 0 : r["Total"]),
                        Ids = MyModels.Encode((int)r["Id"], SecretId),
                    }).FirstOrDefault();
        }

        public static dynamic SaveItem(OrdersProducts dto)
        {
            
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_OrdersProducts",
            new string[] { "@flag","@Id", "@Title", "@ProductId", "@Total",  "@CreatedBy", "@ModifiedBy" },
            new object[] { "SaveItem", dto.Id, dto.Title, dto.ProductId, dto.Total, dto.CreatedBy,dto.ModifiedBy });
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }

      
        public static dynamic DeleteItem(OrdersProducts dto)
        {
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_OrdersProducts",
            new string[] { "@flag", "@Id", "@ModifiedBy" },
            new object[] { "DeleteItem", dto.Id, dto.ModifiedBy});
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }


    }
}
