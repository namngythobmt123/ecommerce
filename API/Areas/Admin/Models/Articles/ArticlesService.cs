﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using API.Areas.Admin.Models.Articles;
using API.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;

namespace API.Areas.Admin.Models.Articles
{
    public class ArticlesService
    {
        public static List<Articles> GetListPagination(SearchArticles dto, string SecretId, string Culture = "all")
        {
            if (dto.CurrentPage <= 0)
            {
                dto.CurrentPage = 1;
            }
            if (dto.ItemsPerPage <= 0)
            {
                dto.ItemsPerPage = 10;
            }
            if (dto.Keyword == null)
            {
                dto.Keyword = "";
            }

            string StartDate = DateTime.ParseExact(dto.ShowStartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyyMMdd");
            string EndDate = DateTime.ParseExact(dto.ShowEndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyyMMdd");

            string str_sql = "GetListPagination_Status";
            Boolean Status = true;
            if (dto.Status == -1)
            {
                str_sql = "GetListPagination";
            }
            else if (dto.Status == 0)
            {
                Status = false;
            }
            var tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Articles",
                new string[] { "@flag", "@CurrentPage", "@ItemsPerPage", "@Keyword", "@CatId", "@IdCoQuan", "@AuthorId", "@StartDate", "@EndDate", "@CreatedBy", "@Status" },
                new object[] { str_sql, dto.CurrentPage, dto.ItemsPerPage, dto.Keyword, dto.CatId, dto.IdCoQuan, dto.AuthorId, StartDate, EndDate, dto.CreatedBy, Status });
            if (tabl == null)
            {
                return new List<Articles>();
            }
            else
            {
                if (Culture == "en")
                {
                    return (from r in tabl.AsEnumerable()
                            select new Articles
                            {
                                Id = (int)r["Id"],
                                Title = (string)((r["Title_EN"] == System.DBNull.Value) ? r["Title"] : r["Title_EN"]),
                                Alias = (string)((r["Alias_EN"] == System.DBNull.Value) ? r["Alias"] : r["Alias_EN"]),
                                CatId = (int)((r["CatId"] == System.DBNull.Value) ? 0 : r["CatId"]),
                                Category = (string)((r["Category"] == System.DBNull.Value) ? "" : r["Category"]),
                                IdCoQuan = (int)((r["IdCoQuan"] == System.DBNull.Value) ? 0 : r["IdCoQuan"]),
                                AuthorId = (int)((r["AuthorId"] == System.DBNull.Value) ? 0 : r["AuthorId"]),
                                AuthorName = (string)((r["AuthorName"] == System.DBNull.Value) ? null : r["AuthorName"]),
                                TenCoQuan = (string)((r["TenCoQuan"] == System.DBNull.Value) ? null : r["TenCoQuan"]),
                                Featured = (Boolean)((r["Featured"] == System.DBNull.Value) ? false : r["Featured"]),
                                FeaturedHome = (Boolean)((r["FeaturedHome"] == System.DBNull.Value) ? false : r["FeaturedHome"]),
                                StaticPage = (Boolean)((r["StaticPage"] == System.DBNull.Value) ? false : r["StaticPage"]),
                                Images = (string)((r["Images"] == System.DBNull.Value) ? null : r["Images"]),
                                ImagesFull = (string)((r["ImagesFull"] == System.DBNull.Value) ? null : r["ImagesFull"]),
                                IntroText = (string)((r["IntroText_EN"] == System.DBNull.Value) ? r["IntroText"] : r["IntroText_EN"]),
                                Status = (Boolean)((r["Status"] == System.DBNull.Value) ? false : r["Status"]),
                                RootNewsFlag = (Boolean)((r["RootNewsFlag"] == System.DBNull.Value) ? false : r["RootNewsFlag"]),
                                RootNewsId = (int)((r["RootNewsId"] == System.DBNull.Value) ? 0 : r["RootNewsId"]),
                                PublishUp = (DateTime)((r["PublishUp"] == System.DBNull.Value) ? null : r["PublishUp"]),
                                PublishUpShow = (string)((r["PublishUp"] == System.DBNull.Value) ? DateTime.Now.ToString("dd/MM/yyyy") : (string)((DateTime)r["PublishUp"]).ToString("dd/MM/yyyy")),
                                Ids = MyModels.Encode((int)r["Id"], SecretId),
                                TotalRows = (int)r["TotalRows"],
                                Title_EN = (string)((r["Title_EN"] == System.DBNull.Value) ? null : r["Title_EN"]),
                                Alias_EN = (string)((r["Alias_EN"] == System.DBNull.Value) ? null : r["Alias_EN"]),
                                IntroText_EN = (string)((r["IntroText_EN"] == System.DBNull.Value) ? null : r["IntroText_EN"]),
                                FullText_EN = (string)((r["FullText_EN"] == System.DBNull.Value) ? null : r["FullText_EN"]),
                                Metadesc_EN = (string)((r["Metadesc_EN"] == System.DBNull.Value) ? null : r["Metadesc_EN"]),
                                Metakey_EN = (string)((r["Metakey_EN"] == System.DBNull.Value) ? null : r["Metakey_EN"]),
                                Metadata_EN = (string)((r["Metadata_EN"] == System.DBNull.Value) ? null : r["Metadata_EN"]),
                            }).ToList();
                }
                else {
                    return (from r in tabl.AsEnumerable()
                            select new Articles
                            {
                                Id = (int)r["Id"],
                                Title = (string)((r["Title"] == System.DBNull.Value) ? null : r["Title"]),
                                Alias = (string)((r["Alias"] == System.DBNull.Value) ? null : r["Alias"]),
                                CatId = (int)((r["CatId"] == System.DBNull.Value) ? 0 : r["CatId"]),
                                Category = (string)((r["Category"] == System.DBNull.Value) ? "" : r["Category"]),
                                IdCoQuan = (int)((r["IdCoQuan"] == System.DBNull.Value) ? 0 : r["IdCoQuan"]),
                                AuthorId = (int)((r["AuthorId"] == System.DBNull.Value) ? 0 : r["AuthorId"]),
                                AuthorName = (string)((r["AuthorName"] == System.DBNull.Value) ? null : r["AuthorName"]),
                                TenCoQuan = (string)((r["TenCoQuan"] == System.DBNull.Value) ? null : r["TenCoQuan"]),
                                Featured = (Boolean)((r["Featured"] == System.DBNull.Value) ? false : r["Featured"]),
                                FeaturedHome = (Boolean)((r["FeaturedHome"] == System.DBNull.Value) ? false : r["FeaturedHome"]),
                                StaticPage = (Boolean)((r["StaticPage"] == System.DBNull.Value) ? false : r["StaticPage"]),
                                Images = (string)((r["Images"] == System.DBNull.Value) ? null : r["Images"]),
                                ImagesFull = (string)((r["ImagesFull"] == System.DBNull.Value) ? null : r["ImagesFull"]),
                                IntroText = (string)((r["IntroText"] == System.DBNull.Value) ? null : r["IntroText"]),
                                Status = (Boolean)((r["Status"] == System.DBNull.Value) ? false : r["Status"]),
                                RootNewsFlag = (Boolean)((r["RootNewsFlag"] == System.DBNull.Value) ? false : r["RootNewsFlag"]),
                                RootNewsId = (int)((r["RootNewsId"] == System.DBNull.Value) ? 0 : r["RootNewsId"]),
                                PublishUp = (DateTime)((r["PublishUp"] == System.DBNull.Value) ? null : r["PublishUp"]),
                                PublishUpShow = (string)((r["PublishUp"] == System.DBNull.Value) ? DateTime.Now.ToString("dd/MM/yyyy") : (string)((DateTime)r["PublishUp"]).ToString("dd/MM/yyyy")),
                                Ids = MyModels.Encode((int)r["Id"], SecretId),
                                TotalRows = (int)r["TotalRows"],
                                Title_EN = (string)((r["Title_EN"] == System.DBNull.Value) ? null : r["Title_EN"]),
                                Alias_EN = (string)((r["Alias_EN"] == System.DBNull.Value) ? null : r["Alias_EN"]),
                                IntroText_EN = (string)((r["IntroText_EN"] == System.DBNull.Value) ? null : r["IntroText_EN"]),
                                FullText_EN = (string)((r["FullText_EN"] == System.DBNull.Value) ? null : r["FullText_EN"]),
                                Metadesc_EN = (string)((r["Metadesc_EN"] == System.DBNull.Value) ? null : r["Metadesc_EN"]),
                                Metakey_EN = (string)((r["Metakey_EN"] == System.DBNull.Value) ? null : r["Metakey_EN"]),
                                Metadata_EN = (string)((r["Metadata_EN"] == System.DBNull.Value) ? null : r["Metadata_EN"]),
                            }).ToList();
                }

                
            }


        }


        public static List<Articles> GetListTransferPagination(SearchArticles dto, string SecretId)
        {
            if (dto.CurrentPage <= 0)
            {
                dto.CurrentPage = 1;
            }
            if (dto.ItemsPerPage <= 0)
            {
                dto.ItemsPerPage = 10;
            }
            if (dto.Keyword == null)
            {
                dto.Keyword = "";
            }
            string StartDate = DateTime.ParseExact(dto.ShowStartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyyMMdd");
            string EndDate = DateTime.ParseExact(dto.ShowEndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyyMMdd");

            string str_sql = "GetListTransferPagination";
            
            var tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Articles",
                new string[] { "@flag", "@CurrentPage", "@ItemsPerPage", "@Keyword", "@CatId", "@IdCoQuan", "@AuthorId", "@StartDate", "@EndDate", "@CreatedBy" },
                new object[] { str_sql, dto.CurrentPage, dto.ItemsPerPage, dto.Keyword, dto.CatId, dto.IdCoQuan, dto.AuthorId, StartDate, EndDate, dto.CreatedBy });
            if (tabl == null)
            {
                return new List<Articles>();
            }
            else
            {
                return (from r in tabl.AsEnumerable()
                        select new Articles
                        {
                            Id = (int)r["Id"],
                            Title = (string)((r["Title"] == System.DBNull.Value) ? null : r["Title"]),
                            Alias = (string)((r["Alias"] == System.DBNull.Value) ? null : r["Alias"]),
                            CatId = (int)((r["CatId"] == System.DBNull.Value) ? 0 : r["CatId"]),
                            Category = (string)((r["Category"] == System.DBNull.Value) ? "" : r["Category"]),
                            IdCoQuan = (int)((r["IdCoQuan"] == System.DBNull.Value) ? 0 : r["IdCoQuan"]),
                            AuthorId = (int)((r["AuthorId"] == System.DBNull.Value) ? 0 : r["AuthorId"]),
                            AuthorName = (string)((r["AuthorName"] == System.DBNull.Value) ? null : r["AuthorName"]),
                            TenCoQuan = (string)((r["TenCoQuan"] == System.DBNull.Value) ? null : r["TenCoQuan"]),
                            Featured = (Boolean)((r["Featured"] == System.DBNull.Value) ? null : r["Featured"]),
                            FeaturedHome = (Boolean)((r["FeaturedHome"] == System.DBNull.Value) ? null : r["FeaturedHome"]),
                            StaticPage = (Boolean)((r["StaticPage"] == System.DBNull.Value) ? 0 : r["StaticPage"]),
                            Images = (string)((r["Images"] == System.DBNull.Value) ? null : r["Images"]),
                            ImagesFull = (string)((r["ImagesFull"] == System.DBNull.Value) ? null : r["ImagesFull"]),
                            IntroText = (string)((r["IntroText"] == System.DBNull.Value) ? null : r["IntroText"]),
                            Status = (Boolean)((r["Status"] == System.DBNull.Value) ? 0 : r["Status"]),
                            RootNewsFlag = (Boolean)((r["RootNewsFlag"] == System.DBNull.Value) ? false : r["RootNewsFlag"]),
                            RootNewsId = (int)((r["RootNewsId"] == System.DBNull.Value) ? 0 : r["RootNewsId"]),
                            PublishUp = (DateTime)((r["PublishUp"] == System.DBNull.Value) ? null : r["PublishUp"]),
                            PublishUpShow = (string)((r["PublishUp"] == System.DBNull.Value) ? DateTime.Now.ToString("dd/MM/yyyy") : (string)((DateTime)r["PublishUp"]).ToString("dd/MM/yyyy")),
                            Ids = MyModels.Encode((int)r["Id"], SecretId),
                            TotalRows = (int)r["TotalRows"],
                        }).ToList();
            }


        }

        public static List<SelectListItem> GetListItemsStatus()
        {
            List<SelectListItem> ListItems = new List<SelectListItem>();
            ListItems.Insert(0, (new SelectListItem { Text = "--- Trạng Thái ---", Value = "-1" }));
            ListItems.Insert(1, (new SelectListItem { Text = "Chưa Duyệt", Value = "0" }));
            ListItems.Insert(2, (new SelectListItem { Text = "Đã Duyệt", Value = "1" }));
            return ListItems;
        }

        public static List<Articles> GetListPaginationByCat(string alias, SearchArticles dto, string SecretId)
        {
            if (dto.CurrentPage <= 0)
            {
                dto.CurrentPage = 1;
            }
            if (dto.ItemsPerPage <= 0)
            {
                dto.ItemsPerPage = 10;
            }
            if (dto.Keyword == null)
            {
                dto.Keyword = "";
            }
            var tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Articles",
                new string[] { "@flag", "@CurrentPage", "@ItemsPerPage", "@Keyword", "@CatAlias" },
                new object[] { "GetListPaginationByCat", dto.CurrentPage, dto.ItemsPerPage, dto.Keyword, alias });
            if (tabl == null)
            {
                return new List<Articles>();
            }
            else
            {
                return (from r in tabl.AsEnumerable()
                        select new Articles
                        {
                            Id = (int)r["Id"],
                            Title = (string)r["Title"],
                            Alias = (string)((r["Alias"] == System.DBNull.Value) ? null : r["Alias"]),
                            CatId = (int)r["CatId"],
                            AuthorId = (int)((r["AuthorId"] == System.DBNull.Value) ? 0 : r["AuthorId"]),
                            AuthorName = (string)((r["AuthorName"] == System.DBNull.Value) ? null : r["AuthorName"]),
                            Category = (string)r["Category"],
                            IntroText = (string)((r["IntroText"] == System.DBNull.Value) ? null : r["IntroText"]),
                            FullText = (string)((r["FullText"] == System.DBNull.Value) ? null : r["FullText"]),
                            Status = (Boolean)r["Status"],
                            CreatedBy = (int?)((r["CreatedBy"] == System.DBNull.Value) ? null : r["CreatedBy"]),
                            ModifiedBy = (int?)((r["ModifiedBy"] == System.DBNull.Value) ? null : r["ModifiedBy"]),
                            CreatedDate = (DateTime)((r["CreatedDate"] == System.DBNull.Value) ? null : r["CreatedDate"]),
                            PublishUp = (DateTime)((r["PublishUp"] == System.DBNull.Value) ? null : r["PublishUp"]),
                            PublishUpShow = (string)((r["PublishUp"] == System.DBNull.Value) ? DateTime.Now.ToString("dd/MM/yyyy") : (string)((DateTime)r["PublishUp"]).ToString("dd/MM/yyyy")),
                            ModifiedDate = (DateTime?)((r["ModifiedDate"] == System.DBNull.Value) ? null : r["ModifiedDate"]),
                            Metadesc = (string)((r["Metadesc"] == System.DBNull.Value) ? null : r["Metadesc"]),
                            Metakey = (string)((r["Metakey"] == System.DBNull.Value) ? null : r["Metakey"]),
                            Metadata = (string)((r["Metadata"] == System.DBNull.Value) ? null : r["Metadata"]),
                            Language = (string)((r["Language"] == System.DBNull.Value) ? null : r["Language"]),
                            Featured = (Boolean)((r["Featured"] == System.DBNull.Value) ? null : r["Featured"]),
                            FeaturedHome = (Boolean)((r["FeaturedHome"] == System.DBNull.Value) ? null : r["FeaturedHome"]),
                            StaticPage = (Boolean)((r["StaticPage"] == System.DBNull.Value) ? null : r["StaticPage"]),
                            Images = (string)((r["Images"] == System.DBNull.Value) ? null : r["Images"]),
                            ImagesFull = (string)((r["ImagesFull"] == System.DBNull.Value) ? null : r["ImagesFull"]),
                            Params = (string)((r["Params"] == System.DBNull.Value) ? null : r["Params"]),
                            Ordering = (int?)((r["Ordering"] == System.DBNull.Value) ? null : r["Ordering"]),
                            Deleted = (Boolean)((r["Deleted"] == System.DBNull.Value) ? null : r["Deleted"]),
                            Ids = MyModels.Encode((int)r["Id"], SecretId),
                            TotalRows = (int)r["TotalRows"],
                        }).ToList();
            }


        }
        public static List<SelectListItem> GetListItems(Boolean Selected = true)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Articles",
                new string[] { "@flag", "@Selected" }, new object[] { "GetList", Convert.ToDecimal(Selected) });
            List<SelectListItem> ListItems = (from r in tabl.AsEnumerable()
                                              select new SelectListItem
                                              {
                                                  Value = (string)((r["Id"] == System.DBNull.Value) ? null : r["Id"].ToString()),
                                                  Text = (string)((r["Title"] == System.DBNull.Value) ? null : r["Title"]),
                                              }).ToList();

            ListItems.Insert(0, (new SelectListItem { Text = "Chọn bài đăng", Value = "0" }));
            return ListItems;

        }


        public static List<Articles> GetListLogArticles(int Id, string SecretId)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_LogArticles",
                new string[] { "@flag", "@Id" }, new object[] { "GetList", Id });
            if (tabl == null)
            {
                return new List<Articles>();
            }
            else
            {
                return (from r in tabl.AsEnumerable()
                        select new Articles
                        {
                            Id = (int)r["Id"],
                            Title = (string)r["Title"],
                            Alias = (string)((r["Alias"] == System.DBNull.Value) ? null : r["Alias"]),
                            CatId = (int)r["CatId"],
                            Category = (string)r["Category"],
                            AuthorId = (int)((r["AuthorId"] == System.DBNull.Value) ? 0 : r["AuthorId"]),
                            AuthorName = (string)((r["AuthorName"] == System.DBNull.Value) ? null : r["AuthorName"]),
                            IntroText = (string)((r["IntroText"] == System.DBNull.Value) ? null : r["IntroText"]),
                            FullText = (string)((r["FullText"] == System.DBNull.Value) ? null : r["FullText"]),
                            Status = (Boolean)r["Status"],
                            CreatedBy = (int?)((r["CreatedBy"] == System.DBNull.Value) ? null : r["CreatedBy"]),
                            CreatedByName = (string)((r["CreatedByName"] == System.DBNull.Value) ? "" : r["CreatedByName"]),
                            CreatedByFullName = (string)((r["CreatedByFullName"] == System.DBNull.Value) ? "" : r["CreatedByFullName"]),
                            ModifiedBy = (int?)((r["ModifiedBy"] == System.DBNull.Value) ? null : r["ModifiedBy"]),
                            CreatedDate = (DateTime)((r["CreatedDate"] == System.DBNull.Value) ? null : r["CreatedDate"]),
                            PublishUp = (DateTime)((r["PublishUp"] == System.DBNull.Value) ? DateTime.Now : r["PublishUp"]),
                            PublishUpShow = (string)((r["PublishUp"] == System.DBNull.Value) ? DateTime.Now.ToString("dd/MM/yyyy") : (string)((DateTime)r["PublishUp"]).ToString("dd/MM/yyyy")),
                            ModifiedDate = (DateTime?)((r["ModifiedDate"] == System.DBNull.Value) ? null : r["ModifiedDate"]),
                            Metadesc = (string)((r["Metadesc"] == System.DBNull.Value) ? null : r["Metadesc"]),
                            Metakey = (string)((r["Metakey"] == System.DBNull.Value) ? null : r["Metakey"]),
                            Metadata = (string)((r["Metadata"] == System.DBNull.Value) ? null : r["Metadata"]),
                            Language = (string)((r["Language"] == System.DBNull.Value) ? null : r["Language"]),
                            Featured = (Boolean)((r["Featured"] == System.DBNull.Value) ? null : r["Featured"]),
                            FeaturedHome = (Boolean)((r["FeaturedHome"] == System.DBNull.Value) ? null : r["FeaturedHome"]),
                            StaticPage = (Boolean)((r["StaticPage"] == System.DBNull.Value) ? null : r["StaticPage"]),
                            Images = (string)((r["Images"] == System.DBNull.Value) ? null : r["Images"]),
                            ImagesFull = (string)((r["ImagesFull"] == System.DBNull.Value) ? null : r["ImagesFull"]),
                            Params = (string)((r["Params"] == System.DBNull.Value) ? null : r["Params"]),
                            Ids = MyModels.Encode((int)r["Id"], SecretId),
                        }).ToList();
            }

        }
        public static List<SelectListItem> GetListStaticArticle(Boolean Selected = true)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Articles",
                new string[] { "@flag", "@Selected" }, new object[] { "GetListStaticArticle", Convert.ToDecimal(Selected) });
            List<SelectListItem> ListItems = (from r in tabl.AsEnumerable()
                                              select new SelectListItem
                                              {
                                                  Value = (string)((r["Id"] == System.DBNull.Value) ? null : r["Id"].ToString()),
                                                  Text = (string)((r["Title"] == System.DBNull.Value) ? null : r["Title"]),
                                              }).ToList();

            ListItems.Insert(0, (new SelectListItem { Text = "Chọn bài đăng", Value = "0" }));
            return ListItems;

        }

        public static List<Articles> GetList(Boolean Selected = true)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Articles",
                new string[] { "@flag", "@Selected" }, new object[] { "GetList", Convert.ToDecimal(Selected) });
            if (tabl == null)
            {
                return new List<Articles>();
            }
            else
            {
                return (from r in tabl.AsEnumerable()
                        select new Articles
                        {
                            Id = (int)r["Id"],
                            Title = (string)r["Title"],
                            Alias = (string)((r["Alias"] == System.DBNull.Value) ? null : r["Alias"]),
                            CatId = (int)r["CatId"],
                            IntroText = (string)((r["IntroText"] == System.DBNull.Value) ? null : r["IntroText"]),                            
                            Status = (Boolean)r["Status"],
                            CreatedBy = (int?)((r["CreatedBy"] == System.DBNull.Value) ? null : r["CreatedBy"]),
                            ModifiedBy = (int?)((r["ModifiedBy"] == System.DBNull.Value) ? null : r["ModifiedBy"]),
                            CreatedDate = (DateTime)((r["CreatedDate"] == System.DBNull.Value) ? null : r["CreatedDate"]),
                            PublishUp = (DateTime)((r["PublishUp"] == System.DBNull.Value) ? DateTime.Now : r["PublishUp"]),                            
                            ModifiedDate = (DateTime?)((r["ModifiedDate"] == System.DBNull.Value) ? DateTime.Now : r["ModifiedDate"]),                           
                        }).ToList();
            }

        }

        public static List<Articles> GetListRelativeNews(string Alias, int CatId=0)
        {
            if (Alias == null || Alias == "" || CatId == 0)
            {
                return new List<Articles>();
            }
            else
            {
                DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Articles",
                new string[] { "@flag", "@Alias", "@CatId" }, new object[] { "GetListRelativeNews", Alias, CatId });
                if (tabl == null)
                {
                    return new List<Articles>();
                }
                else
                {
                    return (from r in tabl.AsEnumerable()
                            select new Articles
                            {
                                Id = (int)r["Id"],
                                Title = (string)r["Title"],
                                Alias = (string)((r["Alias"] == System.DBNull.Value) ? null : r["Alias"]),
                                CatId = (int)r["CatId"],
                                IntroText = (string)((r["IntroText"] == System.DBNull.Value) ? null : r["IntroText"]),
                                FullText = (string)((r["FullText"] == System.DBNull.Value) ? null : r["FullText"]),
                                Status = (Boolean)r["Status"],
                                CreatedBy = (int?)((r["CreatedBy"] == System.DBNull.Value) ? null : r["CreatedBy"]),
                                ModifiedBy = (int?)((r["ModifiedBy"] == System.DBNull.Value) ? null : r["ModifiedBy"]),
                                CreatedDate = (DateTime)((r["CreatedDate"] == System.DBNull.Value) ? null : r["CreatedDate"]),
                                PublishUp = (DateTime)((r["PublishUp"] == System.DBNull.Value) ? DateTime.Now : r["PublishUp"]),
                                PublishUpShow = (string)((r["PublishUp"] == System.DBNull.Value) ? DateTime.Now.ToString("dd/MM/yyyy") : (string)((DateTime)r["PublishUp"]).ToString("dd/MM/yyyy")),
                                ModifiedDate = (DateTime?)((r["ModifiedDate"] == System.DBNull.Value) ? null : r["ModifiedDate"]),
                                Metadesc = (string)((r["Metadesc"] == System.DBNull.Value) ? null : r["Metadesc"]),
                                Metakey = (string)((r["Metakey"] == System.DBNull.Value) ? null : r["Metakey"]),
                                Metadata = (string)((r["Metadata"] == System.DBNull.Value) ? null : r["Metadata"]),
                                Language = (string)((r["Language"] == System.DBNull.Value) ? null : r["Language"]),
                                Featured = (Boolean)((r["Featured"] == System.DBNull.Value) ? null : r["Featured"]),
                                FeaturedHome = (Boolean)((r["FeaturedHome"] == System.DBNull.Value) ? null : r["FeaturedHome"]),
                                StaticPage = (Boolean)((r["StaticPage"] == System.DBNull.Value) ? null : r["StaticPage"]),
                                Images = (string)((r["Images"] == System.DBNull.Value) ? null : r["Images"]),
                                ImagesFull = (string)((r["ImagesFull"] == System.DBNull.Value) ? null : r["ImagesFull"]),
                                Params = (string)((r["Params"] == System.DBNull.Value) ? null : r["Params"]),
                                Ordering = (int?)((r["Ordering"] == System.DBNull.Value) ? null : r["Ordering"]),
                                Deleted = (Boolean)((r["Deleted"] == System.DBNull.Value) ? null : r["Deleted"])
                            }).ToList();
                }
            }


        }
        public static List<Articles> GetListHot(int IdCoQuan = 0)
        {
            if (IdCoQuan == 0)
            {
                IdCoQuan = 1;
            }
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Articles",
                new string[] { "@flag", "@IdCoQuan" }, new object[] { "GetListHot", IdCoQuan });
            if (tabl == null)
            {
                return new List<Articles>();
            }
            else
            {
                return (from r in tabl.AsEnumerable()
                        select new Articles
                        {
                            Id = (int)r["Id"],
                            Title = (string)r["Title"],
                            Alias = (string)((r["Alias"] == System.DBNull.Value) ? null : r["Alias"]),
                            CatId = (int)r["CatId"],
                            IntroText = (string)((r["IntroText"] == System.DBNull.Value) ? null : r["IntroText"]),
                            FullText = (string)((r["FullText"] == System.DBNull.Value) ? null : r["FullText"]),
                            Status = (Boolean)r["Status"],
                            CreatedBy = (int?)((r["CreatedBy"] == System.DBNull.Value) ? null : r["CreatedBy"]),
                            ModifiedBy = (int?)((r["ModifiedBy"] == System.DBNull.Value) ? null : r["ModifiedBy"]),
                            CreatedDate = (DateTime)((r["CreatedDate"] == System.DBNull.Value) ? null : r["CreatedDate"]),
                            PublishUp = (DateTime)((r["PublishUp"] == System.DBNull.Value) ? DateTime.Now : r["PublishUp"]),
                            PublishUpShow = (string)((r["PublishUp"] == System.DBNull.Value) ? DateTime.Now.ToString("dd/MM/yyyy") : (string)((DateTime)r["PublishUp"]).ToString("dd/MM/yyyy")),
                            ModifiedDate = (DateTime?)((r["ModifiedDate"] == System.DBNull.Value) ? null : r["ModifiedDate"]),
                            Metadesc = (string)((r["Metadesc"] == System.DBNull.Value) ? null : r["Metadesc"]),
                            Metakey = (string)((r["Metakey"] == System.DBNull.Value) ? null : r["Metakey"]),
                            Metadata = (string)((r["Metadata"] == System.DBNull.Value) ? null : r["Metadata"]),
                            Language = (string)((r["Language"] == System.DBNull.Value) ? null : r["Language"]),
                            Featured = (Boolean)((r["Featured"] == System.DBNull.Value) ? null : r["Featured"]),
                            FeaturedHome = (Boolean)((r["FeaturedHome"] == System.DBNull.Value) ? null : r["FeaturedHome"]),
                            StaticPage = (Boolean)((r["StaticPage"] == System.DBNull.Value) ? null : r["StaticPage"]),
                            Images = (string)((r["Images"] == System.DBNull.Value) ? null : r["Images"]),
                            ImagesFull = (string)((r["ImagesFull"] == System.DBNull.Value) ? null : r["ImagesFull"]),
                            Params = (string)((r["Params"] == System.DBNull.Value) ? null : r["Params"]),
                            Ordering = (int?)((r["Ordering"] == System.DBNull.Value) ? null : r["Ordering"]),
                            Deleted = (Boolean)((r["Deleted"] == System.DBNull.Value) ? null : r["Deleted"])
                        }).ToList();
            }

        }
        public static List<Articles> GetListTinTuc()
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Articles",
                new string[] { "@flag" }, new object[] { "GetListTinTuc" });
            if (tabl == null)
            {
                return new List<Articles>();
            }
            else
            {
                return (from r in tabl.AsEnumerable()
                        select new Articles
                        {
                            Id = (int)r["Id"],
                            Title = (string)r["Title"],
                            Alias = (string)((r["Alias"] == System.DBNull.Value) ? null : r["Alias"]),
                            CatId = (int)r["CatId"],
                            IntroText = (string)((r["IntroText"] == System.DBNull.Value) ? null : r["IntroText"]),
                            FullText = (string)((r["FullText"] == System.DBNull.Value) ? null : r["FullText"]),
                            Status = (Boolean)r["Status"],
                            CreatedBy = (int?)((r["CreatedBy"] == System.DBNull.Value) ? null : r["CreatedBy"]),
                            ModifiedBy = (int?)((r["ModifiedBy"] == System.DBNull.Value) ? null : r["ModifiedBy"]),
                            CreatedDate = (DateTime)((r["CreatedDate"] == System.DBNull.Value) ? null : r["CreatedDate"]),
                            PublishUp = (DateTime)((r["PublishUp"] == System.DBNull.Value) ? DateTime.Now : r["PublishUp"]),
                            PublishUpShow = (string)((r["PublishUp"] == System.DBNull.Value) ? DateTime.Now.ToString("dd/MM/yyyy") : (string)((DateTime)r["PublishUp"]).ToString("dd/MM/yyyy")),
                            ModifiedDate = (DateTime?)((r["ModifiedDate"] == System.DBNull.Value) ? null : r["ModifiedDate"]),
                            Metadesc = (string)((r["Metadesc"] == System.DBNull.Value) ? null : r["Metadesc"]),
                            Metakey = (string)((r["Metakey"] == System.DBNull.Value) ? null : r["Metakey"]),
                            Metadata = (string)((r["Metadata"] == System.DBNull.Value) ? null : r["Metadata"]),
                            Language = (string)((r["Language"] == System.DBNull.Value) ? null : r["Language"]),
                            Featured = (Boolean)((r["Featured"] == System.DBNull.Value) ? null : r["Featured"]),
                            FeaturedHome = (Boolean)((r["FeaturedHome"] == System.DBNull.Value) ? null : r["FeaturedHome"]),
                            StaticPage = (Boolean)((r["StaticPage"] == System.DBNull.Value) ? null : r["StaticPage"]),
                            Images = (string)((r["Images"] == System.DBNull.Value) ? null : r["Images"]),
                            ImagesFull = (string)((r["ImagesFull"] == System.DBNull.Value) ? null : r["ImagesFull"]),
                            Params = (string)((r["Params"] == System.DBNull.Value) ? null : r["Params"]),
                            Ordering = (int?)((r["Ordering"] == System.DBNull.Value) ? null : r["Ordering"]),
                            Deleted = (Boolean)((r["Deleted"] == System.DBNull.Value) ? null : r["Deleted"])
                        }).ToList();
            }

        }
        public static List<Articles> GetListSuKien()
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Articles",
                new string[] { "@flag" }, new object[] { "GetListSuKien" });
            if (tabl == null)
            {
                return new List<Articles>();
            }
            else
            {
                return (from r in tabl.AsEnumerable()
                        select new Articles
                        {
                            Id = (int)r["Id"],
                            Title = (string)r["Title"],
                            Alias = (string)((r["Alias"] == System.DBNull.Value) ? null : r["Alias"]),
                            CatId = (int)r["CatId"],
                            IntroText = (string)((r["IntroText"] == System.DBNull.Value) ? null : r["IntroText"]),
                            FullText = (string)((r["FullText"] == System.DBNull.Value) ? null : r["FullText"]),
                            Status = (Boolean)r["Status"],
                            CreatedBy = (int?)((r["CreatedBy"] == System.DBNull.Value) ? null : r["CreatedBy"]),
                            ModifiedBy = (int?)((r["ModifiedBy"] == System.DBNull.Value) ? null : r["ModifiedBy"]),
                            CreatedDate = (DateTime)((r["CreatedDate"] == System.DBNull.Value) ? null : r["CreatedDate"]),
                            PublishUp = (DateTime)((r["PublishUp"] == System.DBNull.Value) ? DateTime.Now : r["PublishUp"]),
                            PublishUpShow = (string)((r["PublishUp"] == System.DBNull.Value) ? DateTime.Now.ToString("dd/MM/yyyy") : (string)((DateTime)r["PublishUp"]).ToString("dd/MM/yyyy")),
                            ModifiedDate = (DateTime?)((r["ModifiedDate"] == System.DBNull.Value) ? null : r["ModifiedDate"]),
                            Metadesc = (string)((r["Metadesc"] == System.DBNull.Value) ? null : r["Metadesc"]),
                            Metakey = (string)((r["Metakey"] == System.DBNull.Value) ? null : r["Metakey"]),
                            Metadata = (string)((r["Metadata"] == System.DBNull.Value) ? null : r["Metadata"]),
                            Language = (string)((r["Language"] == System.DBNull.Value) ? null : r["Language"]),
                            Featured = (Boolean)((r["Featured"] == System.DBNull.Value) ? null : r["Featured"]),
                            StaticPage = (Boolean)((r["StaticPage"] == System.DBNull.Value) ? null : r["StaticPage"]),
                            Images = (string)((r["Images"] == System.DBNull.Value) ? null : r["Images"]),
                            ImagesFull = (string)((r["ImagesFull"] == System.DBNull.Value) ? null : r["ImagesFull"]),
                            Params = (string)((r["Params"] == System.DBNull.Value) ? null : r["Params"]),
                            Ordering = (int?)((r["Ordering"] == System.DBNull.Value) ? null : r["Ordering"]),
                            Deleted = (Boolean)((r["Deleted"] == System.DBNull.Value) ? null : r["Deleted"])
                        }).ToList();
            }

        }
        public static List<Articles> GetListNew(int CatId = 0, int Limit = 6, int IdCoQuan = 1)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Articles",
                new string[] { "@flag", "@CatId", "@IdCoQuan" }, new object[] { "GetListNew", CatId, IdCoQuan });
            if (tabl == null)
            {
                return new List<Articles>();
            }
            else
            {
                return (from r in tabl.AsEnumerable()
                        select new Articles
                        {
                            Id = (int)r["Id"],
                            Title = (string)r["Title"],
                            Alias = (string)((r["Alias"] == System.DBNull.Value) ? null : r["Alias"]),
                            CatId = (int)r["CatId"],
                            IntroText = (string)((r["IntroText"] == System.DBNull.Value) ? null : r["IntroText"]),
                            FullText = (string)((r["FullText"] == System.DBNull.Value) ? null : r["FullText"]),
                            Status = (Boolean)r["Status"],
                            CreatedBy = (int?)((r["CreatedBy"] == System.DBNull.Value) ? null : r["CreatedBy"]),
                            ModifiedBy = (int?)((r["ModifiedBy"] == System.DBNull.Value) ? null : r["ModifiedBy"]),
                            CreatedDate = (DateTime)((r["CreatedDate"] == System.DBNull.Value) ? DateTime.Now : r["CreatedDate"]),
                            PublishUp = (DateTime)((r["PublishUp"] == System.DBNull.Value) ? DateTime.Now : r["PublishUp"]),
                            PublishUpShow = (string)((r["PublishUp"] == System.DBNull.Value) ? DateTime.Now.ToString("dd/MM/yyyy") : (string)((DateTime)r["PublishUp"]).ToString("dd/MM/yyyy")),
                            ModifiedDate = (DateTime?)((r["ModifiedDate"] == System.DBNull.Value) ? null : r["ModifiedDate"]),
                            Metadesc = (string)((r["Metadesc"] == System.DBNull.Value) ? null : r["Metadesc"]),
                            Metakey = (string)((r["Metakey"] == System.DBNull.Value) ? null : r["Metakey"]),
                            Metadata = (string)((r["Metadata"] == System.DBNull.Value) ? null : r["Metadata"]),
                            Language = (string)((r["Language"] == System.DBNull.Value) ? null : r["Language"]),
                            Featured = (Boolean)((r["Featured"] == System.DBNull.Value) ? null : r["Featured"]),
                            StaticPage = (Boolean)((r["StaticPage"] == System.DBNull.Value) ? null : r["StaticPage"]),
                            Images = (string)((r["Images"] == System.DBNull.Value) ? null : r["Images"]),
                            ImagesFull = (string)((r["ImagesFull"] == System.DBNull.Value) ? null : r["ImagesFull"]),
                            Params = (string)((r["Params"] == System.DBNull.Value) ? null : r["Params"]),
                            Ordering = (int?)((r["Ordering"] == System.DBNull.Value) ? null : r["Ordering"]),
                            Deleted = (Boolean)((r["Deleted"] == System.DBNull.Value) ? null : r["Deleted"])
                        }).ToList();
            }

        }

        public static List<Articles> GetListFeaturedHome(int CatId = 0, int Limit = 6, int IdCoQuan = 1)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Articles",
                new string[] { "@flag", "@CatId", "@IdCoQuan" }, new object[] { "GetListFeaturedHome", CatId, IdCoQuan });
            if (tabl == null)
            {
                return new List<Articles>();
            }
            else
            {
                return (from r in tabl.AsEnumerable()
                        select new Articles
                        {
                            Id = (int)r["Id"],
                            Title = (string)r["Title"],
                            Alias = (string)((r["Alias"] == System.DBNull.Value) ? null : r["Alias"]),
                            CatId = (int)r["CatId"],
                            IntroText = (string)((r["IntroText"] == System.DBNull.Value) ? null : r["IntroText"]),
                            FullText = (string)((r["FullText"] == System.DBNull.Value) ? null : r["FullText"]),
                            Status = (Boolean)r["Status"],
                            CreatedBy = (int?)((r["CreatedBy"] == System.DBNull.Value) ? null : r["CreatedBy"]),
                            ModifiedBy = (int?)((r["ModifiedBy"] == System.DBNull.Value) ? null : r["ModifiedBy"]),
                            CreatedDate = (DateTime)((r["CreatedDate"] == System.DBNull.Value) ? DateTime.Now : r["CreatedDate"]),
                            PublishUp = (DateTime)((r["PublishUp"] == System.DBNull.Value) ? DateTime.Now : r["PublishUp"]),
                            PublishUpShow = (string)((r["PublishUp"] == System.DBNull.Value) ? DateTime.Now.ToString("dd/MM/yyyy") : (string)((DateTime)r["PublishUp"]).ToString("dd/MM/yyyy")),
                            ModifiedDate = (DateTime?)((r["ModifiedDate"] == System.DBNull.Value) ? null : r["ModifiedDate"]),
                            Metadesc = (string)((r["Metadesc"] == System.DBNull.Value) ? null : r["Metadesc"]),
                            Metakey = (string)((r["Metakey"] == System.DBNull.Value) ? null : r["Metakey"]),
                            Metadata = (string)((r["Metadata"] == System.DBNull.Value) ? null : r["Metadata"]),
                            Language = (string)((r["Language"] == System.DBNull.Value) ? null : r["Language"]),
                            Featured = (Boolean)((r["Featured"] == System.DBNull.Value) ? null : r["Featured"]),
                            StaticPage = (Boolean)((r["StaticPage"] == System.DBNull.Value) ? null : r["StaticPage"]),
                            Images = (string)((r["Images"] == System.DBNull.Value) ? null : r["Images"]),
                            ImagesFull = (string)((r["ImagesFull"] == System.DBNull.Value) ? null : r["ImagesFull"]),
                            Params = (string)((r["Params"] == System.DBNull.Value) ? null : r["Params"]),
                            Ordering = (int?)((r["Ordering"] == System.DBNull.Value) ? null : r["Ordering"]),
                            Deleted = (Boolean)((r["Deleted"] == System.DBNull.Value) ? null : r["Deleted"])
                        }).ToList();
            }

        }

        public static Articles GetItem(decimal Id, string SecretId = null,string Culture="all")
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Articles",
            new string[] { "@flag", "@Id" }, new object[] { "GetItem", Id });
            Articles Item = (from r in tabl.AsEnumerable()
                             select new Articles
                             {
                                 Id = (int)r["Id"],
                                 Title = (string)r["Title"],
                                 Str_ListFile = (string)((r["Str_ListFile"] == System.DBNull.Value) ? null : r["Str_ListFile"]),
                                 Str_Link = (string)((r["Str_Link"] == System.DBNull.Value) ? null : r["Str_Link"]),
                                 Alias = (string)((r["Alias"] == System.DBNull.Value) ? null : r["Alias"]),
                                 CatId = (int)r["CatId"],
                                 IntroText = (string)((r["IntroText"] == System.DBNull.Value) ? null : r["IntroText"]),
                                 FullText = (string)((r["FullText"] == System.DBNull.Value) ? null : r["FullText"]),
                                 Status = (Boolean)r["Status"],
                                 CreatedBy = (int?)((r["CreatedBy"] == System.DBNull.Value) ? null : r["CreatedBy"]),
                                 ModifiedBy = (int?)((r["ModifiedBy"] == System.DBNull.Value) ? null : r["ModifiedBy"]),
                                 CreatedDate = (DateTime)((r["CreatedDate"] == System.DBNull.Value) ? null : r["CreatedDate"]),
                                 PublishUp = (DateTime)((r["PublishUp"] == System.DBNull.Value) ? null : r["PublishUp"]),
                                 PublishUpShow = (string)((r["PublishUp"] == System.DBNull.Value) ? DateTime.Now.ToString("dd/MM/yyyy") : (string)((DateTime)r["PublishUp"]).ToString("dd/MM/yyyy")),
                                 ModifiedDate = (DateTime?)((r["ModifiedDate"] == System.DBNull.Value) ? null : r["ModifiedDate"]),
                                 Metadesc = (string)((r["Metadesc"] == System.DBNull.Value) ? null : r["Metadesc"]),
                                 Metakey = (string)((r["Metakey"] == System.DBNull.Value) ? null : r["Metakey"]),
                                 Metadata = (string)((r["Metadata"] == System.DBNull.Value) ? null : r["Metadata"]),                                 
                                 Language = (string)((r["Language"] == System.DBNull.Value) ? null : r["Language"]),
                                 Featured = (Boolean)((r["Featured"] == System.DBNull.Value) ? null : r["Featured"]),
                                 StaticPage = (Boolean)((r["StaticPage"] == System.DBNull.Value) ? null : r["StaticPage"]),
                                 Images = (string)((r["Images"] == System.DBNull.Value) ? null : r["Images"]),
                                 FileItem = (string)((r["FileItem"] == System.DBNull.Value) ? null : r["FileItem"]),
                                 ImagesFull = (string)((r["ImagesFull"] == System.DBNull.Value) ? null : r["ImagesFull"]),
                                 Params = (string)((r["Params"] == System.DBNull.Value) ? null : r["Params"]),
                                 Ordering = (int?)((r["Ordering"] == System.DBNull.Value) ? null : r["Ordering"]),
                                 Deleted = (Boolean)((r["Deleted"] == System.DBNull.Value) ? null : r["Deleted"]),
                                 IdCoQuan = (int)((r["IdCoQuan"] == System.DBNull.Value) ? 0 : r["IdCoQuan"]),
                                 AuthorId = (int)((r["AuthorId"] == System.DBNull.Value) ? 0 : r["AuthorId"]),
                                 RootNewsFlag = (Boolean)((r["RootNewsFlag"] == System.DBNull.Value) ? false : r["RootNewsFlag"]),
                                 RootNewsId = (int)((r["RootNewsId"] == System.DBNull.Value) ? 0 : r["RootNewsId"]),
                                 Member = (int)((r["Member"] == System.DBNull.Value) ? 0 : r["Member"]),
                                 Ids = MyModels.Encode((int)r["Id"], SecretId),
                                 Title_EN = (string)((r["Title_EN"] == System.DBNull.Value) ? null : r["Title_EN"]),
                                 Alias_EN = (string)((r["Alias_EN"] == System.DBNull.Value) ? null : r["Alias_EN"]),
                                 IntroText_EN = (string)((r["IntroText_EN"] == System.DBNull.Value) ? null : r["IntroText_EN"]),
                                 FullText_EN = (string)((r["FullText_EN"] == System.DBNull.Value) ? null : r["FullText_EN"]),
                                 Metadesc_EN = (string)((r["Metadesc_EN"] == System.DBNull.Value) ? null : r["Metadesc_EN"]),
                                 Metakey_EN = (string)((r["Metakey_EN"] == System.DBNull.Value) ? null : r["Metakey_EN"]),
                                 Metadata_EN = (string)((r["Metadata_EN"] == System.DBNull.Value) ? null : r["Metadata_EN"]),
                             }).FirstOrDefault();
            if (Item != null)
            {
                if (Item.Str_ListFile != null && Item.Str_ListFile != "")
                {
                    Item.ListFile = JsonConvert.DeserializeObject<List<FileArticle>>(Item.Str_ListFile);
                }
                if (Item.Str_Link != null && Item.Str_Link != "")
                {
                    Item.ListLinkArticle = JsonConvert.DeserializeObject<List<LinkArticle>>(Item.Str_Link);
                }
            }
            if (Item.Metadata != null)
            {
                try
                {
                    Item.MetadataCV = JsonConvert.DeserializeObject<API.Models.MetaData>(Item.Metadata);
                }
                catch {
                    Item.MetadataCV = new API.Models.MetaData() { MetaTitle = Item.Title };
                }
            }
            else
            {
                Item.MetadataCV = new API.Models.MetaData() { MetaTitle = Item.Title, MetaH1 = Item.Title, MetaH3 = Item.Title };
            }

            if (Item.Metadata_EN != null)
            {
                try
                {
                    Item.MetadataCV_EN = JsonConvert.DeserializeObject<API.Models.MetaData>(Item.Metadata_EN);
                }
                catch
                {
                    Item.MetadataCV_EN = new API.Models.MetaData() { MetaTitle = Item.Title_EN, MetaH1 = Item.Title_EN, MetaH3 = Item.Title_EN };
                }
            }
            else
            {
                Item.MetadataCV_EN = new API.Models.MetaData() { MetaTitle = Item.Title_EN, MetaH1 = Item.Title_EN, MetaH3 = Item.Title_EN };
            }

            if (Culture == "en")
            {
                if (Item.Title_EN != null && Item.Title_EN != "") { Item.Title = Item.Title_EN; }
                if (Item.Alias_EN != null && Item.Alias_EN != "") { Item.Alias = Item.Alias_EN; }
                if (Item.IntroText_EN != null && Item.IntroText_EN != "") { Item.IntroText = Item.IntroText_EN; }
                if (Item.FullText_EN != null && Item.FullText_EN != "") { Item.FullText = Item.FullText_EN; }
                if (Item.Metadata_EN != null && Item.Metadata_EN != "") { Item.Metadata = Item.Metadata_EN; }
                if (Item.Metadesc_EN != null && Item.Metadesc_EN != "") { Item.Metadesc = Item.Metadesc_EN; }
                if (Item.Metakey_EN != null && Item.Metakey_EN != "") { Item.Metakey = Item.Metakey_EN; }
            }
            return Item;
        }
        public static Articles GetItemByAlias(string Alias, string SecretId = null, string Culture = "all")
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Articles",
            new string[] { "@flag", "@Alias" }, new object[] { "GetItemByAlias", Alias });
            Articles Item = (from r in tabl.AsEnumerable()
                             select new Articles
                             {
                                 Id = (int)r["Id"],
                                 Title = (string)r["Title"],
                                 Alias = (string)((r["Alias"] == System.DBNull.Value) ? null : r["Alias"]),
                                 Str_ListFile = (string)((r["Str_ListFile"] == System.DBNull.Value) ? null : r["Str_ListFile"]),
                                 Str_Link = (string)((r["Str_Link"] == System.DBNull.Value) ? null : r["Str_Link"]),
                                 CatId = (int)r["CatId"],
                                 IntroText = (string)((r["IntroText"] == System.DBNull.Value) ? null : r["IntroText"]),
                                 FullText = (string)((r["FullText"] == System.DBNull.Value) ? null : r["FullText"]),
                                 Status = (Boolean)r["Status"],
                                 CreatedBy = (int?)((r["CreatedBy"] == System.DBNull.Value) ? null : r["CreatedBy"]),
                                 ModifiedBy = (int?)((r["ModifiedBy"] == System.DBNull.Value) ? null : r["ModifiedBy"]),
                                 CreatedDate = (DateTime)((r["CreatedDate"] == System.DBNull.Value) ? null : r["CreatedDate"]),
                                 PublishUp = (DateTime)((r["PublishUp"] == System.DBNull.Value) ? null : r["PublishUp"]),
                                 PublishUpShow = (string)((r["PublishUp"] == System.DBNull.Value) ? DateTime.Now.ToString("dd/MM/yyyy") : (string)((DateTime)r["PublishUp"]).ToString("dd/MM/yyyy")),
                                 ModifiedDate = (DateTime?)((r["ModifiedDate"] == System.DBNull.Value) ? null : r["ModifiedDate"]),
                                 Metadesc = (string)((r["Metadesc"] == System.DBNull.Value) ? null : r["Metadesc"]),
                                 Metakey = (string)((r["Metakey"] == System.DBNull.Value) ? null : r["Metakey"]),
                                 Metadata = (string)((r["Metadata"] == System.DBNull.Value) ? null : r["Metadata"]),                                 
                                 Language = (string)((r["Language"] == System.DBNull.Value) ? null : r["Language"]),
                                 Featured = (Boolean)((r["Featured"] == System.DBNull.Value) ? null : r["Featured"]),
                                 StaticPage = (Boolean)((r["StaticPage"] == System.DBNull.Value) ? null : r["StaticPage"]),
                                 Images = (string)((r["Images"] == System.DBNull.Value) ? null : r["Images"]),
                                 FileItem = (string)((r["FileItem"] == System.DBNull.Value) ? null : r["FileItem"]),
                                 ImagesFull = (string)((r["ImagesFull"] == System.DBNull.Value) ? null : r["ImagesFull"]),
                                 Params = (string)((r["Params"] == System.DBNull.Value) ? null : r["Params"]),
                                 Ordering = (int?)((r["Ordering"] == System.DBNull.Value) ? null : r["Ordering"]),
                                 Deleted = (Boolean)((r["Deleted"] == System.DBNull.Value) ? null : r["Deleted"]),
                                 AuthorId = (int)((r["AuthorId"] == System.DBNull.Value) ? 0 : r["AuthorId"]),
                                 Member = (int)((r["Member"] == System.DBNull.Value) ? 0 : r["Member"]),                                 
                                 Ids = MyModels.Encode((int)r["Id"], SecretId),
                                 Hit = (int)r["Hit"],
                                 Title_EN = (string)((r["Title_EN"] == System.DBNull.Value) ? null : r["Title_EN"]),
                                 Alias_EN = (string)((r["Alias_EN"] == System.DBNull.Value) ? null : r["Alias_EN"]),
                                 IntroText_EN = (string)((r["IntroText_EN"] == System.DBNull.Value) ? null : r["IntroText_EN"]),
                                 FullText_EN = (string)((r["FullText_EN"] == System.DBNull.Value) ? null : r["FullText_EN"]),
                                 Metadesc_EN = (string)((r["Metadesc_EN"] == System.DBNull.Value) ? null : r["Metadesc_EN"]),
                                 Metakey_EN = (string)((r["Metakey_EN"] == System.DBNull.Value) ? null : r["Metakey_EN"]),
                                 Metadata_EN = (string)((r["Metadata_EN"] == System.DBNull.Value) ? null : r["Metadata_EN"]),
                             }).FirstOrDefault();

            if (Item.Str_ListFile != null && Item.Str_ListFile != "")
            {
                Item.ListFile = JsonConvert.DeserializeObject<List<FileArticle>>(Item.Str_ListFile);
            }

            if (Item.Str_Link != null && Item.Str_Link != "")
            {
                Item.ListLinkArticle = JsonConvert.DeserializeObject<List<LinkArticle>>(Item.Str_Link);
            }

            if (Item.Metadata != null)
            {
                Item.MetadataCV = JsonConvert.DeserializeObject<API.Models.MetaData>(Item.Metadata);
            }
            else
            {
                Item.MetadataCV = new API.Models.MetaData() { MetaTitle = Item.Title,MetaH1=  Item.Title ,MetaH3 = Item.Title };
            }

            if (Item.Metadata_EN != null)
            {
                try
                {
                    Item.MetadataCV_EN = JsonConvert.DeserializeObject<API.Models.MetaData>(Item.Metadata_EN);
                }
                catch
                {
                    Item.MetadataCV_EN = new API.Models.MetaData() { MetaTitle = Item.Title_EN, MetaH1 = Item.Title_EN, MetaH3 = Item.Title_EN };
                }
            }
            else
            {
                Item.MetadataCV_EN = new API.Models.MetaData() { MetaTitle = Item.Title_EN, MetaH1 = Item.Title_EN, MetaH3 = Item.Title_EN };
            }
            if (Culture == "en")
            {
                if (Item.Title_EN != null && Item.Title_EN != ""){ Item.Title = Item.Title_EN;}
                if (Item.Alias_EN != null && Item.Alias_EN != ""){ Item.Alias = Item.Alias_EN; }
                if (Item.IntroText_EN != null && Item.IntroText_EN != ""){ Item.IntroText = Item.IntroText_EN; }
                if (Item.FullText_EN != null && Item.FullText_EN != ""){ Item.FullText = Item.FullText_EN; }
                if (Item.Metadata_EN != null && Item.Metadata_EN != ""){ Item.Metadata = Item.Metadata_EN; }
                if (Item.Metadesc_EN != null && Item.Metadesc_EN != ""){ Item.Metadesc = Item.Metadesc_EN; }
                if (Item.Metakey_EN != null && Item.Metakey_EN != ""){ Item.Metakey = Item.Metakey_EN; }
                
                
            }
            return Item;
        }


        public static Articles GetItemLogArticle(decimal Id, string SecretId = null)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_LogArticles",
            new string[] { "@flag", "@Id" }, new object[] { "GetItem", Id });
            Articles Item = (from r in tabl.AsEnumerable()
                             select new Articles
                             {
                                 Id = (int)r["Id"],
                                 Title = (string)r["Title"],
                                 Str_ListFile = (string)((r["Str_ListFile"] == System.DBNull.Value) ? null : r["Str_ListFile"]),
                                 Str_Link = (string)((r["Str_Link"] == System.DBNull.Value) ? null : r["Str_Link"]),
                                 Alias = (string)((r["Alias"] == System.DBNull.Value) ? null : r["Alias"]),
                                 CatId = (int)r["CatId"],
                                 IntroText = (string)((r["IntroText"] == System.DBNull.Value) ? null : r["IntroText"]),
                                 FullText = (string)((r["FullText"] == System.DBNull.Value) ? null : r["FullText"]),
                                 Status = (Boolean)r["Status"],
                                 CreatedBy = (int?)((r["CreatedBy"] == System.DBNull.Value) ? null : r["CreatedBy"]),
                                 ModifiedBy = (int?)((r["ModifiedBy"] == System.DBNull.Value) ? null : r["ModifiedBy"]),
                                 CreatedDate = (DateTime)((r["CreatedDate"] == System.DBNull.Value) ? null : r["CreatedDate"]),
                                 PublishUp = (DateTime)((r["PublishUp"] == System.DBNull.Value) ? null : r["PublishUp"]),
                                 PublishUpShow = (string)((r["PublishUp"] == System.DBNull.Value) ? DateTime.Now.ToString("dd/MM/yyyy") : (string)((DateTime)r["PublishUp"]).ToString("dd/MM/yyyy")),
                                 ModifiedDate = (DateTime?)((r["ModifiedDate"] == System.DBNull.Value) ? null : r["ModifiedDate"]),
                                 Metadesc = (string)((r["Metadesc"] == System.DBNull.Value) ? null : r["Metadesc"]),
                                 Metakey = (string)((r["Metakey"] == System.DBNull.Value) ? null : r["Metakey"]),
                                 Metadata = (string)((r["Metadata"] == System.DBNull.Value) ? null : r["Metadata"]),                                 
                                 Language = (string)((r["Language"] == System.DBNull.Value) ? null : r["Language"]),
                                 Featured = (Boolean)((r["Featured"] == System.DBNull.Value) ? null : r["Featured"]),
                                 StaticPage = (Boolean)((r["StaticPage"] == System.DBNull.Value) ? null : r["StaticPage"]),
                                 Images = (string)((r["Images"] == System.DBNull.Value) ? null : r["Images"]),
                                 ImagesFull = (string)((r["ImagesFull"] == System.DBNull.Value) ? null : r["ImagesFull"]),
                                 Params = (string)((r["Params"] == System.DBNull.Value) ? null : r["Params"]),
                                 Ordering = (int?)((r["Ordering"] == System.DBNull.Value) ? null : r["Ordering"]),
                                 IdCoQuan = (int)((r["IdCoQuan"] == System.DBNull.Value) ? 0 : r["IdCoQuan"]),
                                 AuthorId = (int)((r["AuthorId"] == System.DBNull.Value) ? 0 : r["AuthorId"]),
                                 Ids = MyModels.Encode((int)r["Id"], SecretId),
                             }).FirstOrDefault();
            if (Item.Str_ListFile != null && Item.Str_ListFile != "")
            {
                Item.ListFile = JsonConvert.DeserializeObject<List<FileArticle>>(Item.Str_ListFile);
            }
            if (Item.Str_Link != null && Item.Str_Link != "")
            {
                Item.ListLinkArticle = JsonConvert.DeserializeObject<List<LinkArticle>>(Item.Str_Link);
            }
            return Item;
        }

        public static dynamic SaveItem(Articles dto)
        {
            string Str_ListFile = null;
            string Str_Link = null;
            List<FileArticle> ListFileArticle = new List<FileArticle>();
            List<LinkArticle> ListLinkArticle = new List<LinkArticle>();
            if (dto.CatId == 0)
            {
                dto.CatId = 1;
            }

            if (dto.ListFile != null && dto.ListFile.Count() > 0)
            {
                for (int i = 0; i < dto.ListFile.Count(); i++)
                {
                    if (dto.ListFile[i].FilePath != null && dto.ListFile[i].FilePath.Trim() != "")
                    {
                        ListFileArticle.Add(dto.ListFile[i]);
                    }
                }
                if (ListFileArticle != null && ListFileArticle.Count() > 0)
                {
                    Str_ListFile = JsonConvert.SerializeObject(ListFileArticle);
                }

            }


            if (dto.ListLinkArticle != null && dto.ListLinkArticle.Count() > 0)
            {
                for (int i = 0; i < dto.ListLinkArticle.Count(); i++)
                {
                    if (dto.ListLinkArticle[i].Title != null && dto.ListLinkArticle[i].Title.Trim() != "" && dto.ListLinkArticle[i].Status == true)
                    {
                        ListLinkArticle.Add(dto.ListLinkArticle[i]);
                    }
                }
                if (ListLinkArticle != null && ListLinkArticle.Count() > 0)
                {
                    Str_Link = JsonConvert.SerializeObject(ListLinkArticle);
                }

            }

            if (dto.MetadataCV.MetaTitle == null || dto.MetadataCV.MetaTitle.Trim() == "")
            {
                dto.MetadataCV.MetaTitle = dto.Title;
                dto.MetadataCV.MetaH1 = dto.Title;
                dto.MetadataCV.MetaH3 = dto.Title;
            }

            if (dto.MetadataCV_EN != null)
            {
                if (dto.MetadataCV_EN.MetaTitle == null || dto.MetadataCV_EN.MetaTitle.Trim() == "")
                {
                    dto.MetadataCV_EN.MetaTitle = dto.Title_EN;
                    dto.MetadataCV_EN.MetaH1 = dto.Title_EN;
                    dto.MetadataCV_EN.MetaH3 = dto.Title_EN;
                }
                dto.Metadata_EN = JsonConvert.SerializeObject(dto.MetadataCV_EN);
            }

            DateTime NgayDang = DateTime.ParseExact(dto.PublishUpShow, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Articles",
            new string[] { "@flag", "@Id", "@Title", "@Alias", "@CatId", "@IntroText", "@FullText", "@Status", "@CreatedBy", "@ModifiedBy", "@CreatedDate", "@ModifiedDate", "@Metadesc", "@Metakey", "@Metadata","@Language", "@Featured", "@StaticPage", "@Images", "@ImagesFull", "@FileItem", "@Params", "@Ordering", "@Deleted", "@IdCoQuan", "@FeaturedHome", "@PublishUp", "@Str_ListFile", "@Str_Link", "@AuthorId", "@RootNewsId", "@RootNewsFlag", "@Title_EN", "@Alias_EN", "@IntroText_EN", "@FullText_EN", "@Metadesc_EN", "@Metakey_EN", "@Metadata_EN" },
            new object[] { "SaveItem", dto.Id, dto.Title, dto.Alias, dto.CatId, dto.IntroText, dto.FullText, dto.Status, dto.CreatedBy, dto.ModifiedBy, dto.CreatedDate, dto.ModifiedDate, dto.Metadesc, dto.Metakey, dto.Metadata, dto.Language, dto.Featured, dto.StaticPage, dto.Images,dto.ImagesFull,dto.FileItem, dto.Params, dto.Ordering, dto.Deleted, dto.IdCoQuan, dto.FeaturedHome, NgayDang, Str_ListFile, Str_Link, dto.AuthorId,dto.RootNewsId ,dto.RootNewsFlag, dto.Title_EN, dto.Alias_EN, dto.IntroText_EN, dto.FullText_EN, dto.Metadesc_EN, dto.Metakey_EN, dto.Metadata_EN });
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }
        public static dynamic DeleteItem(Articles dto)
        {
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Articles",
            new string[] { "@flag", "@Id", "@ModifiedBy" },
            new object[] { "DeleteItem", dto.Id, dto.ModifiedBy });
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }

        public static dynamic UpdateStatus(Articles dto)
        {
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Articles",
            new string[] { "@flag", "@Id", "@Status", "@ModifiedBy" },
            new object[] { "UpdateStatus", dto.Id, dto.Status, dto.ModifiedBy });
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }
        public static dynamic UpdateStaticPage(Articles dto)
        {
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Articles",
            new string[] { "@flag", "@Id", "@StaticPage", "@ModifiedBy" },
            new object[] { "UpdateStaticPage", dto.Id, dto.StaticPage, dto.ModifiedBy });
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }
        public static dynamic UpdateFeatured(Articles dto)
        {
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Articles",
            new string[] { "@flag", "@Id", "@Featured", "@ModifiedBy" },
            new object[] { "UpdateFeatured", dto.Id, dto.Featured, dto.ModifiedBy });
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }

        public static dynamic UpdateFeaturedHome(Articles dto)
        {
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Articles",
            new string[] { "@flag", "@Id", "@FeaturedHome", "@ModifiedBy" },
            new object[] { "UpdateFeaturedHome", dto.Id, dto.FeaturedHome, dto.ModifiedBy });
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }

        public static dynamic UpdateAlias(int Id, string Alias)
        {
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Articles",
            new string[] { "@flag", "@Id", "@Alias" },
            new object[] { "UpdateAlias", Id, Alias });
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }

        public static DataTable InsertArticlesTransfer(int Id, int IdCoQuan)
        {
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Articles",
            new string[] { "@flag", "@Id", "@IdCoQuan" },
            new object[] { "InsertArticlesTransfer", Id, IdCoQuan });
            return tabl;

        }

        public static DataTable UpdateArticlesTransfer(int Id, Articles dto)
        {
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Articles",
            new string[] { "@flag", "@Id", "@FullText", "@IntroText", "@Metadesc", "@Metakey", "@ModifiedBy", "@Str_Link", "@Str_ListFile", "@Images" },
            new object[] { "UpdateArticlesTransfer", Id, dto.FullText, dto.IntroText, dto.Metadesc, dto.Metakey, dto.ModifiedBy, dto.Str_Link, dto.Str_ListFile, dto.Images });
            return tabl;

        }



    }
}
