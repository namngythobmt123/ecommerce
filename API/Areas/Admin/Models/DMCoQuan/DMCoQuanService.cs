﻿using API.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace API.Areas.Admin.Models.DMCoQuan
{
    public class DMCoQuanService
    {
        public static List<DMCoQuan> GetListPagination(SearchDMCoQuan dto, string SecretId)
        {
            if (dto.CurrentPage <= 0)
            {
                dto.CurrentPage = 1;
            }
            if (dto.ItemsPerPage <= 0)
            {
                dto.ItemsPerPage = 10;
            }
            if (dto.Keyword == null)
            {
                dto.Keyword = "";
            }
            var tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "DanhMuc_CoQuan",
                new string[] { "@flag", "@CurrentPage", "@ItemsPerPage", "@Keyword", "@CategoryId", "@ParentId" },
                new object[] { "GetListPagination", dto.CurrentPage, dto.ItemsPerPage, dto.Keyword, dto.CategoryId,dto.ParentId });
            if (tabl == null)
            {
                return new List<DMCoQuan>();
            }
            else
            {
                return (from r in tabl.AsEnumerable()
                        select new DMCoQuan
                        {
                            Id = (int)r["Id"],
                            Title = (string)((r["Title"] == System.DBNull.Value) ? null : r["Title"]),
                            Title1 = (string)((r["Title1"] == System.DBNull.Value) ? null : r["Title1"]),                          
                            Images = (string)((r["Images"] == System.DBNull.Value) ? null : r["Images"]),                          
                            Code = (string)((r["Code"] == System.DBNull.Value) ? null : r["Code"]),
                            FolderUpload = (string)((r["FolderUpload"] == System.DBNull.Value) ? null : r["FolderUpload"]),
                            TemplateName = (string)((r["TemplateName"] == System.DBNull.Value) ? null : r["TemplateName"]),
                            Description = (string)((r["Description"] == System.DBNull.Value) ? null : r["Description"]),
                            Status = (bool)r["Status"],
                            CategoryId = (int)((r["CategoryId"] == System.DBNull.Value) ? 0 : r["CategoryId"]),
                            ParentId = (int)((r["ParentId"] == System.DBNull.Value) ? 0 : r["ParentId"]),
                            Ids = MyModels.Encode((int)r["Id"], SecretId),
                            TotalRows = (int)r["TotalRows"]
                        }).ToList();
            }           
        }

        public static List<DMCoQuan> GetAllList(string SecretId)
        {
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "DanhMuc_CoQuan",
                new string[] { "@flag" }, new object[] { "GetList" });
            return (from r in tabl.AsEnumerable()
                    select new DMCoQuan
                    {
                        Id = (int)r["Id"],
                        Title = (string)((r["Title"] == System.DBNull.Value) ? null : r["Title"]),
                        Status = (bool)r["Status"],
                        Ids = MyModels.Encode((int)r["Id"], SecretId)                        
                    }).ToList();

        }

        public static List<SelectListItem> GetList(int Def = 0)
        {
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "DanhMuc_CoQuan",
                new string[] { "@flag" }, new object[] { "GetList" });
            List<SelectListItem> ListItems = (from r in tabl.AsEnumerable()
                                              select new SelectListItem
                                              {
                                                  Value = (string)((r["Id"] == System.DBNull.Value) ? null : r["Id"].ToString()),
                                                  Text = (string)((r["Title"] == System.DBNull.Value) ? null : r["Title"]),
                                              }).ToList();
            if (Def == 0)
            {
                ListItems.Insert(0, (new SelectListItem { Text = "Chọn Cơ Quan cha", Value = "0" }));
            }
            return ListItems;

        }
        public static List<SelectListItem> GetListByLoaiCoQuan(int CategoryId, int Def = 0,int ParentId = 0)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "DanhMuc_CoQuan",
                new string[] { "@flag", "@Selected", "@CategoryId", "@ParentId" }, new object[] { "GetListByLoaiCoQuan", 1, CategoryId, ParentId });
            List<SelectListItem> ListItems = (from r in tabl.AsEnumerable()
                                              select new SelectListItem
                                              {
                                                  Value = (string)((r["Id"] == System.DBNull.Value) ? null : r["Id"].ToString()),
                                                  Text = (string)((r["Title"] == System.DBNull.Value) ? null : r["Title"]),
                                              }).ToList();
            if (Def==0)
            {
                ListItems.Insert(0, (new SelectListItem { Text = "Chọn Cơ Quan cha", Value = "0" }));
            }                        
            return ListItems;
        }

        public static List<SelectListItem> GetListByParent(int Def = 0,int ParentId = 0)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "DanhMuc_CoQuan",
                new string[] { "@flag",  "@ParentId" }, new object[] { "GetListByParent", ParentId });
            List<SelectListItem> ListItems = (from r in tabl.AsEnumerable()
                                              select new SelectListItem
                                              {
                                                  Value = (string)((r["Id"] == System.DBNull.Value) ? null : r["Id"].ToString()),
                                                  Text = (string)((r["Title"] == System.DBNull.Value) ? null : r["Title"]),
                                              }).ToList();
            if (Def == 0)
            {
                ListItems.Insert(0, (new SelectListItem { Text = "Chọn Cơ Quan cha", Value = "0" }));
            }
            return ListItems;
        }

        public static List<SelectListItem> GetListTemplate()
        {
            List<SelectListItem> ListItems = new List<SelectListItem>(){};
            string dirPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot") + "/Templates";
            DirectoryInfo dirInfo = new DirectoryInfo(dirPath);
            if (Directory.Exists(dirPath))
            {
                DirectoryInfo[] childDirs = dirInfo.GetDirectories();
                int i = 0;
                foreach (DirectoryInfo childDir in childDirs)
                {                    
                    ListItems.Insert(i, (new SelectListItem { Text = childDir.Name.ToString(), Value = childDir.Name.ToString() }));
                    i++;
                }
            }

            return ListItems;
        }

        public static DMCoQuan GetItem(int Id, string SecretId = null)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "DanhMuc_CoQuan",
            new string[] { "@flag", "@Id" }, new object[] { "GetItem", Id });
            return (from r in tabl.AsEnumerable()
                    select new DMCoQuan
                    {
                        Id = (int)r["Id"],
                        Title = (string)r["Title"],
                        Code = (string)((r["Code"] == System.DBNull.Value) ? null : r["Code"]),
                        CompanyName = (string)((r["CompanyName"] == System.DBNull.Value) ? null : r["CompanyName"]),
                        Slogan = (string)((r["Slogan"] == System.DBNull.Value) ? null : r["Slogan"]),
                        Facebook = (string)((r["Facebook"] == System.DBNull.Value) ? null : r["Facebook"]),
                        Twitter = (string)((r["Twitter"] == System.DBNull.Value) ? null : r["Twitter"]),
                        Youtube = (string)((r["Youtube"] == System.DBNull.Value) ? null : r["Youtube"]),
                        FolderUpload = (string)((r["FolderUpload"] == System.DBNull.Value) ? null : r["FolderUpload"]),
                        Description = (object)r["Description"],
                        Status = (bool)r["Status"],
                        CategoryId = (int)r["CategoryId"],
                        ParentId = (int)r["ParentId"],                                              
                        Metadesc = (string)((r["Metadesc"] == System.DBNull.Value) ? null : r["Metadesc"]),
                        Metakey = (string)((r["Metakey"] == System.DBNull.Value) ? null : r["Metakey"]),                        
                        Images = (string)((r["Images"] == System.DBNull.Value) ? null : r["Images"]),
                        TemplateName = (string)((r["TemplateName"] == System.DBNull.Value) ? "" : r["TemplateName"]),
                        Email = (string)((r["Email"] == System.DBNull.Value) ? null : r["Email"]),
                        Address = (string)((r["Address"] == System.DBNull.Value) ? "" : r["Address"]),
                        Telephone = (string)((r["Telephone"] == System.DBNull.Value) ? "" : r["Telephone"]),
                        Fax = (string)((r["Fax"] == System.DBNull.Value) ? "" : r["Fax"]),
                        Icon = (string)((r["Icon"] == System.DBNull.Value) ? "" : r["Icon"]),
                        Ids = MyModels.Encode((int)r["Id"], SecretId),
                    }).FirstOrDefault();
        }

        public static DMCoQuan GetItemLevel(int Id)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "DanhMuc_CoQuan",
            new string[] { "@flag", "@Id" }, new object[] { "GetItemLevel", Id });
            return (from r in tabl.AsEnumerable()
                    select new DMCoQuan
                    {
                        Id = (int)r["Id"],
                        Level = (int)r["Level"],
                        Title = (string)r["Title"],                      
                    }).FirstOrDefault();
        }


        public static DMCoQuan GetItemByCode(string Code)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "DanhMuc_CoQuan",
            new string[] { "@flag", "@Code" }, new object[] { "GetItemByCode", Code.Trim() });
            return (from r in tabl.AsEnumerable()
                    select new DMCoQuan
                    {
                        Id = (int)r["Id"],
                        Title = (string)((r["Title"] == System.DBNull.Value) ? null : r["Title"]),
                        Code = (string)((r["Code"] == System.DBNull.Value) ? null : r["Code"]),
                        CompanyName = (string)((r["CompanyName"] == System.DBNull.Value) ? null : r["CompanyName"]),
                        Slogan = (string)((r["Slogan"] == System.DBNull.Value) ? null : r["Slogan"]),
                        Facebook = (string)((r["Facebook"] == System.DBNull.Value) ? null : r["Facebook"]),
                        Twitter = (string)((r["Twitter"] == System.DBNull.Value) ? null : r["Twitter"]),
                        Youtube = (string)((r["Youtube"] == System.DBNull.Value) ? null : r["Youtube"]),
                        Description = (string)((r["Description"] == System.DBNull.Value) ? null : r["Description"]),
                        Status = (bool)r["Status"],
                        CategoryId = (int)r["CategoryId"],
                        ParentId = (int)r["ParentId"],                        
                        Metadesc = (string)((r["Metadesc"] == System.DBNull.Value) ? null : r["Metadesc"]),
                        FolderUpload = (string)((r["FolderUpload"] == System.DBNull.Value) ? null : r["FolderUpload"]),
                        Metakey = (string)((r["Metakey"] == System.DBNull.Value) ? null : r["Metakey"]),
                        TemplateName = (string)((r["TemplateName"] == System.DBNull.Value) ? "" : r["TemplateName"]),
                        Email = (string)((r["Email"] == System.DBNull.Value) ? null : r["Email"]),
                        Address = (string)((r["Address"] == System.DBNull.Value) ? "" : r["Address"]),
                        Telephone = (string)((r["Telephone"] == System.DBNull.Value) ? "" : r["Telephone"]),
                        Fax = (string)((r["Fax"] == System.DBNull.Value) ? "" : r["Fax"]),
                        Icon = (string)((r["Icon"] == System.DBNull.Value) ? "" : r["Icon"]),
                        Images = (string)((r["Images"] == System.DBNull.Value) ? null : r["Images"]),
                    }).FirstOrDefault();
        }
        public static dynamic SaveItem(DMCoQuan dto)
        {
            if (dto.TemplateName ==null || dto.TemplateName == "") {
                dto.TemplateName = "Default";
            }
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "DanhMuc_CoQuan",
            new string[] { "@flag", "@Title", "@Code", "@FolderUpload", "@Id", "@Description", "@Status", "@TemplateName", "@Icon", "@Address", "@Telephone", "@CreatedBy", "@ModifiedBy", "@CategoryId", "ParentId", "@Metadesc", "@Metakey", "@Fax", "@Images", "@Email", "@CompanyName", "@Slogan", "@Facebook", "@Twitter", "@Youtube" },
            new object[] { "SaveItem", dto.Title, dto.Code,dto.FolderUpload, dto.Id, dto.Description, dto.Status,dto.TemplateName, dto.Icon, dto.Address, dto.Telephone, dto.CreatedBy, dto.ModifiedBy, dto.CategoryId, dto.ParentId, dto.Metadesc, dto.Metadesc, dto.Fax, dto.Images,dto.Email,dto.CompanyName,dto.Slogan,dto.Facebook,dto.Twitter,dto.Youtube });
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }
        public static dynamic DeleteItem(DMCoQuan dto)
        {
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "DanhMuc_CoQuan",
            new string[] { "@flag", "@Id", "@ModifiedBy" },
            new object[] { "DeleteItem", dto.Id, dto.ModifiedBy });
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();
        }
        public static dynamic UpdateStatus(DMCoQuan dto)
        {
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "DanhMuc_CoQuan",
            new string[] { "@flag", "@Id", "@Status", "@ModifiedBy" },
            new object[] { "UpdateStatus", dto.Id, dto.Status, dto.ModifiedBy });
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }
    }
}
