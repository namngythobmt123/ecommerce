using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using API.Areas.Admin.Models.Articles;
using API.Areas.Admin.Models.Contacts;
using API.Models;
using ClosedXML.Report;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;

namespace API.Areas.Admin.Models.Contacts
{
    public class ContactsService
    {
        public static List<Contacts> GetListPagination(SearchContacts dto, string SecretId,Boolean flagSql=false)
        {
			if (dto.CurrentPage <= 0)
            {
                dto.CurrentPage = 1;
            }
            if (dto.ItemsPerPage <= 0)
            {
                dto.ItemsPerPage = 10;
            }
            if (dto.Keyword == null)
            {
                dto.Keyword = "";
            }
            string sql = "GetListPagination";
            if (flagSql)
            {
                sql = "GetListPaginationStatus";
            }
            var tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Contacts",
                new string[] { "@flag", "@CurrentPage", "@ItemsPerPage", "@Keyword" , "@IdLinhVuc" },
                new object[] { sql, dto.CurrentPage, dto.ItemsPerPage, dto.Keyword,dto.IdLinhVuc });
            if (tabl == null)
            {
                return new List<Contacts>();
            }
            else
            {
                return (from r in tabl.AsEnumerable()
					select new Contacts
					{
						Id = (int)r["Id"],
 						Title = (string)r["Title"],
 						Status = (Boolean)r["Status"],
 						Fullname = (string)((r["Fullname"] == System.DBNull.Value) ? null : r["Fullname"]),
 						TenLinhVuc = (string)((r["TenLinhVuc"] == System.DBNull.Value) ? null : r["TenLinhVuc"]),
 						Phone = (string)((r["Phone"] == System.DBNull.Value) ? null : r["Phone"]),
 						Email = (string)((r["Email"] == System.DBNull.Value) ? null : r["Email"]),
 						Introtext = (string)((r["Introtext"] == System.DBNull.Value) ? null : r["Introtext"]),
 						Description = (string)((r["Description"] == System.DBNull.Value) ? null : r["Description"]),
 						Address = (string)((r["Address"] == System.DBNull.Value) ? null : r["Address"]), 						
						Ids = MyModels.Encode((int)r["Id"], SecretId),
						TotalRows = (int)r["TotalRows"],
                        IdCoQuan = (int)r["IdCoQuan"],
					}).ToList();
            }


        }

        public static async Task<string> ExportExcel(SearchContacts dto)
        {
            string CreateDate = DateTime.Now.ToString("dd/MM/yyyy");
            List<Contacts> ListItems = GetListPagination(dto, "dsds");
            dto.ListItems = ListItems;
            string TenFileLuu = "dang-ky-lien-he_" + string.Format("{0:ddMMyyHHmmss}" + ".xlsx", DateTime.Now);
            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "temp");
            string outputFile = System.IO.Path.Combine(path, TenFileLuu);

            string ReportTemplate = System.IO.Path.Combine(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TemplatesReport"), "contacts.xlsx");
            var template = new XLTemplate(ReportTemplate);


            template.AddVariable("ListItems", dto.ListItems);
            template.Generate();
            template.SaveAs(outputFile);
            return TenFileLuu;
        }

        public static List<SelectListItem> GetListPaginationNumber(int IdTinhThanh = 0)
        {


            List<SelectListItem> ListItems = new List<SelectListItem>();

            ListItems.Insert(0, (new SelectListItem { Text = "100", Value = "100" }));
            ListItems.Insert(1, (new SelectListItem { Text = "500", Value = "500" }));
            ListItems.Insert(2, (new SelectListItem { Text = "1000", Value = "1000" }));
            ListItems.Insert(3, (new SelectListItem { Text = "10000", Value = "10000" }));
            return ListItems;

        }

        public static List<Contacts> GetList(Boolean Selected = true)
        {
            
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Contacts",
                new string[] { "@flag", "@Selected" }, new object[] { "GetList", Convert.ToDecimal(Selected) });
            if (tabl == null)
            {
                return new List<Contacts>();
            }
            else
            {
                return (from r in tabl.AsEnumerable()
					select new Contacts
					{
						Id = (int)r["Id"],
 						Title = (string)r["Title"],
 						Status = (Boolean)r["Status"],
 						Fullname = (string)((r["Fullname"] == System.DBNull.Value) ? null : r["Fullname"]),
 						Phone = (string)((r["Phone"] == System.DBNull.Value) ? null : r["Phone"]),
 						Email = (string)((r["Email"] == System.DBNull.Value) ? null : r["Email"]),
 						Address = (string)((r["Address"] == System.DBNull.Value) ? null : r["Address"]),
                        IdCoQuan = (int)r["IdCoQuan"],
                    }).ToList();
            }

        }

        public static Contacts GetItem(decimal Id, string SecretId = null)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Contacts",
            new string[] { "@flag", "@Id" }, new object[] { "GetItem", Id });
            Contacts Item = (from r in tabl.AsEnumerable()
                    select new Contacts
                    {
                        Id = (int)r["Id"],
 						Title = (string)r["Title"],
 						Status = (Boolean)r["Status"],
 						Fullname = (string)((r["Fullname"] == System.DBNull.Value) ? null : r["Fullname"]),
                        Str_ListFile = (string)((r["Str_ListFile"] == System.DBNull.Value) ? null : r["Str_ListFile"]),
                        Phone = (string)((r["Phone"] == System.DBNull.Value) ? null : r["Phone"]),
 						Email = (string)((r["Email"] == System.DBNull.Value) ? null : r["Email"]),
 						Address = (string)((r["Address"] == System.DBNull.Value) ? null : r["Address"]),
 						Introtext = (string)((r["Introtext"] == System.DBNull.Value) ? null : r["Introtext"]),
 						Description = (string)((r["Description"] == System.DBNull.Value) ? null : r["Description"]),
 						Deleted = (Boolean)r["Deleted"],
 						CreatedBy = (int)r["CreatedBy"],
 						CreatedDate = (DateTime)r["CreatedDate"],
 						IdLinhVuc = (int)((r["IdLinhVuc"] == System.DBNull.Value) ? null : r["IdLinhVuc"]),
 						ModifiedBy = (int?)((r["ModifiedBy"] == System.DBNull.Value) ? null : r["ModifiedBy"]),
 						ModifiedDate = (DateTime?)((r["ModifiedDate"] == System.DBNull.Value) ? null : r["ModifiedDate"]),
                        Ids = MyModels.Encode((int)r["Id"], SecretId),
                        IdCoQuan = (int)r["IdCoQuan"],
                    }).FirstOrDefault();

            if (Item != null)
            {
                if (Item.Str_ListFile != null && Item.Str_ListFile != "")
                {
                    Item.ListFile = JsonConvert.DeserializeObject<List<FileArticle>>(Item.Str_ListFile);
                }
                
            }
            return Item;
        }

        public static dynamic SaveItem(Contacts dto)
        {
            string Str_ListFile = null;
            List<FileArticle> ListFileArticle = new List<FileArticle>();
            if (dto.ListFile != null && dto.ListFile.Count() > 0)
            {
                for (int i = 0; i < dto.ListFile.Count(); i++)
                {
                    if (dto.ListFile[i].FilePath != null && dto.ListFile[i].FilePath.Trim() != "")
                    {
                        ListFileArticle.Add(dto.ListFile[i]);
                    }
                }
                if (ListFileArticle != null && ListFileArticle.Count() > 0)
                {
                    Str_ListFile = JsonConvert.SerializeObject(ListFileArticle);
                }

            }
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Contacts",
            new string[] { "@flag","@Id","@Title","@Introtext","@Description", "@Str_ListFile", "@Status","@Fullname","@Phone","@Email","@Address","@CreatedBy","@ModifiedBy", "@IdCoQuan", "@IdLinhVuc" },
            new object[] { "SaveItem",dto.Id,dto.Title,dto.Introtext,dto.Description, Str_ListFile, dto.Status,dto.Fullname,dto.Phone,dto.Email,dto.Address,dto.CreatedBy,dto.ModifiedBy,dto.IdCoQuan,dto.IdLinhVuc});
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }
        public static dynamic DeleteItem(Contacts dto)
        {
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Contacts",
            new string[] { "@flag", "@Id", "@ModifiedBy" },
            new object[] { "DeleteItem", dto.Id, dto.ModifiedBy});
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }

        public static dynamic UpdateStatus(Contacts dto)
        {
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Contacts",
            new string[] { "@flag", "@Id","@Status", "@ModifiedBy" },
            new object[] { "UpdateStatus", dto.Id,dto.Status, dto.ModifiedBy });
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }



    }
}
