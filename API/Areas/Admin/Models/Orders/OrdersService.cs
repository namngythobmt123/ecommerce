﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Areas.Admin.Models.Articles;
using API.Areas.Admin.Models.Orders;
using API.Areas.Admin.Models.Orders;
using API.Models;
using ClosedXML.Report;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace API.Areas.Admin.Models.Orders
{
    public class OrdersService
    {
        public static async Task<string> TaoFileBaoCao(SearchOrders dto, Boolean flag = false, DMCoQuan.DMCoQuan ItemCoQuan = null)
        {
            string ShowCurrentDate = "Đắk Lắk, ngày " + DateTime.Now.ToString("dd") + " tháng " + DateTime.Now.ToString("MM") + " năm " + DateTime.Now.ToString("yyyy");
            string CreateDate = DateTime.Now.ToString("dd/MM/yyyy");
            double Total = 0;
            List<Orders> ListItems = OrdersService.GetListPagination(dto, "Hello");
            if (ListItems != null && ListItems.Count() > 0)
            {
                for (int i = 0; i < ListItems.Count(); i++)
                {
                    Total = Total + ListItems[i].Total;
                }
            }
            string TenFileLuu = "DanhSachDonHang_" + string.Format("{0:ddMMyyHHmmss}" + ".xlsx", DateTime.Now);
            string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "temp");
            string outputFile = System.IO.Path.Combine(path, TenFileLuu);
            string ReportTemplate = System.IO.Path.Combine(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TemplatesReport"), "order.xlsx");
           
            var template = new XLTemplate(ReportTemplate);
            template.AddVariable("ShowStartDate", dto.ShowStartDate);
            template.AddVariable("ShowEndDate", dto.ShowEndDate);
            template.AddVariable("ListItems", ListItems);
            template.AddVariable("ShowCurrentDate", ShowCurrentDate);
            template.AddVariable("Total", Total);            
            template.AddVariable("ShowTotal", API.Models.MyHelper.StringHelper.NumberToTextVN(Decimal.Parse(Total.ToString())));
            template.Generate();
            template.SaveAs(outputFile);
            return TenFileLuu;
        }

        public static List<Orders> GetListPagination(SearchOrders dto, string SecretId)
        {
            if (dto.CurrentPage <= 0)
            {
                dto.CurrentPage = 1;
            }
            if (dto.ItemsPerPage <= 0)
            {
                dto.ItemsPerPage = 10;
            }
            if (dto.Keyword == null)
            {
                dto.Keyword = "";
            }
            var tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Orders",
                new string[] { "@flag", "@CurrentPage", "@ItemsPerPage", "@Keyword","@IdTinhThanh","@IdQuanHuyen","@IdPhuongXa" },
                new object[] { "GetListPagination", dto.CurrentPage, dto.ItemsPerPage, dto.Keyword, dto.IdTinhThanh, dto.IdQuanHuyen, dto.IdPhuongXa });
            if (tabl == null)
            {
                return new List<Orders>();
            }
            else
            {
                return (from r in tabl.AsEnumerable()
                        select new Orders
                        {
                            Id = (int)r["Id"],
                            OrderCode = (string)((r["OrderCode"] == System.DBNull.Value) ? "" : r["OrderCode"]),
                            StatusTitle = (string)((r["StatusTitle"] == System.DBNull.Value) ? "" : r["StatusTitle"]),
                            FullName = (string)r["FullName"],                          
                            Note = (string)((r["Note"] == System.DBNull.Value) ? "" : r["Note"]),
                            Address = (string)((r["Address"] == System.DBNull.Value) ? "" : r["Address"]),
                            Telephone = (string)((r["Telephone"] == System.DBNull.Value) ? "" : r["Telephone"]),
                            TinhThanh = (string)((r["TinhThanh"] == System.DBNull.Value) ? "" : r["TinhThanh"]),
                            QuanHuyen = (string)((r["QuanHuyen"] == System.DBNull.Value) ? "" : r["QuanHuyen"]),
                            PhuongXa = (string)((r["PhuongXa"] == System.DBNull.Value) ? "" : r["PhuongXa"]),
                            //Email = (string)((r["Email"] == System.DBNull.Value) ? "" : r["Email"]),
                            StatusId = (int)((r["StatusId"] == System.DBNull.Value) ? 0 : r["StatusId"]),
                            Total = (double)((r["Total"] == System.DBNull.Value) ? Double.Parse("0") : r["Total"]),
                            Amount = (double)((r["Amount"] == System.DBNull.Value) ? Double.Parse("0") : r["Amount"]),
                            CreatedDate = (DateTime)((r["CreatedDate"] == System.DBNull.Value) ? DateTime.Now : r["CreatedDate"]),
                            ModifiedDate = (DateTime)((r["ModifiedDate"] == System.DBNull.Value) ? DateTime.Now : r["ModifiedDate"]),
                            Ids = MyModels.Encode((int)r["Id"], SecretId),
                            TenNV = (string)((r["TenNV"] == System.DBNull.Value) ? "" : r["TenNV"]),
                            MaNV = (string)((r["MaNV"] == System.DBNull.Value) ? "" : r["MaNV"]),
                            PhoneNV = (string)((r["PhoneNV"] == System.DBNull.Value) ? "" : r["PhoneNV"]),
                            ChietKhau = (int)((r["ChietKhau"] == System.DBNull.Value) ? 0 : r["ChietKhau"]),
                            TotalRows = (int)r["TotalRows"],
                        }).ToList();
            }


        }

        public static Orders GetItemByCode(string OrderCode, string SecretId = null)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Orders",
            new string[] { "@flag", "@OrderCode" }, new object[] { "GetItemByCode", OrderCode });
            Orders Item = (from r in tabl.AsEnumerable()
                           select new Orders
                           {
                               Id = (int)r["Id"],
                               OrderCode = (string)((r["OrderCode"] == System.DBNull.Value) ? "" : r["OrderCode"]),
                               OrderKey = (string)((r["OrderKey"] == System.DBNull.Value) ? "" : r["OrderKey"]),
                               Note = (string)((r["Note"] == System.DBNull.Value) ? "" : r["Note"]),
                               Address = (string)((r["Address"] == System.DBNull.Value) ? "" : r["Address"]),
                               //Email = (string)((r["Email"] == System.DBNull.Value) ? "" : r["Email"]),
                               TinhThanh = (string)((r["TinhThanh"] == System.DBNull.Value) ? "" : r["TinhThanh"]),
                               QuanHuyen = (string)((r["QuanHuyen"] == System.DBNull.Value) ? "" : r["QuanHuyen"]),
                               PhuongXa = (string)((r["PhuongXa"] == System.DBNull.Value) ? "" : r["PhuongXa"]),
                               Telephone = (string)((r["Telephone"] == System.DBNull.Value) ? "" : r["Telephone"]),
                               FullName = (string)((r["FullName"] == System.DBNull.Value) ? "" : r["FullName"]),
                               Total = (double)((r["Total"] == System.DBNull.Value) ? Double.Parse("0") : r["Total"]),
                               Amount = (double)((r["Amount"] == System.DBNull.Value) ? Double.Parse("0") : r["Amount"]),
                               TenNV = (string)((r["TenNV"] == System.DBNull.Value) ? "" : r["TenNV"]),
                               MaNV = (string)((r["MaNV"] == System.DBNull.Value) ? "" : r["MaNV"]),
                               PhoneNV = (string)((r["PhoneNV"] == System.DBNull.Value) ? "" : r["PhoneNV"]),
                               ChietKhau = (int)((r["ChietKhau"] == System.DBNull.Value) ? 0 : r["ChietKhau"]),
                               StatusId = (int)((r["StatusId"] == System.DBNull.Value) ? 0 : r["StatusId"]),
                               TotalQuantity = (int)((r["TotalQuantity"] == System.DBNull.Value) ? 0 : r["TotalQuantity"]),
                               StatusTitle = (string)((r["StatusTitle"] == System.DBNull.Value) ? "" : r["StatusTitle"]),
                               ModifiedByFullName = (string)((r["ModifiedByFullName"] == System.DBNull.Value) ? "" : r["ModifiedByFullName"]),
                               CreatedDate = (DateTime)((r["CreatedDate"] == System.DBNull.Value) ? DateTime.Now : r["CreatedDate"]),
                               ModifiedDate = (DateTime?)((r["ModifiedDate"] == System.DBNull.Value) ? null : r["ModifiedDate"]),
                               Ids = MyModels.Encode((int)r["Id"], SecretId),
                           }).FirstOrDefault();
            if (Item == null)
            {
                return null;
            }
            else
            {
                if (Item.StatusId == 0)
                {
                    Item.StatusTitle = "Vé đang chờ thanh toán";
                }
                return Item;
            }
        }
        public static DataTable GetList(Boolean Selected = true)
        {
            
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Orders",
                new string[] { "@flag", "@Selected" }, new object[] { "GetList", Selected });
            return tabl;

        }      

        public static Orders GetItem(int Id, string SecretId = null)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Orders",
            new string[] { "@flag", "@Id" }, new object[] { "GetItem", Id });
            Orders Item = (from r in tabl.AsEnumerable()
                           select new Orders
                           {
                               Id = (int)r["Id"],
                               OrderCode = (string)((r["OrderCode"] == System.DBNull.Value) ? "" : r["OrderCode"]),
                               OrderKey = (string)((r["OrderKey"] == System.DBNull.Value) ? "" : r["OrderKey"]),
                               Note = (string)((r["Note"] == System.DBNull.Value) ? "" : r["Note"]),
                               Address = (string)((r["Address"] == System.DBNull.Value) ? "" : r["Address"]),
                               //Email = (string)((r["Email"] == System.DBNull.Value) ? "" : r["Email"]),
                               TinhThanh = (string)((r["TinhThanh"] == System.DBNull.Value) ? "" : r["TinhThanh"]),
                               QuanHuyen = (string)((r["QuanHuyen"] == System.DBNull.Value) ? "" : r["QuanHuyen"]),
                               PhuongXa = (string)((r["PhuongXa"] == System.DBNull.Value) ? "" : r["PhuongXa"]),
                               Telephone = (string)((r["Telephone"] == System.DBNull.Value) ? "" : r["Telephone"]),
                               FullName = (string)((r["FullName"] == System.DBNull.Value) ? "" : r["FullName"]),
                               Total = (double)((r["Total"] == System.DBNull.Value) ? Double.Parse("0") : r["Total"]),
                               Amount = (double)((r["Amount"] == System.DBNull.Value) ? Double.Parse("0") : r["Amount"]),
                               StatusId = (int)((r["StatusId"] == System.DBNull.Value) ? 0 : r["StatusId"]),
                               IdTinhThanh = (int)((r["IdTinhThanh"] == System.DBNull.Value) ? 0 : r["IdTinhThanh"]),
                               IdQuanHuyen = (int)((r["IdQuanHuyen"] == System.DBNull.Value) ? 0 : r["IdQuanHuyen"]),
                               IdPhuongXa = (int)((r["IdPhuongXa"] == System.DBNull.Value) ? 0 : r["IdPhuongXa"]),
                               TenNV = (string)((r["TenNV"] == System.DBNull.Value) ? "" : r["TenNV"]),
                               MaNV = (string)((r["MaNV"] == System.DBNull.Value) ? "" : r["MaNV"]),
                               PhoneNV = (string)((r["PhoneNV"] == System.DBNull.Value) ? "" : r["PhoneNV"]),
                               ChietKhau = (int)((r["ChietKhau"] == System.DBNull.Value) ? 0 : r["ChietKhau"]),
                               TotalQuantity = (int)((r["TotalQuantity"] == System.DBNull.Value) ? 0 : r["TotalQuantity"]),                              
                               StatusTitle = (string)((r["StatusTitle"] == System.DBNull.Value) ? "" : r["StatusTitle"]),
                               ModifiedByFullName = (string)((r["ModifiedByFullName"] == System.DBNull.Value) ? "" : r["ModifiedByFullName"]),
                               CreatedDate = (DateTime)((r["CreatedDate"] == System.DBNull.Value) ? DateTime.Now : r["CreatedDate"]),
                               ModifiedDate = (DateTime?)((r["ModifiedDate"] == System.DBNull.Value) ? null : r["ModifiedDate"]),
                               Ids = MyModels.Encode((int)r["Id"], SecretId),
                           }).FirstOrDefault();
            if (Item == null)
            {
                return null;
            }
            else
            {
                if (Item.StatusId == 0)
                {
                    Item.StatusTitle = "Vé đang chờ thanh toán";
                }
                return Item;
            }
        }

        public static dynamic SaveItem(Orders dto, DataTable tbItem)
        {
            if (dto.Id == 0)
            {
                string datetime = DateTime.Now.ToString("yyMMddhhmmss");
                dto.OrderCode = API.Models.MyHelper.StringHelper.GetUniqueKey(3).ToUpper() + datetime;
                dto.OrderKey = Guid.NewGuid().ToString("N");
            }
           
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Orders",
            new string[] { "@flag", "@Id","@IdTinhThanh","@IdQuanHuyen", "@IdPhuongXa","TinhThanh","QuanHuyen","PhuongXa", "@MaNV", "@TenNV","@PhoneNV","@ChietKhau", "@OrderCode", "@OrderKey", "@Title", "@Telephone", "@FullName", "@Address", "@Note", "@Total", "@Amount", "@ShipFee", "@StatusId", "@CreatedBy", "@ModifiedBy",  "@TotalQuantity", "@TBL_OrdersProducts" },
            new object[] { "SaveItem", dto.Id, dto.IdTinhThanh, dto.IdQuanHuyen, dto.IdPhuongXa, dto.TinhThanh, dto.QuanHuyen, dto.PhuongXa ,dto.MaNV, dto.TenNV, dto.PhoneNV, dto.ChietKhau, dto.OrderCode, dto.OrderKey, dto.FullName, dto.Telephone, dto.FullName, dto.Address, dto.Note, dto.Total, dto.Amount, dto.ShipFee, dto.StatusId, dto.CreatedBy, dto.ModifiedBy,  dto.TotalQuantity,  tbItem });
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                        OrderCode = (string)(dto.OrderCode),
                    }).FirstOrDefault();

        }

        public static dynamic UpdateOrder(Orders dto)
        {
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Orders",
            new string[] { "@flag","@IdTinhThanh", "@IdQuanHuyen", "@IdPhuongXa", "@TinhThanh", "@QuanHuyen", "@PhuongXa","@Id", "@Telephone", "@FullName", "@Address", "@Note", "@StatusId", "@CreatedBy", "@ModifiedBy" },
            new object[] { "UpdateOrder", dto.IdTinhThanh, dto.IdQuanHuyen, dto.IdPhuongXa, dto.TinhThanh, dto.QuanHuyen, dto.PhuongXa, dto.Id, dto.Telephone, dto.FullName, dto.Address, dto.Note, dto.StatusId, dto.CreatedBy, dto.ModifiedBy});
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }

        public static dynamic UpdateStatus(Orders dto)
        {
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Orders",
            new string[] { "@flag", "@Id", "@StatusId", "@ModifiedBy" },
            new object[] { "UpdateStatus", dto.Id,dto.StatusId, dto.ModifiedBy });
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }

        public static dynamic DeleteItem(Orders dto)
        {
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Orders",
            new string[] { "@flag", "@Id", "@ModifiedBy" },
            new object[] { "DeleteItem", dto.Id, dto.ModifiedBy});
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }
        public static List<SelectListItem> GetListSelectItemsStatus()
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Orders",
                new string[] { "@flag" }, new object[] { "GetListStatus" });
            List<SelectListItem> ListItems = (from r in tabl.AsEnumerable()
                                              select new SelectListItem
                                              {
                                                  Value = (string)((r["Id"] == System.DBNull.Value) ? null : r["Id"].ToString()),
                                                  Text = (string)((r["Title"] == System.DBNull.Value) ? "" : r["Title"]),
                                              }).ToList();
            ListItems.Insert(0, (new SelectListItem { Text = "--- Chọn Trạng thái ---", Value = "0" }));
            return ListItems;

        }

        //public static async Task<string> TaoFileBaoCao(SearchOrders dto, Boolean flag = false, DMCoQuan.DMCoQuan ItemCoQuan = null)
        //{
        //    string ShowCurrentDate = "Đắk Lắk, ngày " + DateTime.Now.ToString("dd") + " tháng " + DateTime.Now.ToString("MM") + " năm " + DateTime.Now.ToString("yyyy");
        //    string CreateDate = DateTime.Now.ToString("dd/MM/yyyy");
        //    double Total = 0;
        //    List<Orders> ListItems = OrdersService.GetListPagination(dto, "Hello");
        //    if (ListItems != null && ListItems.Count() > 0)
        //    {
        //        for (int i = 0; i < ListItems.Count(); i++)
        //        {
        //            Total = Total + ListItems[i].Money;
        //        }
        //    }
        //    string TenFileLuu = "DanhSachBaiViet_" + string.Format("{0:ddMMyyHHmmss}" + ".xlsx", DateTime.Now);
        //    string path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "temp");
        //    string outputFile = System.IO.Path.Combine(path, TenFileLuu);
        //    string ReportTemplate = System.IO.Path.Combine(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "ReportTemplate"), "DanhSachBaiViet.xlsx");
        //    if (ItemCoQuan.MetadataCV.FlagAuthorText == 1)
        //    {
        //        ReportTemplate = System.IO.Path.Combine(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "ReportTemplate"), "DanhSachBaiVietAuthor.xlsx");
        //    }
        //    if (ItemCoQuan.Id == 7)
        //    {
        //        ReportTemplate = System.IO.Path.Combine(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "ReportTemplate"), "DanhSachBaiVietEakar.xlsx");
        //    }
        //    if (ItemCoQuan.FolderUpload == "vks")
        //    {
        //        ReportTemplate = System.IO.Path.Combine(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "ReportTemplate"), "DanhSachBaiVietVKS.xlsx");
        //    }
        //    var template = new XLTemplate(ReportTemplate);
        //    template.AddVariable("ShowStartDate", dto.ShowStartDate);
        //    template.AddVariable("ShowEndDate", dto.ShowEndDate);
        //    template.AddVariable("ListItems", ListItems);
        //    template.AddVariable("ShowCurrentDate", ShowCurrentDate);
        //    template.AddVariable("Total", Total);
        //    template.AddVariable("CompanyName", ItemCoQuan.CompanyName);
        //    template.AddVariable("ShowTotal", API.Models.MyHelper.StringHelper.NumberToTextVN(Decimal.Parse(Total.ToString())));
        //    template.Generate();
        //    template.SaveAs(outputFile);
        //    return TenFileLuu;
        //}

        public static string ConvertToHtmlChild(List<Products.Products> ListItems, int ChietKhau)
        {
            string styleCol = "style='border: 1px solid black;padding: 15px;text-align: left;'";
            double AmountAll = 0;
            string html = "<table style='border-collapse: collapse;border: 1px solid black;'>";
            html = html + "<thead><tr><th " + styleCol + ">STT</th><th " + styleCol + ">Tên Sản Phẩm</th><th " + styleCol + ">Số Lượng</th><th " + styleCol + ">Giá</th><th " + styleCol + ">Thành Tiền</th></tr></thead>";
            html = html + "<tbody>";
            if (ListItems != null && ListItems.Count() > 0)
            {
                for (int i = 0; i < ListItems.Count; i++)
                {
                    html = html + "<tr>";
                    html = html + "<td " + styleCol + ">" + (i + 1).ToString() + "</td>";
                    html = html + "<td " + styleCol + ">" + ListItems[i].Title + "</td>";
                    html = html + "<td " + styleCol + ">" + ListItems[i].Quantity + "</td>";
                    html = html + "<td " + styleCol + ">" + ListItems[i].Price + "</td>";
                    html = html + "<td " + styleCol + ">" + API.Models.MyHelper.StringHelper.ConvertNumberToDisplay((ListItems[i].Quantity * ListItems[i].Price).ToString()) + "</td>";
                    html = html + "</tr>";

                    AmountAll = AmountAll + ListItems[i].Quantity * ListItems[i].Price;
                }
            }
            html = html + "<tr>";
            html = html + "<td colspan='3'" + styleCol + ">Giảm giá</td>";
            html = html + "<td colspan='2'" + styleCol + "><strong>" + API.Models.MyHelper.StringHelper.ConvertNumberToDisplay((ChietKhau != 0 ? (AmountAll * ChietKhau / 100) : 0).ToString()) + "</strong></td>";
            html = html + "</tr>";
            html = html + "<tr>";
            html = html + "<td colspan='3'" + styleCol + ">Thành Tiền</td>";
            html = html + "<td colspan='2'" + styleCol + "><strong>" + API.Models.MyHelper.StringHelper.ConvertNumberToDisplay((ChietKhau != 0 ? (AmountAll - (AmountAll * ChietKhau / 100)) : AmountAll).ToString()) + "</strong></td>";
            html = html + "</tr>";
            html = html + "</tbody>";


            html = html + "</table>";
            return html;
        }

        public static string ConvertBodyEmail(Orders dto, string Body, string HtmlChild)
        {
            Body = Body.Replace("{{OrderCode}}", dto.OrderCode);
            Body = Body.Replace("{{FullName}}", dto.FullName);
            Body = Body.Replace("{{Telephone}}", dto.Telephone);
            Body = Body.Replace("{{Address}}", dto.Address + ", " + dto.PhuongXa + ", " + dto.QuanHuyen + ", " + dto.TinhThanh);
            //Body = Body.Replace("{{Email}}", dto.Email);
            Body = Body.Replace("{{Note}}", dto.Note);
            Body = Body.Replace("{{TenNV}}", dto.TenNV);
            Body = Body.Replace("{{MaNV}}", dto.MaNV);
            Body = Body.Replace("{{PhoneNV}}", dto.PhoneNV);
            Body = Body.Replace("{{ChietKhau}}", dto.ChietKhau.ToString()); 
            Body = Body.Replace("{{Total}}", dto.Total.ToString());
            Body = Body.Replace("{{TotalQuantity}}", dto.TotalQuantity.ToString());
            Body = Body.Replace("{{ContentProducts}}", HtmlChild);
            Body = Body.Replace("{{CreatedDate}}", API.Models.MyHelper.StringHelper.DayweekVN(DateTime.Now) + ", Ngày " + DateTime.Now.ToString("dd") + ", Tháng " + DateTime.Now.ToString("MM") + " Năm " + DateTime.Now.ToString("yyyy"));
            Body = Body.Replace("{{OrderKey}}", dto.OrderKey);
            return Body;
        }


    }
}
