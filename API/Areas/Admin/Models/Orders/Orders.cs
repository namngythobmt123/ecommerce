﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using API.Areas.Admin.Models.Partial;
using Microsoft.AspNetCore.Mvc.Rendering;
/*https://docs.microsoft.com/en-us/aspnet/core/tutorials/razor-pages/validation?view=aspnetcore-2.2*/
namespace API.Areas.Admin.Models.Orders
{
    public class Orders
    {
        public int Id { get; set; }

        
        public string Title { get; set; }
        [Display(Name = "Tên")]
        [StringLength(60, MinimumLength = 3, ErrorMessage = "Độ dài chuỗi phải lớn hơn {2} Không quá {1} ký tự")]
        [Required(ErrorMessage = "Tên không được để trống")]
        public string FullName { get; set; }
        [Display(Name = "Địa chỉ")]
        [StringLength(500, MinimumLength = 3, ErrorMessage = "Độ dài chuỗi phải lớn hơn {2} Không quá {1} ký tự")]
        [Required(ErrorMessage = "Địa chỉ không được để trống")]
        public string Address { get; set; }

        public string PhuongXa { get; set; }
        [Display(Name = "Phường xã")]
        [Range(1, 100000, ErrorMessage = "Phường xã không được để trống")]
        [Required(ErrorMessage = "Phường xã không được để trống")]
        public int IdPhuongXa { get; set; }
        public string QuanHuyen { get; set; }
        [Display(Name = "Quận huyện")]
        [Range(1,1000, ErrorMessage = "Quận huyện không được để trống")]
        [Required(ErrorMessage = "Quận huyện không được để trống")]
        public int IdQuanHuyen { get; set; }
        public string TinhThanh { get; set; }
        [Display(Name = "Tỉnh thành")]
        [Range(1, 100, ErrorMessage = "Tỉnh thành không được để trống")]
        [Required(ErrorMessage = "Tỉnh thành không được để trống")]
        public int IdTinhThanh { get; set; }
        //[Display(Name = "Email")]
        //[StringLength(60, MinimumLength = 3, ErrorMessage = "Độ dài chuỗi phải lớn hơn {2} Không quá {1} ký tự")]
        //[Required(ErrorMessage = "Email không được để trống")]
        //public string Email { get; set; }
        [Display(Name = "Số điện thoại")]
        [StringLength(10, MinimumLength = 10, ErrorMessage = "Độ dài chuỗi phải lớn hơn {2} Không quá {1} ký tự")]
        [Required(ErrorMessage = "Số điện thoại không được để trống")]
        public string Telephone { get; set; }
        public double Total { get; set; }
        public int StatusId { get; set; }
        public string Description { get; set; }
        public int ChietKhau { get; set; }
        public string MaNV { get; set; }
        public string TenNV { get; set; }
        public string PhoneNV { get; set; }
        public string Note { get; set; }
        public string ModifiedByFullName { get; set; }
        public string OrderCode { get; set; }        
        public string OrderKey { get; set; }        
        public string StatusTitle { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; } = null;
        public string Ids { get; set; }
        public int TotalRows { get; set; } = 0;
        public int CreatedBy { get; set; } = 0;
        public int ModifiedBy { get; set; } = 0;
        public int TotalQuantity { get; set; } = 0;
        public Double Amount { get; set; } = 0;
        public Double ShipFee { get; set; } = 0;

    }

    public class OrdersModel {
        public List<Orders> ListItems { get; set; }
        public SearchOrders SearchData { get; set; }
        public Orders Item { get; set; }
        public PartialPagination Pagination { get; set; }
        public List<API.Areas.Admin.Models.Products.Products> ListCart { get; set; }
        public List<API.Areas.Admin.Models.OrdersProducts.OrdersProducts> ListOrdersProducts { get; set; }
        public List<SelectListItem> ListItemsPhuongXa { get; set; }
        public List<SelectListItem> ListItemsQuanHuyen { get; set; }
        public List<SelectListItem> ListItemsTinhThanh { get; set; }

       
        public List<SelectListItem> ListProducts { get; set; }
        public List<SelectListItem> ListStatus { get; set; }
    }
    public class SearchOrders {
        public int CurrentPage { get; set; }
        public int ItemsPerPage { get; set; }
        public int StatusId { get; set; } = -1;
        public int IdPhuongXa { get; set; }
        public int IdQuanHuyen { get; set; }
        public int IdTinhThanh { get; set; }
        public string Keyword { get; set; }
        public string ShowStartDate { get; set; }
        public string ShowEndDate { get; set; }
    }


}
