﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using API.Areas.Admin.Models.Menus;
using API.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace API.Areas.Admin.Models.Menus
{
    public class MenusService
    {
        public static List<Menus> GetListPagination(SearchMenus dto, string SecretId)
        {
            if (dto.CurrentPage <= 0)
            {
                dto.CurrentPage = 1;
            }
            if (dto.ItemsPerPage <= 0)
            {
                dto.ItemsPerPage = 10;
            }
            if (dto.Keyword == null)
            {
                dto.Keyword = "";
            }
            if (dto.CatId < 0 && dto.CatId>7)
            {
                dto.CatId = 0;
            }
            var tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Menus",
                new string[] { "@flag", "@CurrentPage", "@ItemsPerPage", "@Keyword", "@IdCoQuan", "@CatId" },
                new object[] { "GetListPagination", dto.CurrentPage, dto.ItemsPerPage, dto.Keyword ,dto.IdCoQuan,dto.CatId});
            if (tabl == null)
            {
                return new List<Menus>();
            }
            else
            {
                return (from r in tabl.AsEnumerable()
					select new Menus
					{
						Id = (int)r["Id"],
 						Title = (string)r["Title"],
 						CatId = (int)r["CatId"],
                        Ordering = (int)((r["Ordering"] == System.DBNull.Value) ? 0 : r["Ordering"]),
 						Link = (string)((r["Link"] == System.DBNull.Value) ? null : r["Link"]),
 						Link_EN = (string)((r["Link_EN"] == System.DBNull.Value) ? null : r["Link_EN"]),
 						Title_EN = (string)((r["Title_EN"] == System.DBNull.Value) ? null : r["Title_EN"]), 						
 						ParentId = (int)r["ParentId"], 						
 						Status = (Boolean?)((r["Status"] == System.DBNull.Value) ? null : r["Status"]), 						 					
                        Icon = (string)((r["Icon"] == System.DBNull.Value) ? null : r["Icon"]),
                        Ids = MyModels.Encode((int)r["Id"], SecretId),						
					}).ToList();
            }


        }

        public static List<Menus> GetList(Boolean Selected = true,int IdCoQuan = 1,int CatId=0)
        {
            
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Menus",
                new string[] { "@flag", "@Selected", "@IdCoQuan", "@CatId" }, new object[] { "GetList", Convert.ToDecimal(Selected),IdCoQuan, CatId });
            if (tabl == null)
            {
                return new List<Menus>();
            }
            else
            {
                return (from r in tabl.AsEnumerable()
					select new Menus
					{
                        Id = (int)r["Id"],
                        Title = (string)r["Title"],
                        TitleMenu = (string)r["TitleMenu"],
                        Link_EN = (string)((r["Link_EN"] == System.DBNull.Value) ? null : r["Link_EN"]),
                        Title_EN = (string)((r["Title_EN"] == System.DBNull.Value) ? null : r["Title_EN"]),
                        Link = (string)((r["Link"] == System.DBNull.Value) ? null : r["Link"]),
                        IdCoQuan = (int)((r["IdCoQuan"] == System.DBNull.Value) ? 0 : r["IdCoQuan"]),                        
                        ParentId = (int)r["ParentId"],                      
                        Icon = (string)((r["Icon"] == System.DBNull.Value) ? null : r["Icon"])                        
					}).ToList();
            }

        }
        public static List<Menus> GetListByParrent(int ParentId = 0,int IdCoQuan=1, int CatId=0, string Culture="")
        {
            string sql = "GetListByParrent";
            if (Culture.ToLower() == "en")
            {
                sql = "GetListByParrent_EN";
            }
            List<Menus> menus = new List<Menus>();

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Menus",
                new string[] { "@flag", "@ParentId", "@IdCoQuan", "@CatId" }, new object[] { sql, ParentId, IdCoQuan,CatId });
            if (tabl == null)
            {
                menus = new List<Menus>();
            }
            else
            {
                menus = (from r in tabl.AsEnumerable()
                        select new Menus
                        {
                            Id = (int)r["Id"],
                            Title = (string)r["Title"],
                            Link_EN = (string)((r["Link_EN"] == System.DBNull.Value) ? null : r["Link_EN"]),
                            Title_EN = (string)((r["Title_EN"] == System.DBNull.Value) ? null : r["Title_EN"]),
                            Alias = (string)((r["Alias"] == System.DBNull.Value) ? null : r["Alias"]),
                            CatId = (int)r["CatId"],
                            IdCoQuan = (int)r["IdCoQuan"],
                            StaticId = (int)r["StaticId"],
                            Link = (string)((r["Link"] == System.DBNull.Value) ? null : r["Link"]),
                            ParentId = (int)r["ParentId"],
                            ArticleId = (int?)((r["ArticleId"] == System.DBNull.Value) ? null : r["ArticleId"]),
                            Ordering = (int?)((r["Ordering"] == System.DBNull.Value) ? null : r["Ordering"]),
                            Icon = (string)((r["Icon"] == System.DBNull.Value) ? null : r["Icon"]),
                            ChildCount = (int)((r["ChildCount"] == System.DBNull.Value) ? null : r["ChildCount"]),
                        }).ToList();
            }
            for(int i = 0; i < menus.Count; i++)
            {
                if (menus[i].ChildCount > 0)
                {
                    menus[i].ListMenus = GetListByParrent(menus[i].Id, menus[i].IdCoQuan,CatId, Culture);
                    
                }
                
            }
            return menus;
        }

        public static List<SelectListItem> GetListItemsCat()
        {                      
            List<SelectListItem> ListItems = new List<SelectListItem>();
            ListItems.Insert(0, (new SelectListItem { Text = "Menu chính", Value = "0" }));
            ListItems.Insert(1, (new SelectListItem { Text = "Menu Phải", Value = "1" }));               
            ListItems.Insert(2, (new SelectListItem { Text = "Menu Top trái", Value = "2" }));               
            ListItems.Insert(3, (new SelectListItem { Text = "Menu Top Phải", Value = "3" }));               
            return ListItems;
        }

        public static List<SelectListItem> GetListItems(Boolean Selected = true, int IdCoQuan=1,int CatId=0)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Menus",
                new string[] { "@flag", "@Selected", "@IdCoQuan", "@CatId" }, new object[] { "GetList", Convert.ToDecimal(Selected), IdCoQuan, CatId });
            List<SelectListItem> ListItems = (from r in tabl.AsEnumerable()
                                              select new SelectListItem
                                              {
                                                  Value = (string)((r["Id"] == System.DBNull.Value) ? null : r["Id"].ToString()),
                                                  Text = (string)((r["Title"] == System.DBNull.Value) ? null : r["Title"]),
                                              }).ToList();

            ListItems.Insert(0, (new SelectListItem { Text = "Chọn Menu", Value = "0" }));
            return ListItems;
        }

        public static Menus GetItem(decimal Id, string SecretId = null)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Menus",
            new string[] { "@flag", "@Id" }, new object[] { "GetItem", Id });
            return (from r in tabl.AsEnumerable()
                    select new Menus
                    {
                        Id = (int)r["Id"],
 						Title = (string)r["Title"],
 						CatId = (int)r["CatId"],
 						StaticId = (int)r["StaticId"],
                        Link_EN = (string)((r["Link_EN"] == System.DBNull.Value) ? null : r["Link_EN"]),
                        Title_EN = (string)((r["Title_EN"] == System.DBNull.Value) ? null : r["Title_EN"]),
                        Link = (string)((r["Link"] == System.DBNull.Value) ? null : r["Link"]),
                        IdCoQuan = (int)((r["IdCoQuan"] == System.DBNull.Value) ? 0 : r["IdCoQuan"]),
 						ParentId = (int)r["ParentId"],
 						ModifiedBy = (int?)((r["ModifiedBy"] == System.DBNull.Value) ? null : r["ModifiedBy"]),
 						Status = (Boolean?)((r["Status"] == System.DBNull.Value) ? null : r["Status"]),
 						Deleted = (Boolean?)((r["Deleted"] == System.DBNull.Value) ? null : r["Deleted"]),
 						ModifiedDate = (DateTime?)((r["ModifiedDate"] == System.DBNull.Value) ? null : r["ModifiedDate"]),
 						ArticleId = (int?)((r["ArticleId"] == System.DBNull.Value) ? null : r["ArticleId"]),
 						Ordering = (int?)((r["Ordering"] == System.DBNull.Value) ? null : r["Ordering"]),
                        Icon = (string)((r["Icon"] == System.DBNull.Value) ? null : r["Icon"]),
                        Ids = MyModels.Encode((int)r["Id"], SecretId),
                    }).FirstOrDefault();
        }

        public static dynamic SaveItem(Menus dto)
        {
            
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Menus",
            new string[] { "@flag","@Id","@Title", "@Title_EN", "@Link_EN", "@CatId","@StaticId","@Link","@ParentId","@ModifiedBy","@Status","@Deleted","@ModifiedDate","@ArticleId","@Ordering","@Icon", "@IdCoQuan" },
            new object[] { "SaveItem",dto.Id,dto.Title,dto.Title_EN,dto.Link_EN,dto.CatId,dto.StaticId,dto.Link,dto.ParentId,dto.ModifiedBy,dto.Status,dto.Deleted,dto.ModifiedDate,dto.ArticleId,dto.Ordering,dto.Icon,dto.IdCoQuan });
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }
        public static dynamic DeleteItem(Menus dto)
        {
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Menus",
            new string[] { "@flag", "@Id", "@ModifiedBy" },
            new object[] { "DeleteItem", dto.Id, dto.ModifiedBy});
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }
		
		public static dynamic UpdateStatus(Menus dto)
        {
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Menus",
            new string[] { "@flag", "@Id","@Status", "@ModifiedBy" },
            new object[] { "UpdateStatus", dto.Id,dto.Status, dto.ModifiedBy });
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }


    }
}
