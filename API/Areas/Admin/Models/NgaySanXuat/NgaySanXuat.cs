using System;
using System.Collections.Generic;
using API.Areas.Admin.Models.Partial;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace API.Areas.Admin.Models.NgaySanXuat
{
    public class NgaySanXuat
    {
		public string Ids { get; set; }
        public int TotalRows { get; set; }
        public int Id { get; set; }        
 		public string Title { get; set; }
 		public string Description { get; set; }
 		public Boolean Status { get; set; }
 		public Boolean Deleted { get; set; }
 		public int CreatedBy { get; set; }
 		public DateTime? CreatedDate { get; set; }
 		public int? ModifiedBy { get; set; }
 		public DateTime? ModifiedDate { get; set; }
 		public DateTime NgaySX { get; set; }
        public string NgaySXShow { get; set; } = DateTime.Now.ToString("dd/MM/yyyy");
        public int IdNhaMay { get; set; } = 1;
		public Double CongXuat { get; set; }
		public Double MucNuocHienTai { get; set; }
        public int TinhTrang { get; set; } = 1;
		public string TinhTrangTitle { get; set; }
		public string NhaMayTitle { get; set; }
		public string TinhTrangThietBi { get; set; }
		public Double SanLuong { get; set; }
 		
    }
	
	public class NgaySanXuatModel {
        public List<NgaySanXuat> ListItems { get; set; }                 
        public SearchNgaySanXuat SearchData { get; set; }
        public NgaySanXuat Item { get; set; }
        public PartialPagination Pagination { get; set; }
        public PartialPagination PaginationMobile { get; set; }
        public List<SelectListItem> ListItemsNhaMay { get; set; }
        public List<SelectListItem> ListItemsTinhTrang { get; set; }
    }

    public class SearchNgaySanXuat {
        public int CurrentPage { get; set; }
        public int ItemsPerPage { get; set; }
        public int IdNhaMay { get; set; }
        public string ShowStartDate { get; set; } = "01/01/" + DateTime.Now.ToString("yyyy");
        public string ShowEndDate { get; set; } = DateTime.Now.ToString("dd/MM/yyyy");
        public string Keyword { get; set; }
        
    }
}
