﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using API.Areas.Admin.Models.NgaySanXuat;
using API.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace API.Areas.Admin.Models.NgaySanXuat
{
    public class NgaySanXuatService
    {
        public static List<NgaySanXuat> GetListPagination(SearchNgaySanXuat dto, string SecretId)
        {
			if (dto.CurrentPage <= 0)
            {
                dto.CurrentPage = 1;
            }
            if (dto.ItemsPerPage <= 0)
            {
                dto.ItemsPerPage = 30;
            }
            if (dto.Keyword == null)
            {
                dto.Keyword = "";
            }
            string StartDate = DateTime.ParseExact(dto.ShowStartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyyMMdd");
            string EndDate = DateTime.ParseExact(dto.ShowEndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyyMMdd");

            var tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_NgaySanXuat",
                new string[] { "@flag", "@CurrentPage", "@ItemsPerPage", "@Keyword","@IdNhaMay" },
                new object[] { "GetListPagination", dto.CurrentPage, dto.ItemsPerPage, dto.Keyword ,dto.IdNhaMay});
            if (tabl == null)
            {
                return new List<NgaySanXuat>();
            }
            else
            {
                return (from r in tabl.AsEnumerable()
					select new NgaySanXuat
					{
						Id = (int)r["Id"],
 						Title = (string)((r["Title"] == System.DBNull.Value) ? null : r["Title"]),
 						Description = (string)((r["Description"] == System.DBNull.Value) ? null : r["Description"]),
                        TinhTrangThietBi = (string)((r["TinhTrangThietBi"] == System.DBNull.Value) ? null : r["TinhTrangThietBi"]),
 						Status = (Boolean)((r["Status"] == System.DBNull.Value) ? null : r["Status"]), 						
 						CongXuat = (Double)((r["CongXuat"] == System.DBNull.Value) ? Double.Parse("0") : r["CongXuat"]),
 						SanLuong = (Double)((r["SanLuong"] == System.DBNull.Value) ? Double.Parse("0") : r["SanLuong"]),
 						MucNuocHienTai = (Double)((r["MucNuocHienTai"] == System.DBNull.Value) ? Double.Parse("0") : r["MucNuocHienTai"]),
 						TinhTrang = (int)((r["TinhTrang"] == System.DBNull.Value) ? 0 : r["TinhTrang"]),
 						IdNhaMay = (int)((r["IdNhaMay"] == System.DBNull.Value) ? 0 : r["IdNhaMay"]),
 						NhaMayTitle = (string)((r["NhaMayTitle"] == System.DBNull.Value) ? null : r["NhaMayTitle"]),
                        NgaySX = (DateTime)((r["NgaySX"] == System.DBNull.Value) ? DateTime.Now : r["NgaySX"]),
                        NgaySXShow = (string)((r["NgaySX"] == System.DBNull.Value) ? DateTime.Now.ToString("dd/MM/yyyy") : (string)((DateTime)r["NgaySX"]).ToString("dd/MM/yyyy")),
                        Ids = MyModels.Encode((int)r["Id"], SecretId),
						TotalRows = (int)r["TotalRows"],
					}).ToList();
            }

        }

        public static DataTable GetReportFE()
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_NgaySanXuat",
                new string[] { "@flag"}, new object[] { "GetReportFE" });
            return tabl;

        }
        public static List<NgaySanXuat> GetList(Boolean Selected = true)
        {
            
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_NgaySanXuat",
                new string[] { "@flag", "@Selected" }, new object[] { "GetList", Convert.ToDecimal(Selected) });
            if (tabl == null)
            {
                return new List<NgaySanXuat>();
            }
            else
            {
                return (from r in tabl.AsEnumerable()
					select new NgaySanXuat
					{
						Id = (int)r["Id"],
 						Title = (string)((r["Title"] == System.DBNull.Value) ? null : r["Title"]), 						
 						Status = (Boolean)((r["Status"] == System.DBNull.Value) ? null : r["Status"])						
					}).ToList();
            }

        }

        public static List<SelectListItem> GetListItemsTinhTrang()
        {
            List<SelectListItem> ListItems = new List<SelectListItem>();
            ListItems.Insert(0, (new SelectListItem { Text = "--- Chọn tình trạng ---", Value = "0" }));
            ListItems.Insert(1, (new SelectListItem { Text = "Đang hoạt động tốt ", Value = "1" }));            
            return ListItems;
        }

        public static NgaySanXuat GetItem(decimal Id, string SecretId = null)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_NgaySanXuat",
            new string[] { "@flag", "@Id" }, new object[] { "GetItem", Id });
            return (from r in tabl.AsEnumerable()
                    select new NgaySanXuat
                    {
                        Id = (int)r["Id"],
 						Title = (string)((r["Title"] == System.DBNull.Value) ? null : r["Title"]),
 						Description = (string)((r["Description"] == System.DBNull.Value) ? null : r["Description"]),
                        TinhTrangThietBi = (string)((r["TinhTrangThietBi"] == System.DBNull.Value) ? null : r["TinhTrangThietBi"]),
                        CongXuat = (Double)((r["CongXuat"] == System.DBNull.Value) ? Double.Parse("0") : r["CongXuat"]),
                        SanLuong = (Double)((r["SanLuong"] == System.DBNull.Value) ? Double.Parse("0") : r["SanLuong"]),
                        MucNuocHienTai = (Double)((r["MucNuocHienTai"] == System.DBNull.Value) ? Double.Parse("0") : r["MucNuocHienTai"]),
                        TinhTrang = (int)((r["TinhTrang"] == System.DBNull.Value) ? 0 : r["TinhTrang"]),
                        IdNhaMay = (int)((r["IdNhaMay"] == System.DBNull.Value) ? 0 : r["IdNhaMay"]),
                        NgaySX = (DateTime)((r["NgaySX"] == System.DBNull.Value) ? DateTime.Now : r["NgaySX"]),
                        NgaySXShow = (string)((r["NgaySX"] == System.DBNull.Value) ? DateTime.Now.ToString("dd/MM/yyyy") : (string)((DateTime)r["NgaySX"]).ToString("dd/MM/yyyy")),
                        Status = (Boolean)((r["Status"] == System.DBNull.Value) ? null : r["Status"]),                      
                        Ids = MyModels.Encode((int)r["Id"], SecretId),
                    }).FirstOrDefault();
        }

        public static dynamic SaveItem(NgaySanXuat dto)
        {
            DateTime NgaySX = DateTime.ParseExact(dto.NgaySXShow, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_NgaySanXuat",
            new string[] { "@flag","@Id","@Title","@Description","@Status","@CreatedBy","@ModifiedBy", "@CongXuat" , "@SanLuong" , "@IdNhaMay", "@MucNuocHienTai" , "@NgaySX", "@TinhTrang", "@TinhTrangThietBi" },
            new object[] { "SaveItem",dto.Id,dto.Title,dto.Description,dto.Status,dto.CreatedBy,dto.ModifiedBy,dto.CongXuat,dto.SanLuong,dto.IdNhaMay,dto.MucNuocHienTai, NgaySX ,dto.TinhTrang,dto.TinhTrangThietBi });
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }
        public static dynamic DeleteItem(NgaySanXuat dto)
        {
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_NgaySanXuat",
            new string[] { "@flag", "@Id", "@ModifiedBy" },
            new object[] { "DeleteItem", dto.Id, dto.ModifiedBy});
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }

        public static dynamic UpdateStatus(NgaySanXuat dto)
        {
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_NgaySanXuat",
            new string[] { "@flag", "@Id","@Status", "@ModifiedBy" },
            new object[] { "UpdateStatus", dto.Id,dto.Status, dto.ModifiedBy });
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }


    }
}
