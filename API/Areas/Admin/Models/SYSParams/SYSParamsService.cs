﻿using API.Models;
using API.Areas.Admin.Models.SYSParams;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Http;
using System.Globalization;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace API.Areas.Admin.Models.SYSParams
{
    public class SYSParamsService
    {

        public static List<SYSParams> GetListBySTRId(string IdType, string str_Id)
        {
            var tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_SYS_Params",
               new string[] { "@flag", "@IdType", "@str_Id" },
               new object[] { "GetListBySTRId", IdType, str_Id });
            return (from r in tabl.AsEnumerable()
                    select new SYSParams
                    {
                        Id = (int)r["Id"],
                        Title = (string)((r["Title"] == System.DBNull.Value) ? "" : r["Title"]),
                        Icon = (string)((r["Icon"] == System.DBNull.Value) ? "" : r["Icon"]),
                        Introtext = (string)((r["Introtext"] == System.DBNull.Value) ? "" : r["Introtext"]),
                        Alias = (string)((r["Alias"] == System.DBNull.Value) ? "" : r["Alias"]),
                        Selected = (Boolean)((r["Selected"] == System.DBNull.Value) ? false : r["Selected"]),
                    }).ToList();
        }
        public static SYSConfig GetItemConfigByHome()
        {
            
            SYSConfig Item = SYSParamsService.GetItemConfig(CultureInfo.CurrentCulture.Name.ToLower());
            
            return Item;
        }

        public static SYSConfig GetItemConfig(string Culture="vi")
        {
            SearchSYSParams dto = new SearchSYSParams() { IdType = "IdConfig" };
            List<SYSParams> listCauHinh = SYSParamsService.GetList(dto);
            SYSConfig itemCauHinhHeThong = new SYSConfig();
            Dictionary<string, string> dAttributes = new Dictionary<string, string>();
            try
            {
                if (Culture.ToLower() == "en")
                {
                    for (int k = 0; k < listCauHinh.Count(); k++)
                    {
                        string type = listCauHinh[k].Title;
                        string value = listCauHinh[k].Introtext_EN;
                        dAttributes.Add(type, value);
                    }
                }
                else
                {
                    for (int k = 0; k < listCauHinh.Count(); k++)
                    {
                        string type = listCauHinh[k].Title.Trim();
                        string value = listCauHinh[k].Introtext;
                        dAttributes.Add(type, value);
                    }
                }
                itemCauHinhHeThong.Email = dAttributes["Email"];
                itemCauHinhHeThong.WebsiteName = dAttributes["WebsiteName"];
                itemCauHinhHeThong.CompanyName = dAttributes["CompanyName"];
                itemCauHinhHeThong.Slogan = dAttributes["Slogan"];
                itemCauHinhHeThong.Phone = dAttributes["Phone"];
                itemCauHinhHeThong.Facebook = dAttributes["Facebook"];
                itemCauHinhHeThong.Twitter = dAttributes["Twitter"];
                itemCauHinhHeThong.Footer = dAttributes["Footer"];
                itemCauHinhHeThong.SEODescription = dAttributes["SEODescription"];
                itemCauHinhHeThong.SEOKeyword = dAttributes["SEOKeyword"];
                itemCauHinhHeThong.Contact = dAttributes["Contact"];
                itemCauHinhHeThong.Map = dAttributes["Map"];
                itemCauHinhHeThong.Youtube = dAttributes["Youtube"];
                itemCauHinhHeThong.Address = dAttributes["Address"];
                itemCauHinhHeThong.CountYTST = dAttributes["CountYTST"];
                itemCauHinhHeThong.Phone_2 = dAttributes["Phone_2"];
                itemCauHinhHeThong.FanpageFacebook = dAttributes["FanpageFacebook"];
                itemCauHinhHeThong.ChatFacebook = dAttributes["ChatFacebook"];
                itemCauHinhHeThong.AboutUs = dAttributes["AboutUs"];
                itemCauHinhHeThong.Bank = dAttributes["Bank"];
                itemCauHinhHeThong.LayoutSite = dAttributes["LayoutSite"];
                itemCauHinhHeThong.FlagSurvey = dAttributes["FlagSurvey"];
                itemCauHinhHeThong.FlagLeftMenu = dAttributes["FlagLeftMenu"];
                itemCauHinhHeThong.SiteCode = dAttributes["SiteCode"];
                itemCauHinhHeThong.MST = dAttributes["MST"];
                itemCauHinhHeThong.DataHeader = dAttributes["DataHeader"];
                itemCauHinhHeThong.FlagProductNewHome = dAttributes["FlagProductNewHome"];
                itemCauHinhHeThong.FlagCatHome = dAttributes["FlagCatHome"];

                itemCauHinhHeThong.FlagProductNewHomeSLS = dAttributes["FlagProductNewHomeSLS"];
                itemCauHinhHeThong.FlagCatHomeSLS = dAttributes["FlagCatHomeSLS"];
                itemCauHinhHeThong.FlagNewsletter = dAttributes["FlagNewsletter"];
                itemCauHinhHeThong.Logo = dAttributes["Logo"];
            }
            catch(Exception e) { 
            
            }
            
            return itemCauHinhHeThong;
        }

        public static List<SelectListItem> GetListLayout()
        {
            List<SelectListItem> ListItems = new List<SelectListItem>();
            ListItems.Insert(0, (new SelectListItem { Text = "Giao diện mặc định", Value = "default.css" }));
            ListItems.Insert(1, (new SelectListItem { Text = " Màu Xanh ", Value = "mau-xanh.css" }));
            ListItems.Insert(2, (new SelectListItem { Text = " Màu Đỏ ", Value = "mau-do.css" }));
            return ListItems;

        }

        public static List<SelectListItem> GetListStatus()
        {
            List<SelectListItem> ListItems = new List<SelectListItem>();
            ListItems.Insert(0, (new SelectListItem { Text = "Ẩn", Value = "0" }));
            ListItems.Insert(1, (new SelectListItem { Text = "Hiện", Value = "1" }));            
            return ListItems;

        }

        public static void SaveConfig(SYSConfig dto)
        {
            Dictionary<string, string> dAttributes = new Dictionary<string, string>() {
                { "Email", dto.Email},
                { "WebsiteName",dto.WebsiteName},
                { "CompanyName",dto.CompanyName},
                { "Slogan",dto.Slogan},
                { "Phone",dto.Phone},
                { "Facebook",dto.Facebook},
                { "Youtube",dto.Youtube},
                { "Twitter",dto.Twitter},
                { "SEODescription",dto.SEODescription},
                { "SEOKeyword",dto.SEOKeyword},
                { "Map",dto.Map},
                { "Contact",dto.Contact},
                { "Address",dto.Address},
                { "Footer",dto.Footer},
                { "Phone_2",dto.Phone_2},                
                { "FanpageFacebook",dto.FanpageFacebook},
                { "AboutUs",dto.AboutUs},
                { "ChatFacebook",dto.ChatFacebook},
                { "LayoutSite",dto.LayoutSite},
                { "FlagSurvey",dto.FlagSurvey},
                { "Bank",dto.Bank},
                { "FlagLeftMenu",dto.FlagLeftMenu},
                { "SiteCode",dto.SiteCode},
                { "MST",dto.MST},
                { "DataHeader",dto.DataHeader},
                { "FlagProductNewHome",dto.FlagProductNewHome},
                { "FlagCatHome",dto.FlagCatHome},
                { "FlagProductNewHomeSLS",dto.FlagProductNewHomeSLS},
                { "FlagCatHomeSLS",dto.FlagCatHomeSLS},
                { "FlagNewsletter",dto.FlagNewsletter},
                { "Logo",dto.Logo},
            };


            foreach (var item in dAttributes)
            {
                if (dto.Culture == "en")
                {
                    SYSParams param = new SYSParams() { Title = item.Key, Introtext = item.Value };
                    var tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_SYS_Params",
                        new string[] { "@flag", "@Title", "@Introtext" },
                        new object[] { "SaveConfig_EN", param.Title, param.Introtext });
                }
                else {
                    SYSParams param = new SYSParams() { Title = item.Key, Introtext = item.Value };
                    var tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_SYS_Params",
                        new string[] { "@flag", "@Title", "@Introtext" },
                        new object[] { "SaveConfig", param.Title, param.Introtext });
                }
                
            }
        }

        public static dynamic SaveItemCountYTST(string Introtext)
        {
            return ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_SYS_Params",
                    new string[] { "@flag", "@Title", "@Introtext" },
                    new object[] { "SaveConfig", "CountYTST", Introtext });
        }

        public static SYSParams GetItem(string IdType, int Id)

        {

            var tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_SYS_Params",
                new string[] { "@flag", "@IdType", "@Id" },
                new object[] { "GetItem", IdType, Id });
            return (from r in tabl.AsEnumerable()
                    select new SYSParams
                    {
                        Id = (int)r["Id"],
                        Title = (string)((r["Title"] == System.DBNull.Value) ? "" : r["Title"]),
                        Icon = (string)((r["Icon"] == System.DBNull.Value) ? "" : r["Icon"]),
                        Introtext = (string)((r["Introtext"] == System.DBNull.Value) ? "" : r["Introtext"]),
                        Alias = (string)((r["Alias"] == System.DBNull.Value) ? "" : r["Alias"]),
                    }).FirstOrDefault();
        }

        public static SYSParams GetItemByAlias(SearchSYSParams dto)
        {

            var tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_SYS_Params",
                new string[] { "@flag", "@IdType", "@Alias" },
                new object[] { "GetItemByAlias", dto.IdType, dto.Alias });
            return (from r in tabl.AsEnumerable()
                    select new SYSParams
                    {
                        Id = (int)r["Id"],
                        Title = (string)((r["Title"] == System.DBNull.Value) ? "" : r["Title"]),
                        Icon = (string)((r["Icon"] == System.DBNull.Value) ? "" : r["Icon"]),
                        Introtext = (string)((r["Introtext"] == System.DBNull.Value) ? "" : r["Introtext"]),
                        Alias = (string)((r["Alias"] == System.DBNull.Value) ? "" : r["Alias"]),
                    }).FirstOrDefault();
        }

        public static List<SYSParams> GetListByHome(SearchSYSParams dto)
        {
            int status = 0;
            if (dto.Selected)
            {
                status = 1;
            }
            var tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_SYS_Params",
                new string[] { "@flag", "@IdType", "@Selected" },
                new object[] { "GetList", dto.IdType, status });
            return (from r in tabl.AsEnumerable()
                    select new SYSParams
                    {
                        Id = (int)r["Id"],
                        Title = (string)((r["Title"] == System.DBNull.Value) ? "" : r["Title"]),
                        Icon = (string)((r["Icon"] == System.DBNull.Value) ? "" : r["Icon"]),
                        Introtext = (string)((r["Introtext"] == System.DBNull.Value) ? "" : r["Introtext"]),
                        Alias = (string)((r["Alias"] == System.DBNull.Value) ? "" : r["Alias"])
                    }).ToList();
        }


        public static List<SYSParams> GetList(SearchSYSParams dto)
        {
            int status = 0;
            if (dto.Selected)
            {
                status = 1;
            }
            var tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_SYS_Params",
                new string[] { "@flag", "@IdType", "@Selected" },
                new object[] { "GetList", dto.IdType, status });
            return (from r in tabl.AsEnumerable()
                    select new SYSParams
                    {
                        Id = (int)r["Id"],
                        Title = (string)((r["Title"] == System.DBNull.Value) ? "" : r["Title"]),
                        Icon = (string)((r["Icon"] == System.DBNull.Value) ? "" : r["Icon"]),
                        Introtext = (string)((r["Introtext"] == System.DBNull.Value) ? "" : r["Introtext"]),
                        Introtext_EN = (string)((r["Introtext_EN"] == System.DBNull.Value) ? "" : r["Introtext_EN"]),
                        Alias = (string)((r["Alias"] == System.DBNull.Value) ? "" : r["Alias"])
                    }).ToList();
        }

        public static List<SYSParams> GetListDestinationCat(SearchSYSParams dto)
        {
            int status = 0;
            if (dto.Selected)
            {
                status = 1;
            }
            var tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_SYS_Params",
                new string[] { "@flag", "@IdType", "@Selected" },
                new object[] { "GetList", dto.IdType, status });
            return (from r in tabl.AsEnumerable()
                    select new SYSParams
                    {
                        Id = (int)r["Id"],
                        Title = (string)((r["Title"] == System.DBNull.Value) ? "" : r["Title"]),
                        Icon = (string)((r["Icon"] == System.DBNull.Value) ? "" : r["Icon"]),
                        Introtext = (string)((r["Introtext"] == System.DBNull.Value) ? "" : r["Introtext"]),
                        Alias = (string)((r["Alias"] == System.DBNull.Value) ? "" : r["Alias"])
                    }).ToList();
        }

    }
}