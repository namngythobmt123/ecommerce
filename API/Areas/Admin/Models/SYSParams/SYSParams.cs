﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Areas.Admin.Models.SYSParams
{
    public class SYSParams
    {
        public string IdType { get; set; }
        public int Id { get; set; }
        public string Title { get; set; }
        public string Introtext { get; set; }
        public string Introtext_EN { get; set; }
        public string Icon { get; set; }
        public string Alias { get; set; }
        public int Ordering { get; set; }
        public int CountItem { get; set; }

        public Boolean Selected;
    }
    public class SearchSYSParams {
        public Boolean Selected { get; set; }
        public string IdType { get; set; }
        public string Alias { get; set; }
    }

    public class SYSConfigModel
    {
        public SYSConfig Item { get; set; }
        public List<SelectListItem> ListItemsLayout { get; set; }
        public List<SelectListItem> ListStatus { get; set; }
    }

    public class SYSConfig
    {
        public string WebsiteName { get; set; }
        public string CompanyName { get; set; }
        public string Slogan { get; set; }
        public string Address { get; set; }        
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Facebook { get; set; }
        
        public string Twitter { get; set; }
        public string Footer { get; set; }
        public string SEODescription { get; set; }
        public string SEOKeyword { get; set; }
        public string Contact { get; set; }
        public string AboutUs { get; set; }
        public string Map { get; set; }
        public string Youtube { get; set; }       
        public string CountYTST { get; set; }       
        public string Phone_2 { get; set; }        
        public string FanpageFacebook { get; set; }
        public string ChatFacebook { get; set; }
        public string Bank { get; set; }
        public string Culture { get; set; } = "vi";
        public string LayoutSite { get; set; }
        public string FlagSurvey { get; set; }
        public string FlagLeftMenu { get; set; }
        public string SiteCode { get; set; }
        public string MST { get; set; }
        public string DataHeader { get; set; }
        public string FlagProductNewHomeSLS { get; set; }
        public string FlagProductNewHome { get; set; }
        public string FlagCatHome { get; set; }
        public string FlagCatHomeSLS { get; set; }
        public string FlagNewsletter { get; set; }
        public string Logo { get; set; }
        public string Keyword { get; set; }       

    }
}