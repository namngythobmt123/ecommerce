﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using API.Areas.Admin.Models.DocumentsCategories;
using API.Areas.Admin.Models.Partial;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace API.Areas.Admin.Models.Employees
{
    public class Employees
    {
        public string Ids { get; set; }
        public int TotalRows { get; set; }
        public int Id { get; set; }
        public int IdCoQuan { get; set; }
        [Display(Name = "Tên")]
        [StringLength(500, MinimumLength = 3, ErrorMessage = "Độ dài tiêu đề chuỗi phải lớn hơn {2} Không quá {1} ký tự")]
        [Required(ErrorMessage = "Tên không được để trống")]
        public string Fullname { get; set; }
        [Display(Name = "Mã nhân viên")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Độ dài tiêu đề chuỗi phải lớn hơn {2} Không quá {1} ký tự")]
        [Required(ErrorMessage = "Mã nhân viên không được để trống")]
        public string MaNV { get; set; }
        //public string Alias { get; set; }
        public string Phone {  get; set; }
        public string Address { get; set; }
        public int ChietKhau { get; set; }
        //public string Description { get; set; }
        public Boolean Status { get; set; }
        public Boolean Deleted { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }

    public class EmployeesModel
    {
        public List<Employees> ListItems { get; set; }
        public SearchEmployees SearchData { get; set; }
        public Employees Item { get; set; }
        public PartialPagination Pagination { get; set; }
        public List<SelectListItem> ListDMCoQuan { get; set; }
    }

    public class SearchEmployees
    {
        public int CurrentPage { get; set; }
        public int ItemsPerPage { get; set; }
        public string Keyword { get; set; }
        public int IdCoQuan { get; set; }
    }
}