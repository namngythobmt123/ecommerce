﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using API.Areas.Admin.Models.Employees;
using API.Models;
using Microsoft.AspNetCore.DataProtection;

namespace API.Areas.Admin.Models.Employees
{
    public class EmployeesService
    {
        public static List<Employees> GetListPagination(SearchEmployees dto, string SecretId)
        {
            if (dto.CurrentPage <= 0)
            {
                dto.CurrentPage = 1;
            }
            if (dto.ItemsPerPage <= 0)
            {
                dto.ItemsPerPage = 10;
            }
            if (dto.Keyword == null)
            {
                dto.Keyword = "";
            }
            var tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Employees",
                new string[] { "@flag", "@CurrentPage", "@ItemsPerPage", "@Keyword" },
                new object[] { "GetListPagination", dto.CurrentPage, dto.ItemsPerPage, dto.Keyword });
            if (tabl == null)
            {
                return new List<Employees>();
            }
            else
            {
                return (from r in tabl.AsEnumerable()
                        select new Employees
                        {
                            Id = (int)r["Id"],
                            Fullname = (string)r["Fullname"],
                            Address = (string)r["Address"],
                            Phone  = (string)r["Phone"],
                            ChietKhau = (int)r["ChietKhau"],
                            Status = (Boolean)r["Status"],
                            MaNV = (string)r["MaNV"],
                            Ids = MyModels.Encode((int)r["Id"], SecretId),
                            TotalRows = (int)r["TotalRows"],
                        }
                        ).ToList();
            }
        }

        public static List<Employees> GetList(Boolean Selected = true)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Employees",
                new string[] { "@flag", "@Selected" }, new object[] { "GetList", Convert.ToDecimal(Selected) });
            if (tabl == null)
            {
                return new List<Employees>();
            }
            else
            {
                return (from r in tabl.AsEnumerable()
                        select new Employees
                        {
                            Id = (int)r["Id"],
                            Fullname = (string)r["Fullname"],
                            Address = (string)r["Address"],
                            Phone = (string)r["Phone"],
                            ChietKhau = (int)r["ChietKhau"],
                            Status = (Boolean)r["Status"],
                            MaNV = (string)r["MaNV"],
                            TotalRows = (int)r["TotalRows"],
                        }).ToList();
            }

        }

        public static Employees GetItem(decimal Id, string SecretId = null)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Employees",
            new string[] { "@flag", "@Id" }, new object[] { "GetItem", Id });
            return (from r in tabl.AsEnumerable()
                    select new Employees
                    {
                        Id = (int)r["Id"],
                        Fullname = (string)r["Fullname"],
                        Address = (string)r["Address"],
                        Phone = (string)r["Phone"],
                        ChietKhau = (int)r["ChietKhau"],
                        Status = (Boolean)r["Status"],
                        MaNV = (string)r["MaNV"],
                        Ids = MyModels.Encode((int)r["Id"], SecretId),
                    }).FirstOrDefault();
        }

        public static Employees GetItem(string MaNV, string SecretId = null)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Employees",
            new string[] { "@flag", "@MaNV" }, new object[] { "GetItemByMaNV", MaNV });
            return (from r in tabl.AsEnumerable()
                    select new Employees
                    {
                        Id = (int)r["Id"],
                        Fullname = (string)r["Fullname"],
                        Address = (string)r["Address"],
                        Phone = (string)r["Phone"],
                        ChietKhau = (int)r["ChietKhau"],
                        Status = (Boolean)r["Status"],
                        MaNV = (string)r["MaNV"],
                        Ids = MyModels.Encode((int)r["Id"], SecretId),
                    }).FirstOrDefault();
        }

        public static dynamic SaveItem(Employees dto)
        {

            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Employees",
            new string[] { "@flag", "@Id", "@Fullname", "@Phone", "@Address", "@ChietKhau", "@MaNV","@Status", "@CreatedBy", "@ModifiedBy", "@Deleted", "@IdCoQuan" },
            new object[] { "SaveItem", dto.Id, dto.Fullname, dto.Phone, dto.Address, dto.ChietKhau, dto.MaNV,dto.Status, dto.CreatedBy, dto.ModifiedBy, dto.Deleted, dto.IdCoQuan });
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }

        public static dynamic DeleteItem(Employees dto)
        {
            DataTable tabl = ConnectDb.ExecuteDataTableTask(Startup.ConnectionString, "SP_Employees",
            new string[] { "@flag", "@Id", "@ModifiedBy" },
            new object[] { "DeleteItem", dto.Id, dto.ModifiedBy });
            return (from r in tabl.AsEnumerable()
                    select new
                    {
                        N = (int)(r["N"]),
                    }).FirstOrDefault();

        }

    }
}
