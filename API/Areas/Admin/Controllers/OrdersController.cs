﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using API.Areas.Admin.Models.Orders;
using API.Models;
using Newtonsoft.Json;
using System.Data;
using API.Areas.Admin.Models.OrdersProducts;
using API.Areas.Admin.Models.DMPhuongXa;
using API.Areas.Admin.Models.DMQuanHuyen;
using API.Areas.Admin.Models.DMTinhThanh;
using API.Areas.Admin.Models.Articles;
using API.Areas.Admin.Models.DMCoQuan;

namespace API.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class OrdersController : Controller
    {
        [HttpPost]
        public async Task<dynamic> TaoFileBaoCao([FromBody] SearchOrders dto)
        {
            try
            {
                dto.ItemsPerPage = 1000;
                OrdersModel data = new OrdersModel() { SearchData = dto };
                return new API.Models.MsgSuccess() { Data = await OrdersService.TaoFileBaoCao(data.SearchData, false) };
            }
            catch (Exception e)
            {
                return new API.Models.MsgError() { Msg = e.Message };
            }
        }

        [HttpPost]
        public IActionResult GetListItems([FromBody] SearchOrders dto)
        {
            int TotalItems = 0;
            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            OrdersModel data = new OrdersModel() { SearchData = dto };

            data.ListItems = OrdersService.GetListPagination(data.SearchData, API.Models.Settings.SecretId + ControllerName);


            if (data.ListItems != null && data.ListItems.Count() > 0)
            {
                TotalItems = data.ListItems[0].TotalRows;
            }
            data.Pagination = new Models.Partial.PartialPagination() { CurrentPage = data.SearchData.CurrentPage, ItemsPerPage = data.SearchData.ItemsPerPage, TotalItems = TotalItems, QueryString = Request.QueryString.ToString() };

            return Json(new MsgSuccess() { Data = data });
        }

        [HttpPost]
        public IActionResult GetListOrdersProducts([FromBody] SearchOrdersProducts dto)
        {
            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            dto.OrderId = Int32.Parse(MyModels.Decode(dto.Ids, API.Models.Settings.SecretId + ControllerName).ToString());
            List<OrdersProducts> ListItems = new List<OrdersProducts>();
            if (dto.OrderId > 0)
            {
                ListItems = OrdersProductsService.GetListPagination(dto, API.Models.Settings.SecretId + ControllerName);
            }


            return Json(new MsgSuccess() { Data = ListItems });
        }
        public IActionResult Index([FromQuery] SearchOrders dto)
            {
            int TotalItems = 0;
            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            OrdersModel data = new OrdersModel() { SearchData = dto};

            data.ListItemsTinhThanh = DMTinhThanhService.GetListSelectItems();
            //data.ListItemsQuanHuyen = DMQuanHuyenService.GetListSelectItems(data.SearchData.IdTinhThanh);
            //data.ListItemsPhuongXa = DMPhuongXaService.GetListSelectItems(data.SearchData.IdQuanHuyen);

            data.ListItems = OrdersService.GetListPagination(data.SearchData, API.Models.Settings.SecretId + ControllerName);            
            if (data.ListItems != null && data.ListItems.Count() > 0)
            {
                TotalItems = data.ListItems[0].TotalRows;
            }
            data.Pagination = new Models.Partial.PartialPagination() { CurrentPage = data.SearchData.CurrentPage, ItemsPerPage = data.SearchData.ItemsPerPage, TotalItems = TotalItems, QueryString = Request.QueryString.ToString() };

            return View(data);
        }

        public IActionResult SaveItem(string Id = null)
        {
            OrdersModel data = new OrdersModel();
            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            int IdDC = Int32.Parse(MyModels.Decode(Id, API.Models.Settings.SecretId + ControllerName).ToString());
            data.SearchData = new SearchOrders() { CurrentPage = 0, ItemsPerPage = 10, Keyword = "" };


            if (IdDC == 0)
            {
                data.Item = new Orders();
            }
            else
            {
                data.Item = OrdersService.GetItem(IdDC, API.Models.Settings.SecretId + ControllerName);                
                data.ListStatus = OrdersService.GetListSelectItemsStatus();
                data.ListItemsTinhThanh = DMTinhThanhService.GetListSelectItems();
                data.ListItemsQuanHuyen = DMQuanHuyenService.GetListSelectItems(data.Item.IdTinhThanh);
                data.ListItemsPhuongXa = DMPhuongXaService.GetListSelectItems(data.Item.IdQuanHuyen);
            }


            return View(data);
        }

        [HttpPost]
        //public async Task<dynamic> TaoFileBaoCao([FromBody] SearchOrders dto)
        //{
        //    try
        //    {
        //        dto.ItemsPerPage = 1000;
        //        string ThongTinCoQuan = HttpContext.Session.GetString("ThongTinCoQuan");
        //        DMCoQuan ItemCoQuan = JsonConvert.DeserializeObject<DMCoQuan>(ThongTinCoQuan);
        //        dto.IdCoQuan = int.Parse(HttpContext.Request.Headers["IdCoQuan"]);
        //        OrdersModel data = new OrdersModel() { SearchData = dto };
        //        return new API.Models.MsgSuccess() { Data = await OrdersService.TaoFileBaoCao(data.SearchData, false, ItemCoQuan) };
        //    }
        //    catch (Exception e)
        //    {
        //        return new API.Models.MsgError() { Msg = e.Message };
        //    }
        //}


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveItem(Orders model)
        {
            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            int IdDC = Int32.Parse(MyModels.Decode(model.Ids, API.Models.Settings.SecretId + ControllerName).ToString());
            OrdersModel data = new OrdersModel() { Item = model };
            if (ModelState.IsValid)
            {
                if (model.Id == IdDC)
                {
                    model.CreatedBy = model.ModifiedBy = int.Parse(HttpContext.Request.Headers["Id"]);
                    DMPhuongXa CheckPhuongXa = new DMPhuongXa();
                    DMQuanHuyen CheckQuanHuyen = new DMQuanHuyen();
                    DMTinhThanh CheckTinhThanh = new DMTinhThanh();

                    CheckPhuongXa = DMPhuongXaService.GetItem(data.Item.IdPhuongXa);
                    CheckQuanHuyen = DMQuanHuyenService.GetItem(data.Item.IdQuanHuyen);
                    CheckTinhThanh = DMTinhThanhService.GetItem(data.Item.IdTinhThanh);

                    model.TinhThanh = CheckTinhThanh.Ten;
                    model.QuanHuyen = CheckQuanHuyen.Ten;
                    model.PhuongXa = CheckPhuongXa.Ten;

                    dynamic DataSave = OrdersService.UpdateOrder(model);
                    if (model.Id > 0)
                    {
                        TempData["MessageSuccess"] = "Cập nhật thành công";
                    }
                    else
                    {
                        TempData["MessageSuccess"] = "Thêm mới thành công";
                    }
                    return RedirectToAction("Index");
                }
            }
            else
            {
                data.ListStatus = OrdersService.GetListSelectItemsStatus();
                //data.ListOrderProducts = OrdersProductsService.GetList(IdDC, API.Models.Settings.SecretId + ControllerName);
            }

            return View(data);
        }

        [ValidateAntiForgeryToken]
        public ActionResult DeleteItem(string Id)
        {
            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            int IdDC = Int32.Parse(MyModels.Decode(Id, API.Models.Settings.SecretId + ControllerName).ToString());

            try
            {
                if (IdDC > 0)
                {
                    Orders item = OrdersService.GetItem(IdDC);
                    if (item.StatusId != 2)
                    {
                        item.CreatedBy = item.ModifiedBy = int.Parse(HttpContext.Request.Headers["Id"]);
                        dynamic DataDelete = OrdersService.DeleteItem(item);
                        return Json(new MsgSuccess() { Msg = "Xóa thành công" });
                    }
                    else
                    {
                        return Json(new MsgError() { Msg = "Xóa Không thành công" });
                    }

                }
                else
                {
                    return Json(new MsgError() { Msg = "Xóa Không thành công" });
                }

            }
            catch
            {

                return Json(new MsgError() { Msg = "Xóa không thành công" });
            }
        }


    }
}