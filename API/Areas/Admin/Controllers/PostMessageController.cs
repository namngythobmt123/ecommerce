using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using API.Areas.Admin.Models.PostMessage;
using API.Models;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Configuration;

namespace API.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class PostMessageController : Controller
    {
        private IConfiguration Configuration;
        public PostMessageController(IConfiguration config)
        {
            Configuration = config;
        }
        public IActionResult Index([FromQuery] SearchPostMessage dto)
        {
            int TotalItems = 0;
            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            PostMessageModel data = new PostMessageModel() { SearchData = dto};
            data.ListItems = PostMessageService.GetListPagination(data.SearchData, Configuration["Security:SecretId"] + ControllerName);            
            if (data.ListItems != null && data.ListItems.Count() > 0)
            {
                TotalItems = data.ListItems[0].TotalRows;
            }
            data.Pagination = new Models.Partial.PartialPagination() { CurrentPage = data.SearchData.CurrentPage, ItemsPerPage = data.SearchData.ItemsPerPage, TotalItems = TotalItems, QueryString = Request.QueryString.ToString() };

            return View(data);
        }
        [HttpGet]
        public async Task<IActionResult> DownloadFile(string Id = null)
        {
            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            int IdDC = Int32.Parse(MyModels.Decode(Id, Configuration["Security:SecretId"] + ControllerName).ToString());
            
            if (IdDC == 0)
            {
                return Json(new API.Models.MsgError() { Msg = "File Không tồn tại" });
            }
            else
            {
                PostMessage Item =  PostMessageService.GetItem(IdDC, Configuration["Security:SecretId"] + ControllerName);
                if (Item != null && Item.LinkFile != null && Item.LinkFile.Trim() != "")
                {
                    

                    var localFilePath = Path.Combine(Directory.GetCurrentDirectory(), "Upload_files/") + Item.LinkFile;

                    if (!System.IO.File.Exists(localFilePath))
                    {
                        return Json(new API.Models.MsgError() { Msg = "File Không tồn tại" });
                    }
                    else
                    {
                        var memory = new MemoryStream();
                        using (var stream = new FileStream(localFilePath, FileMode.Open))
                        {
                            await stream.CopyToAsync(memory);
                        }
                        memory.Position = 0;

                        return File(memory, GetContentType(localFilePath), Item.LinkFile);
                    }
                }
                else {
                    return Json(new API.Models.MsgError() { Msg = "File Không tồn tại" });
                }
                
            }

            
            
        }

        private string GetContentType(string path)
        {
            var provider = new FileExtensionContentTypeProvider();
            string contentType;
            if (!provider.TryGetContentType(path, out contentType))
            {
                contentType = "application/octet-stream";
            }
            return contentType;
        }

        public IActionResult SaveItem(string Id=null)
        {
            PostMessageModel data = new PostMessageModel();
            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            int IdDC = Int32.Parse(MyModels.Decode(Id, Configuration["Security:SecretId"] + ControllerName).ToString());            
            data.SearchData = new SearchPostMessage() { CurrentPage = 0, ItemsPerPage = 10, Keyword = ""};            
            if (IdDC == 0)
            {
                data.Item = new PostMessage();
            }
            else {
                data.Item = PostMessageService.GetItem(IdDC, Configuration["Security:SecretId"] + ControllerName);
            }
            
           
            return View(data);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveItem(PostMessage model)
        {
            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            int IdDC = Int32.Parse(MyModels.Decode(model.Ids, Configuration["Security:SecretId"] + ControllerName).ToString());
            PostMessageModel data = new PostMessageModel() { Item = model};            
            if (ModelState.IsValid)
            {
                if (model.Id == IdDC)
                {
                    model.CreatedBy = int.Parse(HttpContext.Request.Headers["Id"]);
                    model.ModifiedBy = int.Parse(HttpContext.Request.Headers["Id"]);
                    PostMessageService.SaveItem(model);
                    if (model.Id > 0)
                    {
                        TempData["MessageSuccess"] = "Cập nhật thành công";
                    }
                    else
                    {
                        TempData["MessageSuccess"] = "Thêm mới thành công";
                    }
                    return RedirectToAction("Index");
                }
            }
            return View(data);
        }
        
        [ValidateAntiForgeryToken]
        public ActionResult DeleteItem(string Id)
        {
            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            PostMessage model = new PostMessage() { Id = Int32.Parse(MyModels.Decode(Id, Configuration["Security:SecretId"] + ControllerName).ToString()) };            
            try
            {
                if (model.Id > 0)
                {
                    model.CreatedBy = int.Parse(HttpContext.Request.Headers["Id"]);
                    model.ModifiedBy = int.Parse(HttpContext.Request.Headers["Id"]);
                    PostMessageService.DeleteItem(model);
                    TempData["MessageSuccess"] = "Xóa thành công";
                    return Json(new MsgSuccess());
                }
                else {
                    TempData["MessageError"] = "Xóa Không thành công";
                    return Json(new MsgError());
                }
                
            }
            catch {
                TempData["MessageSuccess"] = "Xóa không thành công";
                return Json(new MsgError());
            }
            

        }

        [ValidateAntiForgeryToken]
        public ActionResult UpdateStatus([FromQuery] string Ids, Boolean Status)
        {
            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            PostMessage item = new PostMessage() { Id = Int32.Parse(MyModels.Decode(Ids, Configuration["Security:SecretId"] + ControllerName).ToString()), Status = Status };
            try
            {
                if (item.Id > 0)
                {
                    item.CreatedBy = int.Parse(HttpContext.Request.Headers["Id"]);
                    item.ModifiedBy = int.Parse(HttpContext.Request.Headers["Id"]);
                    dynamic UpdateStatus = PostMessageService.UpdateStatus(item);
                    TempData["MessageSuccess"] = "Cập nhật Trạng Thái thành công";
                    return Json(new MsgSuccess());
                }
                else
                {
                    TempData["MessageError"] = "Cập nhật Trạng Thái Không thành công";
                    return Json(new MsgError());
                }
            }
            catch
            {
                TempData["MessageSuccess"] = "Cập nhật Trạng Thái không thành công";
                return Json(new MsgError());
            }
        }
    }
}
