﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using API.Areas.Admin.Models.SYSParams;
using API.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class HomeController : Controller
    {
        [HttpPost]
        public IActionResult DeleteAllTemp()
        {
            string dirPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot") + "/temp/";
            DirectoryInfo dirInfo = new DirectoryInfo(dirPath);
            FileInfo[] childFiles = dirInfo.GetFiles();

            foreach (FileInfo childFile in childFiles)
            {                
                if (System.IO.File.Exists(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot") + "/temp/"+ childFile.Name))
                {                    
                    System.IO.File.Delete(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot") + "/temp/" + childFile.Name);
                }
            }
            return Json(new MsgSuccess() {  });         
        }

        public IActionResult Index()
        {
            return Redirect("/Admin/Articles/Index");            
        }
        [HttpGet]
        public IActionResult SetSeccionMenu(int Id)
        {
            HttpContext.Session.SetInt32("IdMenu", Id);
            return Json(new MsgSuccess() { });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SaveItem(SYSConfig model)
        {
            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString();            
            SYSConfigModel data = new SYSConfigModel() { Item = model };
            if (ModelState.IsValid)
            {                
                SYSParamsService.SaveConfig(model);
                TempData["MessageSuccess"] = "Cập nhật thành công";
                return RedirectToAction("SaveItem");
            }
            data.ListItemsLayout = SYSParamsService.GetListLayout();
            data.ListStatus = SYSParamsService.GetListStatus();
            return View(data);
            
        }

        public IActionResult SaveItem([FromQuery] string Culture = "vi")
        {
            SYSConfigModel Model = new SYSConfigModel() {
                Item = SYSParamsService.GetItemConfig(Culture.ToLower()),
                ListItemsLayout = SYSParamsService.GetListLayout(),
                ListStatus = SYSParamsService.GetListStatus()
            };
            
            Model.Item.Culture = Culture.ToLower();
            return View(Model);
        }
    }
}