﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using API.Areas.Admin.Models.Articles;
using API.Models;
using API.Areas.Admin.Models.CategoriesArticles;
using API.Areas.Admin.Models.DMCoQuan;
using API.Models.Utilities;
using System.Threading.Tasks;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace API.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class ArticlesTransferController : Controller
    {
        public IActionResult Index([FromQuery] SearchArticles dto)
        {
            int TotalItems = 0;
            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString() + "_" + HttpContext.Request.Headers["UserName"];
            dto.IdCoQuan = int.Parse(HttpContext.Request.Headers["IdCoQuan"]);
            ArticlesModel data = new ArticlesModel() { SearchData = dto };
            data.ListItems = ArticlesService.GetListTransferPagination(data.SearchData, API.Models.Settings.SecretId + ControllerName);
            data.ListItemsDanhMuc = CategoriesArticlesService.GetListItems();
            data.ListItemsAuthors = API.Areas.Admin.Models.USUsers.USUsersService.GetListItemsAuthor(4);
            data.ListItemsCreatedBy = API.Areas.Admin.Models.USUsers.USUsersService.GetListItemsAuthor(3);
            data.ListItemsStatus = ArticlesService.GetListItemsStatus();
            data.ListDMCoQuan = DMCoQuanService.GetListByLoaiCoQuan(-1,0, int.Parse(HttpContext.Request.Headers["IdCoQuan"]));
            if (data.ListItems != null && data.ListItems.Count() > 0)
            {
                TotalItems = data.ListItems[0].TotalRows;
            }

            HttpContext.Session.SetString("STR_Action_Link_" + ControllerName, Request.QueryString.ToString());
            data.Pagination = new Models.Partial.PartialPagination() { CurrentPage = data.SearchData.CurrentPage, ItemsPerPage = data.SearchData.ItemsPerPage, TotalItems = TotalItems, QueryString = Request.QueryString.ToString() };

            return View(data);
        }

        public IActionResult GetItem(string Id = null)
        {
            Articles Item = new Articles();
            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString() + "_" + HttpContext.Request.Headers["UserName"];
            int IdDC = Int32.Parse(MyModels.Decode(Id, API.Models.Settings.SecretId + ControllerName).ToString());
            if (IdDC > 0)
            {
                Item = ArticlesService.GetItem(IdDC, API.Models.Settings.SecretId + ControllerName);
            }
            return Json(Item);
        }

        public IActionResult InsertArticlesTransfer(string Id = null)
        {
            Articles Item = new Articles();
            string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString() + "_" + HttpContext.Request.Headers["UserName"];
            int IdDC = Int32.Parse(MyModels.Decode(Id, API.Models.Settings.SecretId + ControllerName).ToString());

            try
            {
                var res = ArticlesService.InsertArticlesTransfer(IdDC, Int32.Parse(HttpContext.Request.Headers["IdCoQuan"]));
                var a = res.Rows[0][0];
                return Json(new API.Models.MsgSuccess() { Msg = "Lấy dữ liệu tin tức Thành Công",Data = res.Rows[0][0] });
            }
            catch(Exception e) {
                return Json(new API.Models.MsgError() { Msg = e.Message });
            }
            

            
        }


    }
}